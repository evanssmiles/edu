export const iconButton = {
    height: 20,
    width: 20,
    marginHorizontal: 10,
    resizeMode: 'center',
}