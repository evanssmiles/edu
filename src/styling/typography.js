export const headline =  {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
}

export const textCenter = {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
}

export const textCenterBlack = {
    fontSize: 14,
    textAlign: 'center',
    marginVertical: 10,
}