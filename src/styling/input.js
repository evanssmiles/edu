export const input = {
    borderWidth: .5,
    borderRadius: 10,
    paddingVertical: 3,
    paddingHorizontal: 10,
    marginVertical: 10,
}