import * as colors from './color'
import * as layout from './layout'
import * as font from './typography'
import * as btn from './button'
import * as input from './input'
import * as image from './image'

export {colors, layout, font, btn, input, image}