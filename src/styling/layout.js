export const container = {
    padding: 20
}

export const flexRowCenter = {
    flexDirection: 'row', 
    justifyContent: 'center', 
    alignItems: 'center', 
    marginVertical: 30
}