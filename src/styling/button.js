import * as Colors from './color'

export const button = {
    borderRadius: 25,
    paddingVertical: 10,
    marginVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
}

export const buttonShadow = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
}

export const buttonIcon = {
    flexDirection: 'row',
}

export const optionalButton = {
    borderRadius: 15,
    marginVertical: 30,
    paddingVertical: 10,
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
    flexDirection: 'row',
    justifyContent: 'center',
}

export const iconButton = {
    height: 20,
    width: 20,
    marginHorizontal: 10,
    resizeMode: 'center',
}