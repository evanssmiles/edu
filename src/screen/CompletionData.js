import React, {useState} from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import {colors} from '../styling';
import {Button, Input} from '../component';
import {AuthContext} from '../component/Context';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {baseUrl} from './dashboard/SeminarList';

function dummyJobDetail() {
  const {signOut} = React.useContext(AuthContext);

  return (
    <View
      style={{justifyContent: 'center', alignItems: 'center', height: '100%'}}>
      <Text style={{marginBottom: 30}}>
        Please Complete your data here first
      </Text>
      <TouchableOpacity
        style={{backgroundColor: 'green', padding: 20}}
        onPress={() => {
          signOut();
        }}>
        <Text style={{color: 'white'}}>Later, Let me Sign Out</Text>
      </TouchableOpacity>
    </View>
  );
}

const JobDetail = () => {
  const [currentPosition, setCurrentPosition] = useState(1);
  const [token, setToken] = useState(null);

  let savedStep;
  let key;
  setTimeout(async () => {
    savedStep = await AsyncStorage.getItem('stepCompletion');
    key = await AsyncStorage.getItem('userToken');
    setToken(key);
    setCurrentPosition(savedStep);
  }, 1000);

  const labels = ['Register', 'Pekerjaan', 'Referral Code'];
  const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: colors.color.yellowPrimary,
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: colors.color.yellowPrimary,
    stepStrokeUnFinishedColor: colors.color.grey2,
    separatorFinishedColor: colors.color.yellowPrimary,
    separatorUnFinishedColor: colors.color.grey2,
    stepIndicatorFinishedColor: colors.color.yellowPrimary,
    stepIndicatorUnFinishedColor: colors.color.grey2,
    stepIndicatorCurrentColor: colors.color.yellowPrimary,
    stepIndicatorLabelFontSize: 3,
    currentStepIndicatorLabelFontSize: 0,
    stepIndicatorLabelCurrentColor: 'transparent',
    stepIndicatorLabelFinishedColor: 'transparent',
    stepIndicatorLabelUnFinishedColor: 'transparent',
    labelColor: '#999999',
    labelSize: 12,
    currentStepLabelColor: colors.color.black,
  };

  const [errorName, setErrorName] = useState('');
  const [errorSchool, setErrorSchool] = useState('');
  const [errorTitle, setErrorTitle] = useState('');

  const [data, setData] = React.useState({
    name: '',
    title: 0,
    school: '',
  });

  const [errorReferral, setErrorReferral] = useState('');
  const [inpReferral, setInpReferral] = useState('');

  const {completeData} = React.useContext(AuthContext);

  const onPageChange2 = position => {
    setCurrentPosition(position);
  };

  const onPageChange = position => {
    if (currentPosition == 1) {
      let userName = data.name.trim();
      let userTitle = data.title + 1;
      let userSchool = data.school.trim();
      let validation = false;
      console.log('namanya ', userName);
      console.log('panggilanya ', userTitle);
      console.log('sekolahnya ', userSchool);

      if (userName != '') {
        validation = true;
        setErrorName('');
      } else {
        validation = false;
        setErrorName('Nama tidak boleh kosong');
      }

      if (userTitle > 0 && userTitle < 3) {
        validation = true;
        setErrorTitle('');
      } else {
        validation = false;
        setErrorTitle('Silakan pilih nama panggilan anda');
      }

      if (userSchool != '') {
        validation = true;
        setErrorSchool('');
      } else {
        validation = false;
        setErrorSchool('Sekolah anda tidak boleh kosong');
      }

      if (validation) {
        axios({
          method: 'post',
          url: baseUrl + '/v2/1/onbording/compliting',
          data: JSON.stringify({
            name: userName,
            Gender: userTitle,
            SchoolName: userSchool,
          }),
          headers: {
            token: token,
            ContentType: 'application/json',
          },
        })
          .then(async function (res) {
            console.log(res);
            if (res.status >= 200 && res.status < 300) {
              setCurrentPosition(position);
              await AsyncStorage.setItem('stepCompletion', '2');
            } else {
              console.log(res);
            }
          })
          .catch(function (error) {
            console.log(error);
          });
      }
      // for last step
    } else if (currentPosition == 2) {
      let ref = inpReferral;
      if (ref == '') {
        setErrorReferral('Kode referral harus diisi');
        console.log('isi dulu qq');
        return;
      }
      console.log(ref);
      let url = baseUrl + '/v2/onbording/' + ref + '/referal';
      console.log(url);
      setErrorReferral('');
      axios({
        method: 'get',
        url: url,
        headers: {
          token: token,
        },
      })
        .then(function (res) {
          console.log(res);
          if (res.status >= 200 && res.status < 300) {
            completeData();
            console.log('kita proses ya');
          } else {
          }
        })
        .catch(function (error) {
          let msg = error.response.data.message;
          setErrorReferral(msg);
        });
    }
  };

  const handleComplete = () => {
    axios({
      method: 'get',
      url: baseUrl + '/v2/onbording/2/skip',
      headers: {
        token: token,
      },
    })
      .then(function (res) {
        console.log(res);
        if (res.status >= 200 && res.status < 300) {
          completeData();
        } else {
          console.log(res);
        }
      })
      .catch(function (error) {
        console.log(error.response.status);
        console.log(error.response.data);
      });
  };
  console.log(currentPosition);

  return (
    <View style={styles.container}>
      <StepIndicator
        customStyles={customStyles}
        currentPosition={parseInt(currentPosition)}
        stepCount={3}
        labels={labels}
      />
      {currentPosition == 1 ? (
        <View>
          <View style={styles.content}>
            <Text style={styles.headline}>Halo, Guru Teladan Indonesia!</Text>
            <Text style={styles.subHeadline}>
              Bantu kami lebih mengenal anda
            </Text>
            <Input.Input
              label="Nama"
              placeholder="Tulis nama anda"
              onChange={name => {
                setData({...data, name: name});
              }}
              error={errorName}
            />
            <Input.radioInput
              label="Di Sekolah Anda Dipanggil"
              options={['Bapak Guru', 'Ibu Guru']}
              value={data.title}
              onValueChange={title => {
                setData({...data, title: title});
              }}
              error={errorTitle}
            />
            <Input.Input
              label="Di Sekolah mana anda mengajar"
              placeholder="Contoh: SMA 1 Jakarta"
              onChange={school => {
                setData({...data, school: school});
              }}
              error={errorSchool}
            />
          </View>
          <Button.filledButton
            title="Selanjutnya"
            onPress={() => {
              onPageChange(currentPosition + 1);
            }}
          />
        </View>
      ) : (
        <View>
          <View style={styles.content}>
            <Text style={styles.headline}>Masukkan Refferal Code</Text>
            <Input.Input
              placeholder="#1234"
              onChange={ref => {
                setInpReferral(ref);
              }}
              error={errorReferral}
            />
          </View>
          <Button.filledButton
            title="Submit"
            onPress={() => {
              onPageChange(currentPosition + 1);
            }}
          />
          <Text style={styles.label}>Belum ada referral code?</Text>
          <Button.textButton
            label="Lewati"
            style={styles.txtButton}
            onPress={() => {
              handleComplete();
            }}
          />
        </View>
      )}
    </View>
  );
};

export default JobDetail;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    paddingHorizontal: 25,
  },
  content: {
    marginVertical: 50,
  },
  headline: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  subHeadline: {
    fontSize: 15,
    textAlign: 'center',
    color: colors.color.grey,
    marginTop: 10,
    marginBottom: 20,
  },
  label: {
    textAlign: 'center',
    color: colors.color.grey,
    marginVertical: 15,
  },
  txtButton: {
    color: colors.color.greenPrimary,
    fontSize: 15,
  },
});
