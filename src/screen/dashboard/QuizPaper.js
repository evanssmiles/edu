import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  ScrollView,
  Image,
  TextInput,
  Text,
  TouchableOpacity,
  Linking,
  StyleSheet,
} from 'react-native';
import {colors} from '../../styling';
import {Button, Input} from '../../component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Video from 'react-native-video';

import IconClockWhite from '../../assets/icon/clockWhite.svg';
import IconClock from '../../assets/icon/alarm.svg';
import IconClockGreen from '../../assets/icon/alarmGreen.svg';
import IconDown from '../../assets/icon/arrowDown.svg';
import IconBurger from '../../assets/icon/burgermenu.svg';
import IconReply from '../../assets/icon/iconReply.svg';
import IconDoc from '../../assets/icon/doc.svg';
import IconClose from '../../assets/icon/closeIcon.svg';

import CountDown from 'react-native-countdown-component';
import {baseUrl} from './SeminarList';

const general = StyleSheet.create({
  inline: {
    flexDirection: 'row',
  },
  centerBoth: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentPadding: {
    padding: 15,
  },
});

export default function QuizPaper({navigation, id, questionBank, quizData}) {
  let timeConsume = 0;
  const [process, setProcess] = useState(0);
  const [bankQuestion, setBankQuestion] = useState(questionBank);
  const [timer, setTimer] = useState(quizData.completion_time * 60);

  async function finishQuiz() {
    let collect = {
      aktivitas_detail_id: quizData.aktivitas_detail_id,
      ps_id: quizData.ps_id,
      ev_id: quizData.ev_id,
      bank_soal: [],
    };
    for (let i = 0; i < bankQuestion.length; i++) {
      let pass = {
        bs_id: bankQuestion[i].bs_id,
        true_answer: bankQuestion[i].true_answer,
        user_answer: bankQuestion[i].user_answer,
      };
      collect.bank_soal.push(pass);
    }
    console.log('THIS IS YOUR FINAL ANSWER : ', collect), '<<<<63';

    const tokek = await AsyncStorage.getItem('userToken');
    console.log('token>>>>', tokek);
    axios({
      url: 'https://api-sandbox.bank-soal.proedu.id/users/answers/create',
      method: 'post',
      headers: {
        Authorization: await AsyncStorage.getItem('userToken'),
      },
      data: collect,
    })
      .then(function (res) {
        console.log(res.data, '<<<<580');
      })
      .catch(function (err) {
        console.log(err.response.data, '<<<81');
      });

    navigation.navigate('DashboardStack', {
      screen: 'Home',
    });
  }

  const Heading = ({data}) => {
    const header = StyleSheet.create({
      container: {
        height: 50,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
      numDone: {
        color: colors.color.greenPrimary,
      },
      timer: {
        marginLeft: 5,
      },
    });

    return (
      <View style={[header.container, general.contentPadding]}>
        <IconClose height={15} width={15} />
        <View style={[general.inline, {flex: 1, marginLeft: 30}]}>
          <Text style={[header.numDone]}>{process + 1}/</Text>
          <Text>{data.bank_soal.length}</Text>
        </View>
        <View style={[general.inline, general.centerBoth]}>
          <IconClockGreen />
          <CountDown
            size={14}
            until={timer}
            // onFinish={() => alert('Finished')}
            digitStyle={{backgroundColor: 'transparent', borderWidth: 0}}
            digitTxtStyle={{color: colors.color.black}}
            timeLabelStyle={{color: 'red', fontWeight: 'bold'}}
            separatorStyle={{color: colors.color.black}}
            timeToShow={['H', 'M', 'S']}
            timeLabels={{m: null, s: null}}
            onChange={() => setTimer(timer - 1)}
            showSeparator
          />
        </View>
      </View>
    );
  };

  function moveQuestion(type) {
    setTimer(timer);
    let endProcess;
    if (type == 'next') {
      endProcess = process + 1;
      if (endProcess >= questionBank.length) {
        navigation.navigate('ActivityStack', {id: id});
      } else {
        setProcess(process + 1);
        setTimer(timer);
      }
    } else if (type == 'prev') {
      endProcess = process - 1;
      if (endProcess < 0) {
        return;
      }
      setProcess(process - 1);
      setTimer(timer);
    }
  }

  const Question = () => {
    const question = StyleSheet.create({
      container: {
        padding: 25,
      },
    });

    const changeOption = (title, idx) => {
      let collect = bankQuestion;
      collect[idx].user_answer = title;
      setBankQuestion(collect);
    };

    return (
      <ScrollView style={{flex: 1}}>
        <View style={[question.container, {flex: 1}]}>
          <View style={[general.inline]}>
            <Text>{process + 1} </Text>
            <View style={{width: '100%'}}>
              <Text>{bankQuestion[process].question}</Text>
              <View style={[question.option]}>
                <Input.radioAnswer
                  label=""
                  options={bankQuestion[process].answer.map((item, key) => {
                    return item.answer;
                  })}
                  dataPack={bankQuestion[process].answer}
                  value={bankQuestion[process].user_answer}
                  onValueChange={title => changeOption(title, process)}
                />
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  };

  return (
    <View style={{flex: 1}}>
      <Heading data={quizData} />
      <Question></Question>
      <View
        style={{
          padding: 20,
          backgroundColor: 'white',
          borderTopWidth: 1,
          borderTopColor: 'transparent',
          elevation: 3,
        }}>
        {process != 0 && (
          <Button.filledWhite
            title="Sebelumnya"
            onPress={() => {
              moveQuestion('prev');
            }}
          />
        )}
        {process + 1 < bankQuestion.length && (
          <Button.filledButton
            title="Selanjutnya"
            onPress={() => {
              moveQuestion('next');
            }}
          />
        )}
        {process + 1 == bankQuestion.length && (
          <Button.filledButton
            title="Selesai"
            onPress={() => {
              finishQuiz();
            }}
          />
        )}
      </View>
    </View>
  );
}
