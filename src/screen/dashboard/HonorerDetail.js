import React, {useState, useEffect, useCallback, useContext} from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { colors } from '../../styling'
import { Input, Button } from '../../component'
import IconInfoRed from '../../assets/icon/infoRed.svg'
import IconInfoGrey from '../../assets/icon/infoGrey.svg'
import IconSuccess from '../../assets/icon/checkGreen.svg'
import IconSuccessCircle from '../../assets/icon/checkedCircleGreen.svg'
import IconFileUpload from '../../assets/icon/uploadFile.svg'
import DocumentPicker from "react-native-document-picker/index";
import NativeUploady, {
  UploadyContext,
  useItemFinishListener,
  useItemStartListener,
  useItemErrorListener,
} from "@rpldy/native-uploady";

const style = StyleSheet.create({
    container: {
        position: 'relative',
        flex: 1,
    },
    content: {
        padding: 20,
    },
    inline: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    icon: {
        marginRight: 10,
    },
    txtGreen: {
        color: colors.color.greenPrimary
    },
    heading: {
        fontSize: 14, 
        fontWeight: 'bold',
        marginBottom: 10,
    },
    referral: {
        textAlign: 'center',
        fontSize: 32,
        fontWeight: 'bold',
        marginVertical: 25,
    },
    warningTxt: {
        color: colors.color.red,
        lineHeight: 16,
    },
    spaceY: {
        marginVertical: 10,
    },
    absoluteBottom: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
        padding: 25,
        backgroundColor: '#FFF'
    }
})

const upload = StyleSheet.create({
    container: {
        backgroundColor: '#F2F2FB',
        borderWidth: 1,
        borderColor: colors.color.blueShape,
        borderRadius: 15, 
        borderStyle: 'dashed',
        width: '100%',
        height: 200,
        marginTop: 30,
        marginBottom: 10,
        position: 'relative',
    },
    content: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        flex: 1,
        padding: 15,
    },
    fileInfo: {
        fontSize: 12,
        marginRight: 15,
    }
})

const header = StyleSheet.create({
    header: {
        width: '100%',
        padding: 15,
    },
    fail: {
        backgroundColor: colors.color.red2,
        color: colors.color.red
    },
    success: {
        backgroundColor: colors.color.greenPrimary,
        color: 'white'
    }
})

const UploadContainer = () => {
    const [uploadUrl, setUploadUrl] = useState(false);
    const [loading, setLoading] = useState(false);
    const [uploaded, setUploaded] = useState(false);
    const uploadyContext = useContext(UploadyContext);
  
    useItemFinishListener((item) => {
        console.log('finish sih : ',item)
        // const response = JSON.parse(item.uploadResponse.data);
        // console.log(`item ${item.id} finished uploading, response was: `, response);
        // setUploadUrl(response.url);
        setLoading(false)
        setUploaded(true)
    });
  
    useItemErrorListener((item) => {
        console.log(`item ${item.id} upload error !!!! `, item);
    });
  
    useItemStartListener((item) => {
        setLoading(true)
        console.log(`item ${item.id} starting to upload,name = ${item.file.name}`);

    });
  
    const pickFile = useCallback(async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            });
            console.log('res : ',res)
            uploadyContext.upload(res);
            console.log('uploadly : ',uploadyContext)
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log("User cancelled the picker, exit any dialogs or menus and move on");
            } else {
                console.log(err);
                throw err;
            }
        }
    }, [uploadyContext]);
  
    return (
        <View>
            <TouchableOpacity style={[upload.container]} onPress={pickFile}>
                { !loading && !uploaded && (
                    <View style={[upload.content]}>
                        <IconFileUpload width={48} height={48} style={[style.spaceY]} />
                        <Text style={[style.spaceY]}>Format: PDF, JPEG, JPG, PNG, Max 2 MB</Text>
                        <Text style={[style.spaceY, style.txtGreen]}>Klik untuk mencari dokumen</Text>
                    </View> 
                )}
                { loading && !uploaded && (
                    <View style={[upload.content]}>
                        <Text style={{color:colors.color.grey, textAlign: 'center'}}>Sedang mengupload...</Text>
                    </View>
                )}
                { !loading && uploaded && (
                    <View style={[upload.content]}>
                        <Text>Upload Berhasil</Text>
                    </View>
                )}
            </TouchableOpacity>
            { uploaded && (
                    <View style={[style.inline, {alignItems:'center'}]}>
                        <Text style={[upload.fileInfo]}>Surat Keputusan - 5 MB</Text>
                        <IconSuccess height={12} width={12}/>
                    </View>
                )
            }
            {uploadUrl && <Image source={{ uri: uploadUrl }} style={styles.uploadedImage} />}
        </View>
    );
};

export default function HonorerDetail() {
    const [loading, setLoading] = useState(false)
    const [honorer, setHonorer] = useState(true)
    const [uploading, setUploading] = useState(false)
    const [file, setFile] = useState(null)

    const inputReferral = (code) => {
        setReferral(code)
    }

    const submitDoc = () => {
        
    }

    const changeReferral = () => {
        setHaveReferral(false)
    }

    return (
        <View style={[style.container]}>
            <View style={[header.header, honorer?header.success:header.fail]}>
                <Text style={[honorer?header.success:header.fail]}>
                    {honorer?'Dokumen Terverifikasi':'Dokumen Kadaluarsa'} 
                </Text>
            </View>
            <View style={[style.content]}>
                <Text style={[style.heading]}>Upload Dokumen:</Text>
                <Text>Surat Keputusan / Screenshoot lembar GTK Anda / Dokumen penunjang</Text>
                <NativeUploady destination={{ url: "https://my-server.test.com/upload" }}>
                    <UploadContainer />
                </NativeUploady>
                <View style={[style.inline, {alignItems: 'center', marginTop: 15}]}>
                    <IconInfoGrey width={16} height={16} fill={colors.color.grey} style={[style.icon]}/>
                    <Text style={{flex: 1}}>Masa berlaku dokumen 1 bulan. Setelah masa berlaku habis, mohon upload kembali dokumen anda</Text>
                </View>
            </View>
            <View style={[style.absoluteBottom]}>
                <Button.filledButton 
                    disabled={honorer||loading?true:false}
                    title={loading?'Simpan...':'Menyimpan'} 
                    onPress={() => submitDoc()} />
                {/* <View style={[style.inline, {alignItems: 'center', justifyContent: 'center'}]}>
                    <Text style={{fontWeight: 'bold', marginRight: 5}}>Tersimpan!</Text>
                    <IconSuccessCircle />
                </View> */}
            </View>
        </View>
    )
}
