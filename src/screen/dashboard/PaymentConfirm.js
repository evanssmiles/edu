import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ToastAndroid,
  Modal,
} from 'react-native';
import {colors} from '../../styling';
import {Input, Button} from '../../component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import chevDown from '../../assets/icon/chevron-down.png';
import {CartLoading} from './CartDetail';
import Clipboard from '@react-native-clipboard/clipboard';
import {baseUrl} from './SeminarList';
import x from '../../assets/icon/Subtract.png';

const style = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  content: {
    padding: 20,
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    marginRight: 10,
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
  heading: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  referral: {
    textAlign: 'center',
    fontSize: 32,
    fontWeight: 'bold',
    marginVertical: 25,
  },
  warningTxt: {
    color: colors.color.red,
    lineHeight: 16,
  },
  spaceY: {
    marginVertical: 10,
  },
  absoluteBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    padding: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'white',
  },
});

const header = StyleSheet.create({
  container: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    backgroundColor: 'white',
  },
  btn: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: colors.color.grey2,
    marginRight: 10,
  },
  txt: {
    color: colors.color.grey,
  },
  txtActive: {
    color: colors.color.greenPrimary,
  },
  btnActive: {
    borderColor: colors.color.greenPrimary,
  },
});

export default function PaymentConfirm({navigation, data}) {
  console.log(data.item[0], '<<<<<96');
  const [loading, setLoading] = useState(true);
  const [status, setStatus] = useState('loading');
  const [token, setToken] = useState('');
  const [mBanking, setMBanking] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [liteDetail, setLiteDetail] = useState(null);

  const [checkoutData, setCheckoutData] = useState(null);

  let url;

  if (data.buyNow) {
    url = `${baseUrl}/v1/transaction/checkout/${data.payment.mercentId}?type=buynow`;
  } else {
    url = baseUrl + '/v1/transaction/checkout/' + data.payment.mercentId;
  }

  useEffect(async () => {
    let getToken = await AsyncStorage.getItem('userToken');
    axios({
      url: url,
      method: 'GET',
      headers: {
        token: getToken,
      },
    })
      .then(function (res) {
        if (res.status < 300) {
          setCheckoutData(res.data.result);
        }
        setLoading(false);
      })
      .catch(function (err) {
        console.log('error is : ', err.response);
        setLoading(false);
      });
  }, []);

  function nextStep() {
    navigation.replace('TransactionStack', {
      screen: 'Detail',
      data: {id: checkoutData.idTransaksi},
    });
  }

  if (loading) {
    return <CartLoading />;
  }

  const copyToClipboard = n => {
    Clipboard.setString(n);
    ToastAndroid.show('Teks berhasil disalin', ToastAndroid.SHORT);
  };

  return (
    <View style={[style.container]}>
      <View style={[header.container, {elevation: 3}]}>
        <Text>Menunggu Pembayaran</Text>
      </View>
      <ScrollView contentContainerStyle={{paddingBottom: 150}}>
        <View
          style={{
            paddingHorizontal: 20,
            paddingVertical: 15,
            backgroundColor: 'white',
          }}>
          <Text>Segera selesaikan pembayaran sebelum</Text>
          <Text style={{fontWeight: 'bold'}}>{checkoutData.limitPaid}</Text>
        </View>
        <>
          <Text
            style={{
              paddingHorizontal: 20,
              marginVertical: 10,
              fontWeight: 'bold',
            }}>
            Metode Pembayaran
          </Text>
          <View
            style={{
              paddingHorizontal: 20,
              paddingVertical: 15,
              backgroundColor: 'white',
              borderBottomWidth: 1,
              borderBottomColor: colors.color.grey2,
            }}>
            <View
              style={[
                style.inline,
                {justifyContent: 'space-between', marginBottom: 10},
              ]}>
              <View>
                <Text style={[{color: colors.color.grey, marginBottom: 5}]}>
                  Virtual Account
                </Text>
                <Text>{checkoutData.mercent.title}</Text>
              </View>
              <Image source={{uri: checkoutData.mercent.logo}} />
            </View>
          </View>
          <View
            style={{
              paddingHorizontal: 20,
              paddingVertical: 15,
              backgroundColor: 'white',
              borderBottomWidth: 1,
              borderBottomColor: colors.color.grey2,
            }}>
            <View>
              <Text style={[{color: colors.color.grey, marginBottom: 5}]}>
                Nomor Virtual Account
              </Text>
              <View
                style={[
                  style.inline,
                  {justifyContent: 'space-between', marginBottom: 10},
                ]}>
                <Text>
                  {checkoutData.mercent.bill_key
                    ? checkoutData.mercent.bill_key
                    : checkoutData.mercent.va_number}
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    copyToClipboard(
                      checkoutData.mercent.bill_key ||
                        checkoutData.mercent.va_number,
                    )
                  }>
                  <Text
                    style={[
                      style.txtGreen,
                      {fontWeight: 'bold', letterSpacing: 3},
                    ]}>
                    SALIN
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {checkoutData.mercent.bill_key && (
            <View
              style={{
                paddingHorizontal: 20,
                paddingVertical: 15,
                backgroundColor: 'white',
                borderBottomWidth: 1,
                borderBottomColor: colors.color.grey2,
              }}>
              <View>
                <Text style={[{color: colors.color.grey, marginBottom: 5}]}>
                  Biller Code
                </Text>
                <View
                  style={[
                    style.inline,
                    {justifyContent: 'space-between', marginBottom: 10},
                  ]}>
                  <Text>{checkoutData.mercent.biller_code}</Text>
                  <TouchableOpacity
                    onPress={() =>
                      copyToClipboard(checkoutData.mercent.biller_code)
                    }>
                    <Text
                      style={[
                        style.txtGreen,
                        {fontWeight: 'bold', letterSpacing: 3},
                      ]}>
                      SALIN
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
          <View
            style={{
              paddingHorizontal: 20,
              paddingVertical: 15,
              backgroundColor: 'white',
              borderBottomWidth: 1,
              borderBottomColor: colors.color.grey2,
            }}>
            <View>
              <Text style={[{color: colors.color.grey, marginBottom: 5}]}>
                Total Pembayaran
              </Text>
              <View
                style={[
                  style.inline,
                  {justifyContent: 'flex-start', marginBottom: 10},
                ]}>
                <Text style={{color: colors.color.red}}>
                  {checkoutData.full_price}
                </Text>
              </View>
            </View>
          </View>
        </>
        <>
          <Text
            style={{
              paddingHorizontal: 20,
              marginVertical: 10,
              fontWeight: 'bold',
            }}>
            Cara Pembayaran
          </Text>
          {checkoutData.paidMethodeStep.map((val, key) => {
            return (
              <View
                key={val.title}
                style={{
                  paddingHorizontal: 20,
                  paddingVertical: 15,
                  backgroundColor: 'white',
                  borderBottomWidth: 1,
                  borderBottomColor: colors.color.grey2,
                }}>
                <View
                  style={[
                    !mBanking && style.inline,
                    {justifyContent: 'space-between'},
                  ]}>
                  <Text style={{marginBottom: 5}}>{val.title}</Text>
                  {mBanking && <Text>{val.description}</Text>}
                  {!mBanking && (
                    <TouchableOpacity onPress={() => setMBanking(!mBanking)}>
                      <View
                        style={{
                          width: 30,
                          height: 30,
                          alignItems: 'flex-end',
                          justifyContent: 'center',
                        }}>
                        <Image
                          style={{
                            width: 13,
                            height: 13,
                            resizeMode: 'contain',
                          }}
                          source={chevDown}
                        />
                      </View>
                    </TouchableOpacity>
                  )}
                  {mBanking && (
                    <TouchableOpacity onPress={() => setMBanking(!mBanking)}>
                      <Text
                        style={{
                          marginTop: 3,
                          alignSelf: 'flex-end',
                          letterSpacing: 1.5,
                          fontWeight: 'bold',
                          color: colors.color.black,
                        }}>
                        TUTUP
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            );
          })}
        </>
      </ScrollView>
      <View style={[status != 'check' ? style.absoluteBottom : null]}>
        <Button.filledButton
          style={[]}
          title="Cek Status Pembayaran"
          onPress={() => nextStep()}
        />
        <Button.filledWhite
          style={[]}
          title="Eksplor Seminar Lainnya"
          onPress={() =>
            navigation.replace('SeminarStack', {
              screen: 'ExploreSeminar',
            })
          }
        />
      </View>
    </View>
  );
}
