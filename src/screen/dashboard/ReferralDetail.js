import React, {useState, useEffect} from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { colors } from '../../styling'
import { Input, Button } from '../../component'
import IconInfoRed from '../../assets/icon/infoRed.svg'
import IconSuccess from '../../assets/icon/checkedCircleGreen.svg'

const style = StyleSheet.create({
    container: {
        position: 'relative',
        flex: 1,
    },
    content: {
        padding: 20,
    },
    inline: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    icon: {
        marginRight: 10,
    },
    txtGreen: {
        color: colors.color.greenPrimary
    },
    heading: {
        fontSize: 14, 
        fontWeight: 'bold',
        marginBottom: 10,
    },
    referral: {
        textAlign: 'center',
        fontSize: 32,
        fontWeight: 'bold',
        marginVertical: 25,
    },
    warningTxt: {
        color: colors.color.red,
        lineHeight: 16,
    },
    spaceY: {
        marginVertical: 10,
    },
    absoluteBottom: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
        padding: 25,
        backgroundColor: '#FFF'
    }
})

export default function ReferralDetail() {
    const [loading, setLoading] = useState(false)
    const [haveReferral, setHaveReferral] = useState(false)
    const [referral, setReferral] = useState('')
    const [error, setError] = useState('')

    const inputReferral = (code) => {
        setReferral(code)
    }

    const submitReferral = () => {
        setError('')
        setLoading(true)
        // if failed
        // setError('custom made error for testing')
        // if true
        // setHaveReferral(true)
        // setTimeout(() => {
        //     setLoading(false)
        // }, 3000);
    }

    const changeReferral = () => {
        setHaveReferral(false)
    }

    return (
        <View style={[style.container]}>
            <View style={[style.content]}>
                {haveReferral
                ? <>
                    <Text style={[style.heading]}>Kode anda sekarang</Text>
                    <Text style={[style.referral]}>#123A**</Text>
                    <View style={[style.inline]}>
                        <IconInfoRed width={16} height={16} style={[style.icon]}/>
                        <Text style={[style.warningTxt]}>Kode bersifat rahasia. Tidak untuk dibagikan dan bersifat terbatas.</Text>
                    </View>
                </>
                : <>
                    <Text style={[style.heading]}>Masukkan Referral Code Anda</Text>
                    <Text>Masukkan kode dari kombinasi simbol, huruf dan angka untuk menikmati layanan premium dari kami</Text>
                    <Text style={[style.txtGreen, style.spaceY]}>Bagaimana saya mendapatkan referral code?</Text>
                    <Input.Input 
                        placeholder='#123ABC' 
                        onChange={(txt) => inputReferral(txt)}
                        error={error} />
                </>
                }
            </View>
            <View style={[style.absoluteBottom]}>
                { haveReferral 
                ? <>
                    { loading 
                    ? <View style={[style.inline, {justifyContent: 'center'}]}>
                        <IconSuccess width={16} height={16} style={[style.icon]} />
                        <Text style={[style.heading]}>Berhasil Diterapkan</Text>
                    </View>
                    : <Button.filledWhite 
                        title='Ganti Kode' 
                        onPress={() => changeReferral()} /> 
                    }
                </>
                : <Button.filledButton 
                    title={loading?'Menerapkan...':'Terapkan'} 
                    disabled={referral.length<6||loading?true:false} 
                    onPress={() => submitReferral()} />
                }
            </View>
        </View>
    )
}
