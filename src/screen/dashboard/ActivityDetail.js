import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  View,
  ScrollView,
  Image,
  TextInput,
  Text,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Linking,
  StyleSheet,
  PermissionsAndroid,
  Alert,
  ToastAndroid,
} from 'react-native';
import {colors} from '../../styling';
import {Button} from '../../component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Video from 'react-native-video';
import {CartLoading} from './CartDetail';
import IconClockWhite from '../../assets/icon/clockWhite.svg';
import IconClock from '../../assets/icon/alarm.svg';
import IconDown from '../../assets/icon/arrowDown.svg';
import IconBurger from '../../assets/icon/burgermenu.svg';
import IconReply from '../../assets/icon/iconReply.svg';
import IconDoc from '../../assets/icon/doc.svg';
import IconArrowForward from '../../assets/icon/arrow_forward.svg';
import IconCheck from '../../assets/icon/checkedCircleGreen.svg';

import ProgressCircle from 'react-native-progress-circle';
import {NavigationContainer} from '@react-navigation/native';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';

import Accordion from 'react-native-collapsible/Accordion';

import YoutubePlayer from 'react-native-youtube-iframe';
import RNFetchBlob from 'rn-fetch-blob';
import {baseUrl} from './SeminarList';

const general = StyleSheet.create({
  inline: {
    flexDirection: 'row',
  },
  centerBoth: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentPadding: {
    padding: 15,
  },
});

export default function ActivityDetail({
  navigation,
  id,
  title,
  speaker,
  navData,
  quizStatus,
  quizDone,
  published,
  certificateData,
}) {
  const parentNavigation = navigation;
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({
    progress: 0,
  });
  const [quizComplete, setQuizComplete] = useState(null);

  const [taskList, setTaskList] = useState(null); //move up, call in parent screen
  const [task, setTask] = useState(0);
  const [playing, setPlaying] = useState(false);

  // const setTaskToComplete = activityId => {
  //   let targetTask = task;
  //   if (activityId != null && activityId != '' && activityId != undefined) {
  //     targetTask = activityId;
  //   }
  //   // console.log('complete content mana : ', targetTask);
  //   // console.log('complete content mana2 : ', task);
  //   // console.log('complete content mana3 : ', activityId);
  //   let setNewList = taskList;

  //   setNewList.map((item, key) => {
  //     item.sub.map((val, i) => {
  //       // console.log('compare = ', val.aktivitas_detail_id, ' : ', targetTask);
  //       // if (val.aktivitas_detail_id == targetTask) {
  //       //   setNewList[key].sub[i].TaskFinishStatus = 'FINISH';
  //       // }
  //       // console.log(val, '<<<91');
  //     });
  //   });

  //   setTaskList(setNewList);
  // };

  console.log(published, '<<<104');
  console.log(certificateData, '<<<106');

  useEffect(() => {
    // if (quizComplete != undefined && quizComplete != null) {
    //   setTaskToComplete(quizComplete);
    //   setQuizComplete(null);
    // }

    fetchAllTask(id);
    fetchProgress(id);

    // loading or progress bar data
  }, []);

  const completeATask = async (aktivitasId, id) => {
    axios({
      url: baseUrl + '/v1/1/event/theory/' + aktivitasId + '/finish',
      method: 'get',
      headers: {
        token: await AsyncStorage.getItem('userToken'),
      },
    })
      .then(function (res) {
        console.log(res.data, '<<<<580');
        fetchAllTask(id);
        fetchProgress(id);
      })
      .catch(function (err) {
        console.log(err.response);
      });
  };

  const fetchAllTask = async id => {
    let token = await AsyncStorage.getItem('userToken');
    setLoading(true);
    axios({
      //getting all task list
      url: baseUrl + '/v1/1/event/detail/' + id + '/activity',
      method: 'get',
      headers: {
        token: token,
      },
    })
      .then(function (res) {
        if (res.status < 300) {
          setTaskList(res.data.result);
        }
        setLoading(false);
      })
      .catch(function (err) {
        console.log(err.response);
        setLoading(false);
      });
  };

  const fetchProgress = async id => {
    let token = await AsyncStorage.getItem('userToken');
    setLoading(true);

    axios({
      url: baseUrl + '/v1/1/event/detail/' + id + '/percent',
      method: 'get',
      headers: {
        token: token,
      },
    })
      .then(function (res) {
        if (res.status < 300) {
          setData({...data, progress: res.data.result});
        }
        setLoading(false);
      })
      .catch(function (err) {
        console.log(err.response);
        setLoading(false);
      });
  };

  if (!taskList) {
    return <CartLoading />;
  }

  const Heading = () => {
    const header = StyleSheet.create({
      container: {
        height: 50,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
    });
    return (
      <View style={[header.container, general.contentPadding]}>
        <IconDown height={15} width={15} />
        <View style={[general.inline, general.centerBoth]}>
          <Text style={{marginRight: 15}}>Progress Course</Text>
          <ProgressCircle
            percent={data.progress}
            radius={18}
            borderWidth={5}
            color={colors.color.greenPrimary}
            shadowColor={colors.color.grey2}
            bgColor="#fff">
            <Text style={{fontSize: 10}}>{data.progress + '%'}</Text>
          </ProgressCircle>
        </View>
      </View>
    );
  };

  const CertificateOption = () => {
    const certi = StyleSheet.create({
      container: {
        width: '100%',
        paddingHorizontal: 15,
        paddingVertical: 10,
        backgroundColor: colors.color.greenPrimary,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
      txtUnpublish: {
        color: 'white',
      },
    });

    function Terbit() {
      return (
        <TouchableOpacity
          onPress={() => {
            if (data.progress == 100) {
              navigation.navigate('CertificateStack', {
                id,
                published: 1,
              });
            } else {
              ToastAndroid.show('Selesaikan Tugas Anda', ToastAndroid.SHORT);
            }
          }}>
          <View style={[certi.container]}>
            <Text style={certi.txtUnpublish}>Terbitkan Sertifikat Anda</Text>
            <IconArrowForward />
          </View>
        </TouchableOpacity>
      );
    }
    function Lihat() {
      return (
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('CertificateStack', {
              id: id,
              published: 3,
              certificateData: certificateData,
            });
          }}>
          <View
            style={[
              certi.container,
              {backgroundColor: colors.color.yellowHover},
            ]}>
            <Text style={certi.txtUnpublish}>Lihat Sertifikat</Text>
            <IconArrowForward />
          </View>
        </TouchableOpacity>
      );
    }
    return !published ? <Terbit /> : <Lihat />;
  };

  const CourseTitle = () => {
    const titles = StyleSheet.create({
      container: {
        backgroundColor: colors.color.grey3,
      },
      title: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 5,
      },
      speaker: {
        color: colors.color.grey,
      },
    });
    return (
      <View style={[general.contentPadding, title.container]}>
        <Text style={[titles.title]}>{title}</Text>
        <Text style={[titles.speaker]}>
          Oleh :&nbsp;
          {speaker.map((item, key) => {
            let txt = key + 1 < speaker.length ? item.name + ', ' : item.name;
            return txt;
          })}
        </Text>
      </View>
    );
  };

  // start using task list data
  const TaskTitle = ({navigation, data}) => {
    const taskTitle = StyleSheet.create({
      header: {
        backgroundColor: colors.color.greenShape,
        height: 50,
        width: '100%',
      },
      title: {
        color: 'white',
        fontWeight: '800',
        flex: 1,
        marginLeft: 10,
      },
      quality: {
        marginLeft: 5,
        color: 'white',
      },
    });
    return (
      <View style={[taskTitle.header]}>
        <View
          style={[general.inline, general.contentPadding, general.centerBoth]}>
          <BurgerMenu navigation={navigation} />
          <Text style={[taskTitle.title]}>
            {data.order + '. ' + data.title}
          </Text>
          {/* <IconClockWhite height={16} width={16}/>
                    <Text style={[taskTitle.quality]}>2 Jam/2 JP</Text>  */}
        </View>
      </View>
    );
  };

  function BurgerMenu({navigation}) {
    return (
      <IconBurger
        height={16}
        width={16}
        onPress={() => navigation.openDrawer()}
      />
    );
  }

  const Discussion = ({data}) => {
    const reviewStyle = StyleSheet.create({
      centerMidInline: {
        flexDirection: 'row',
        // justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 2,
        marginRight: 5,
      },
    });

    const commentStyle = StyleSheet.create({
      container: {
        padding: 5,
        height: 300,
        paddingBottom: 5,
      },
      item: {
        marginVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
      },
      avatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 25,
      },
      detail: {
        flex: 1,
      },
      name: {
        fontWeight: 'bold',
        fontSize: 14,
      },
      reply: {
        color: colors.color.grey,
        fontSize: 12,
      },
      date: {
        flex: 1,
        fontSize: 10,
        color: colors.color.grey2,
        marginBottom: 5,
      },
      comment: {
        marginVertical: 5,
        fontSize: 12,
      },
      iconReply: {
        marginRight: 5,
      },
    });

    const text = StyleSheet.create({
      textArea: {
        borderWidth: 1,
        borderColor: colors.color.grey2,
        borderRadius: 15,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        color: colors.color.black,
        padding: 10,
        textAlignVertical: 'top',
      },
    });

    return (
      <View style={[general.contentPadding, {paddingVertical: 5, flex: 1}]}>
        <ScrollView
          style={[commentStyle.container]}
          nestedScrollEnabled={true}
          showsVerticalScrollIndicator={false}>
          <TextInput
            style={text.textArea}
            multiline={true}
            numberOfLines={5}
            placeholder="Tulis pendapat atau tanggapan anda"
            placeholderTextColor={colors.color.grey}
          />
          <View style={{alignItems: 'flex-end', width: '100%'}}>
            <Button.filledButton
              title="Kirim"
              style={{width: 120, marginVertical: 5}}
              styleFont={{fontSize: 12}}
            />
          </View>
          <View style={[commentStyle.item]}>
            <Image
              style={[commentStyle.avatar]}
              source={{
                uri: 'https://sps.widyatama.ac.id/wp-content/uploads/2020/08/dummy-profile-pic-male1-300x300.jpg',
              }}></Image>
            <View style={[commentStyle.detail]}>
              <Text style={[commentStyle.name]}>Rudi Thamrin, B.A</Text>
              <Text style={[commentStyle.comment]}>Mantap! Bermanfaat</Text>
              <View style={[reviewStyle.centerMidInline]}>
                <Text style={[commentStyle.date]}>2 Bulan yang lalu</Text>
              </View>
              <TouchableOpacity style={[reviewStyle.centerMidInline]}>
                <IconReply
                  height={12}
                  width={12}
                  style={[commentStyle.iconReply]}
                />
                <Text style={[commentStyle.reply]}>Balas</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={[commentStyle.item]}>
            <Image
              style={[commentStyle.avatar]}
              source={{
                uri: 'https://sps.widyatama.ac.id/wp-content/uploads/2020/08/dummy-profile-pic-male1-300x300.jpg',
              }}></Image>
            <View style={[commentStyle.detail]}>
              <Text style={[commentStyle.name]}>Rudi Thamrin, B.A</Text>
              <Text style={[commentStyle.comment]}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </Text>
              <TouchableOpacity style={[reviewStyle.centerMidInline]}>
                <IconReply
                  height={12}
                  width={12}
                  style={[commentStyle.iconReply]}
                />
                <Text style={[commentStyle.reply]}>Balas</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  };

  function Watch({navigation, data}) {
    const [tabActive, setTabActive] = useState('description');
    const [content, setContent] = useState(null);
    const tabStyle = StyleSheet.create({
      parent: {
        flexDirection: 'row',
        width: '100%',
        height: 50,
        backgroundColor: colors.color.grey3,
      },
      tab: {
        width: 150,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        borderBottomWidth: 2,
        borderColor: colors.color.grey,
      },
      tabActive: {
        borderBottomWidth: 2,
        borderColor: colors.color.yellowHover,
      },
      menu: {
        padding: 15,
      },
    });
    const [currentTime, setCurrentTime] = useState(1);
    const [duration, setDuration] = useState(10);
    const completionVideo = currentTime / duration;

    const playerRef = useRef();

    useEffect(async () => {
      axios({
        url:
          baseUrl +
          '/v1/1/event/theory/' +
          data.aktivitas_detail_id +
          '/content',
        type: 'get',
        headers: {
          token: await AsyncStorage.getItem('userToken'),
        },
      })
        .then(function (res) {
          console.log(res.data.result, '<<<<466');
          setContent(res.data.result);
        })
        .catch(function (err) {
          console.log(err.response);
        });
    }, []);

    useEffect(() => {
      if (duration == 0) {
        playerRef.current
          ?.getDuration()
          .then(getDuration => setDuration(getDuration));
      }
    }, [currentTime]);

    const onStateChange = useCallback(state => {
      console.log(state, '<<<493');
      if (state === 'ended' || 'paused') {
        playerRef.current
          ?.getCurrentTime()
          .then(currentTime => setCurrentTime(currentTime));
      }
    }, []);

    console.log(completionVideo, '<<<509');

    if (completionVideo > 80) {
      completeATask(data.aktivitas_detail_id, id);

      console.log('complete');
    }

    return (
      <View style={{flex: 1, backgroundColor: 'green'}}>
        <TaskTitle navigation={navigation} data={data} />
        <YoutubePlayer
          webViewStyle={{
            backgroundColor: 'grey',
            padding: 0,
            margin: 0,
            flexGrow: 1,
          }}
          height={200}
          play={playing}
          videoId={content != null ? content.content : null}
          onChangeState={onStateChange}
          ref={playerRef}
        />

        <View style={{backgroundColor: 'red', flex: 1}}>
          <View style={tabStyle.parent}>
            <View
              style={[
                tabStyle.tab,
                tabActive == 'description' ? tabStyle.tabActive : null,
              ]}>
              <TouchableOpacity
                style={[tabStyle.menu]}
                onPress={() => {
                  setTabActive('description');
                }}>
                <Text>Deskripsi</Text>
              </TouchableOpacity>
            </View>
            <View
              style={[
                tabStyle.tab,
                tabActive == 'discussion' ? tabStyle.tabActive : null,
              ]}>
              <TouchableOpacity
                style={[tabStyle.menu]}
                onPress={() => {
                  setTabActive('discussion');
                }}>
                <Text>Diskusi</Text>
              </TouchableOpacity>
            </View>
          </View>
          <ScrollView contentContainerStyle={{flex: 1}}>
            <View style={{width: '100%', flex: 1, backgroundColor: 'white'}}>
              {tabActive == 'description' && (
                <ScrollView nestedScrollEnabled={true}>
                  <Text style={[general.contentPadding]}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    doeiusmod tempor incididunt ut labore et dolore Lorem ipsum
                    dolor sit amet, consectetur adipiscing elit, sed doeiusmod
                    tempor incididunt ut labore et dolore Lorem ipsum dolor sit
                    amet, consectetur adipiscing elit, sed doeiusmod tempor
                    incididunt ut labore et dolore Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed doeiusmod tempor incididunt
                    ut labore et dolore Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed doeiusmod tempor incididunt ut labore
                    et dolore Lorem ipsum dolor sit amet, consectetur adipiscing
                    elit, sed doeiusmod tempor incididunt ut labore et dolore{' '}
                  </Text>
                </ScrollView>
              )}
              {tabActive == 'discussion' && <Discussion />}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }

  function Read({navigation, data}) {
    const [visualContent, setVisualContent] = useState(null);
    const [flag, setFlag] = useState(0);
    useEffect(async () => {
      axios({
        url:
          baseUrl +
          '/v1/1/event/theory/' +
          data.aktivitas_detail_id +
          '/content',
        type: 'get',
        headers: {
          token: await AsyncStorage.getItem('userToken'),
        },
      })
        .then(function (res) {
          // console.log(res.data.result, '<<<<466');
          setVisualContent(res.data.result.content);
        })
        .catch(function (err) {
          console.log(err.response);
        });
    }, []);

    const isCloseToBottom = ({
      layoutMeasurement,
      contentOffset,
      contentSize,
    }) => {
      const paddingToBottom = 20;
      return (
        layoutMeasurement.height + contentOffset.y >=
        contentSize.height - paddingToBottom
      );
    };

    return (
      <View style={{flex: 1}}>
        <TaskTitle navigation={navigation} data={data} />
        <ScrollView
          style={[general.contentPadding]}
          onScroll={({nativeEvent}) => {
            if (isCloseToBottom(nativeEvent)) {
              if (flag < 1) {
                completeATask(data.aktivitas_detail_id, id);
              }
              setFlag(flag + 1);
            }
          }}
          scrollEventThrottle={400}>
          <Text>{visualContent}</Text>
        </ScrollView>
      </View>
    );
  }

  function Download({navigation, data}) {
    const [pdfContent, setPdfContent] = useState(null);
    useEffect(async () => {
      axios({
        url:
          baseUrl +
          '/v1/1/event/theory/' +
          data.aktivitas_detail_id +
          '/content',
        type: 'get',
        headers: {
          token: await AsyncStorage.getItem('userToken'),
        },
      })
        .then(function (res) {
          console.log(res.data.result, '<<<<633');
          setPdfContent(res.data.result.content);
        })
        .catch(function (err) {
          console.log(err.response);
        });
    }, []);

    function historyDownload() {
      //Function to check the platform
      //If iOS the start downloading
      //If Android then ask for runtime permission
      if (Platform.OS === 'ios') {
        this.downloadHistory();
      } else {
        try {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              title: 'storage title',
              message: 'storage_permission',
            },
          ).then(granted => {
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              //Once user grant the permission start downloading
              console.log('Storage Permission Granted.');
              downloadHistory();
            } else {
              Alert.alert('storage_permission');
            }
          });
        } catch (err) {
          //To handle permission related issue
          console.log('error', err);
        }
      }
    }

    async function downloadHistory() {
      const {config, fs} = RNFetchBlob;
      let PictureDir = fs.dirs.PictureDir;
      let date = new Date();
      let options = {
        fileCache: true,
        addAndroidDownloads: {
          //Related to the Android only
          useDownloadManager: true,
          notification: true,
          mediaScannable: true,
          path:
            PictureDir +
            '/me_' +
            Math.floor(date.getTime() + date.getSeconds() / 2),
          description: 'File Proedu',
        },
      };
      config(options)
        .fetch('GET', pdfContent)
        .then(res => {
          //Showing alert after successful downloading
          console.log('res -> ', JSON.stringify(res));
          Alert.alert('', 'File Berhasil Diunduh');
        });
    }

    return (
      <View style={{flex: 1}}>
        <TaskTitle navigation={navigation} data={data} />
        <View
          style={[general.contentPadding, {flex: 1, paddingHorizontal: 30}]}>
          <View
            style={[
              general.inline,
              {flex: 1, justifyContent: 'center', alignItems: 'center'},
            ]}>
            <IconDoc style={{marginRight: 10}} />
            <Text>Materi Bacaan Pembelajaran Daring yang Efektif.PDF</Text>
          </View>
          <Button.filledButton
            title="Download"
            onPress={() => {
              historyDownload();
              completeATask(data.aktivitas_detail_id, id);
            }}
          />
        </View>
      </View>
    );
  }

  function Quiz({navigation, data}) {
    const [quizData, setQuizData] = useState(null);
    const [loading, setLoading] = useState(true);
    const quiz = StyleSheet.create({
      container: {
        flex: 1,
      },
      detail: {
        borderWidth: 1,
        borderColor: colors.color.bgColor,
        padding: 15,
        width: 200,
        marginTop: 20,
      },
      info: {
        justifyContent: 'space-between',
        marginVertical: 5,
      },
      greeting: {
        fontWeight: 'bold',
      },
      topic: {
        color: colors.color.grey,
      },
      bottomAction: {
        padding: 20,
        elevation: 3,
        borderTopWidth: 1,
        borderTopColor: 'transparent',
      },
    });

    const [quizResult, setQuizResult] = useState(null);

    if (quizStatus != undefined && quizStatus != null) {
      setTaskToComplete(quizStatus);
    }

    useEffect(async () => {
      axios({
        url:
          'https://api-sandbox.bank-soal.proedu.id/users/questions/' +
          data.aktivitas_detail_id +
          '/' +
          data.ps_id +
          '/' +
          data.EventId,
        method: 'get',
        headers: {
          Authorization: await AsyncStorage.getItem('userToken'),
        },
      })
        .then(function (res) {
          console.log(res, 'quizdataaa');
          setQuizData(res.data);
          setLoading(false);
        })
        .catch(function (err) {
          console.log(err.response.data.data, '<<<809');
          if (
            err.response.data.message == 'You have submitted this assignment !'
          ) {
            setQuizResult(err.response.data.data);
          }
        });
    }, []);

    console.log(quizData, '<<<<<811');

    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <TaskTitle navigation={navigation} data={data} />

        <View
          style={[general.contentPadding, general.centerBoth, quiz.container]}>
          {!loading ? (
            <>
              <Image
                style={{width: 200, height: 100, resizeMode: 'contain'}}
                source={{
                  uri: 'https://quadlayers.com/wp-content/uploads/2020/02/quiz-plugins-for-WordPress.png',
                }}
              />
              <Text style={[quiz.greeting]}>Hai Guru, Ada Soal Untuk Anda</Text>
              <Text style={[quiz.topic]}>
                {'Tema : ' + quizData.aktivitas_detail_title}
              </Text>
              <View style={[quiz.detail]}>
                <View style={[general.inline, quiz.info]}>
                  <Text>Jumlah Soal</Text>
                  <Text>{quizData.bank_soal.length}</Text>
                </View>
                <View style={[general.inline, quiz.info]}>
                  <Text>Total Waktu Pengerjaan</Text>
                  <Text>{quizData.completion_time + ' menit'}</Text>
                </View>
              </View>
            </>
          ) : null}

          {/* {"aktivitas_detail_id": 45, "aktivitas_detail_title": "Quiz", "ev_id": 6, "jumlah_soal": 0, "ps_id": 17, "total_false_answer": 0, "total_score": 100, "total_true_answer": 0, "type": "EXAM"} */}

          {quizResult && (
            <View
              style={[
                general.contentPadding,
                general.centerBoth,
                quiz.container,
              ]}>
              <Text style={[quiz.greeting]}>Ujian Telah Selesai!</Text>
              <Text style={[quiz.topic]}>
                Anda telah menyelesaikan ujian. Lihat hasilnya dibawah ini.
                Semoga hasilnya memuaskan!
              </Text>
              <Text style={{marginTop: 20}}>Nilai</Text>
              <View style={[general.inline, {alignItems: 'flex-end'}]}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: colors.color.greenPrimary,
                    fontSize: 32,
                  }}>
                  {quizResult.total_score}
                </Text>
                <Text
                  style={{
                    fontWeight: 'bold',
                    lineHeight: 32,
                    color: colors.color.grey,
                  }}>
                  /100
                </Text>
              </View>
              <View style={[quiz.detail]}>
                <View style={[general.inline, quiz.info]}>
                  <Text>Benar</Text>
                  <Text>{quizResult.total_true_answer}</Text>
                </View>
                <View style={[general.inline, quiz.info]}>
                  <Text>Salah</Text>
                  <Text style={{color: colors.color.red}}>
                    {quizResult.total_false_answer}
                  </Text>
                </View>
              </View>
            </View>
          )}
        </View>

        <View style={[quiz.bottomAction]}>
          <Button.filledButton
            title="Mulai Ujian"
            onPress={() => {
              parentNavigation.navigate('QuizStack', {
                id: id,
                questionBank: quizData.bank_soal,
                quizData: quizData,
              });
            }}
            disabled={!quizData}
          />
        </View>
      </View>
    );
  }

  function CustomDrawerContent(props) {
    // props.dataCollection[0].aktivitas_detail_id
    const drawer = StyleSheet.create({
      content: {
        padding: 15,
      },
      title: {
        fontWeight: 'bold',
      },
      item: {
        backgroundColor: colors.color.greenShape,
      },
      list: {
        marginRight: 5,
        fontWeight: 'bold',
      },
      taskTitle: {
        fontWeight: 'bold',
      },
      infoJP: {
        alignItems: 'center',
        marginTop: 5,
      },
      txtActive: {
        color: 'white',
      },
      txt: {
        color: 'black',
      },
    });

    const changeTask = menu => {
      setTask(menu);
      props.navigation.navigate(menu);
    };

    return (
      <DrawerContentScrollView {...props} style={{padding: 0}}>
        <View style={[drawer.content]}>
          <Text style={[drawer.title]}>Task Anda</Text>
        </View>
        {taskList?.map((taskItem, key) => {
          return (
            <View key={key} style={{marginBottom: 10}}>
              <Text style={{padding: 15}}>{taskItem.title}</Text>
              {taskItem.sub.map((item, i) => {
                return (
                  <TouchableOpacity
                    key={i}
                    style={[
                      drawer.content,
                      item.aktivitas_detail_id == task ? drawer.item : null,
                      general.inline,
                    ]}
                    onPress={() => {
                      changeTask(String(item.aktivitas_detail_id));
                    }}>
                    {item.TaskFinishStatus == 'NONE' ? (
                      <Text
                        style={[
                          drawer.list,
                          item.aktivitas_detail_id == task
                            ? drawer.txtActive
                            : drawer.txt,
                        ]}>
                        {i + 1}.{' '}
                      </Text>
                    ) : (
                      <IconCheck
                        height={14}
                        width={14}
                        style={{marginRight: 10}}
                      />
                    )}

                    <View>
                      <Text
                        style={[
                          drawer.taskTitle,
                          item.aktivitas_detail_id == task
                            ? drawer.txtActive
                            : drawer.txt,
                          item.TaskFinishStatus != 'NONE'
                            ? {lineHeight: 14}
                            : null,
                        ]}>
                        {item.title}
                      </Text>
                      <View style={[general.inline, drawer.infoJP]}>
                        {item.aktivitas_detail_id == task ? (
                          <IconClockWhite
                            width={16}
                            height={16}
                            style={{marginRight: 5}}
                          />
                        ) : (
                          <IconClock
                            width={16}
                            height={16}
                            style={{marginRight: 5}}
                          />
                        )}
                        <Text
                          style={[
                            item.aktivitas_detail_id == task
                              ? drawer.txtActive
                              : drawer.txt,
                          ]}>
                          {item.resType}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </View>
          );
        })}
      </DrawerContentScrollView>
    );
  }

  const Drawer = createDrawerNavigator();

  function ContentGenerator({navigation, route}) {
    // console.log('ini content yang didapat : ', route);
    // console.log('dan ini task list yang aktif : ', task);
    setTask(task);
    let type = route.params.data.type;
    let resType = route.params.data.resType;
    return (
      <>
        {type == 'EXAM' && (
          <Quiz navigation={navigation} data={route.params.data} />
        )}
        {resType == 'VIDEO_EVENT' && (
          <Watch navigation={navigation} data={route.params.data} />
        )}
        {resType == 'VISUAL' && (
          <Read navigation={navigation} data={route.params.data} />
        )}
        {type == 'THEORY' &&
          resType != 'VISUAL' &&
          resType != 'VIDEO_EVENT' && (
            <Download navigation={navigation} data={route.params.data} />
          )}
      </>
    );
  }

  function MyDrawer() {
    const drawer = StyleSheet.create({
      content: {
        padding: 15,
      },
      title: {
        fontWeight: 'bold',
      },
      item: {
        backgroundColor: colors.color.greenShape,
      },
    });
    let collectData = [];
    taskList?.map((v, k) => {
      v.sub.map((val, key) => {
        collectData.push(val);
      });
    });
    return (
      <Drawer.Navigator
        drawerContent={props => (
          <CustomDrawerContent {...props} dataCollection={collectData} />
        )}>
        {collectData.map((item, key) => {
          return (
            <Drawer.Screen
              key={key}
              name={String(item.aktivitas_detail_id)}
              component={ContentGenerator}
              initialParams={{data: item}}
            />
          );
        })}
      </Drawer.Navigator>
    );
  }

  return (
    <NavigationContainer independent={true}>
      {loading ? (
        <></>
      ) : (
        <>
          <View>
            <Heading />
            <CertificateOption />
            <CourseTitle />
          </View>
          <MyDrawer />
        </>
      )}
    </NavigationContainer>
  );
}
