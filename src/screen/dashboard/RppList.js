import React, {useState, useEffect} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {colors} from '../../styling';
import ContentLoader, {Rect} from 'react-content-loader/native';
// icon
import clockIcon from '../../assets/icon/clockgreen.png';
import clockWarning from '../../assets/icon/clockWarning.png';
import exploreSeminar from '../../assets/icon/eksplorSeminar.png';
import exploreECourse from '../../assets/icon/eksplorOnlineCourse.png';
import plusIcon from '../../assets/icon/plus.png';
import { baseUrl } from './SeminarList';

const Loader = () => {
  return (
    <View style={[styling.loaderContainer]}>
      <ContentLoader viewBox="0 0 400 235" backgroundColor={colors.color.grey2}>
        <Rect x="0" y="0" rx="4" ry="4" width="255" height="30" />
        <Rect x="0" y="45" rx="4" ry="4" width="155" height="185" />
        <Rect x="160" y="45" rx="4" ry="4" width="155" height="185" />
        <Rect x="320" y="45" rx="4" ry="4" width="155" height="185" />
      </ContentLoader>
    </View>
  );
};

function ListOfItems({item, title, navigation}) {
  let items = item;
  let html;
  if (items.length < 1) {
    html = (
      <View
        style={{
          width: 155,
          height: 212,
          borderWidth: 1,
          borderStyle: 'dashed',
          borderColor: colors.color.grey,
          borderRadius: 1,
          justifyContent: 'center',
          alignItems: 'center',
          marginLeft: 10,
        }}>
        <Image source={plusIcon} />
        <Text style={{color: colors.color.grey, marginTop: 15}}>Buat RPP</Text>
      </View>
    );
  } else {
    html = items.map((item, key) => (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('SeminarStack', {
            id: item.EventId == undefined ? item.id : item.EventId,
          });
        }}
        key={key}>
        <View style={styles.card}>
          <View style={styles.cardHead}>
            <Image style={styles.cardHeadImg} source={{uri: item.thumbnail}} />
          </View>
          <View style={styles.cardBody}>
            <Text style={styles.cardBodyTitle} numberOfLines={2}>
              {item.title}
            </Text>
            <View style={styles.inline}>
              <Image style={styles.clockIcon} source={clockIcon} />
            </View>
            <View style={[styles.inline, {marginTop: 15}]}>
              <Image style={styles.clockIcon} source={clockWarning} />
              <Text style={styles.txtWarning}>2 Jam 30 Menit Lagi</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    ));
  }
  return (
    <View
      style={[
        styling.listContainer,
        styling.horizontalHeight,
        items.length < 1 ? styling.emptyList : null,
      ]}>
      <Text
        style={{
          paddingHorizontal: 10,
          fontWeight: 'bold',
          fontSize: 14,
          marginBottom: 15,
        }}>
        {title}
      </Text>
      <ScrollView horizontal={true}>{html}</ScrollView>
    </View>
  );
}

function RppGet({token, navigation}) {
  const [loading, setLoading] = useState(true);
  const [listData, setListData] = useState(null);
  let title;
  let url;
  let parent;
  let view = 'seminar';
  if (seminar != undefined) {
    parent = seminar;
  }
  switch (type) {
    case 'SEMINAR':
      title = 'Seminar Akan Datang';
      url =
        baseUrl+'/v2/event/list?type=single&typeevent=seminar&subtype=none&sort=desc&status=all&page=1&limite=100';
      break;
    default:
      break;
  }
  useEffect(() => {
    axios({
      url: url,
      method: 'GET',
      headers: {
        token: token,
      },
    })
      .then(function (res) {
        Array.isArray(res.data.result)
          ? setListData(res.data.result)
          : setListData(res.data.result.content);
        setLoading(false);
      })
      .catch(function (err) {
        console.log(err.response);
      });
  }, []);
  return (
    <View style={{flex: 1}}>
      {loading ? (
        <Loader />
      ) : (
        <ListOfItems item={listData} title={title} navigation={navigation} />
      )}
    </View>
  );
}

export function RppList() {
  const [token, setToken] = useState(null);
  useEffect(() => {
    setTimeout(async () => {
      let token = await AsyncStorage.getItem('userToken');
      setToken(token);
    }, 250);
  }, []);
  return (
    <View style={{width: '100%', flex: 1}}>
      {token == null ? (
        <Loader />
      ) : (
        <View>
          <ListOfItems item={[]} title="RPP Terakhir Dibuat" />
        </View>
      )}
    </View>
  );
}

export function MySeminarList({navigation}) {
  const [token, setToken] = useState(null);
  useEffect(() => {
    setTimeout(async () => {
      let token = await AsyncStorage.getItem('userToken');
      setToken(token);
    }, 250);
  }, []);
  return (
    <View style={{width: '100%', flex: 1}}>
      {token == null ? null : (
        <View>
          <SeminarGet token={token} type="MY_SEMINAR" navigation={navigation} />
          <SeminarGet
            token={token}
            type="MY_COURSE_ONLINE"
            navigation={navigation}
          />
          <SeminarGet
            token={token}
            type="MY_EVENT_DONE"
            navigation={navigation}
          />
        </View>
      )}
    </View>
  );
}

export function SeminarList({navigation}) {
  const [token, setToken] = useState(null);
  useEffect(() => {
    setTimeout(async () => {
      let token = await AsyncStorage.getItem('userToken');
      setToken(token);
    }, 250);
  }, []);
  return (
    <View style={{width: '100%', flex: 1}}>
      {token == null ? null : (
        <View>
          <SeminarGet token={token} type="SEMINAR" navigation={navigation} />
          <SeminarGet token={token} type="PACKAGE" navigation={navigation} />
          <SeminarGet
            token={token}
            type="COURSE_ONLINE"
            navigation={navigation}
          />
        </View>
      )}
    </View>
  );
}

export function SeminarRecommendation({navigation, seminar}) {
  const [token, setToken] = useState(null);
  useEffect(() => {
    setTimeout(async () => {
      let token = await AsyncStorage.getItem('userToken');
      setToken(token);
    }, 250);
  }, []);
  return (
    <View style={{width: '100%', flex: 1}}>
      {token == null ? null : (
        <View>
          <SeminarGet
            navigation={navigation}
            token={token}
            seminar={seminar}
            type="SEMINAR_RELATED"
          />
        </View>
      )}
    </View>
  );
}

const styling = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  loaderContainer: {
    width: '100%',
    height: 235,
    paddingHorizontal: 20,
    paddingVertical: 0,
    marginTop: 0,
  },
  listContainer: {
    width: '100%',
    paddingHorizontal: 10,
    marginVertical: 15,
  },
  horizontalHeight: {
    height: 230,
  },
  emptyList: {
    height: 'auto',
    flex: 1,
    marginBottom: 50,
  },
  emptyText: {
    fontStyle: 'italic',
    marginHorizontal: 10,
  },
});

const styles = StyleSheet.create({
  inline: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtWarning: {
    color: colors.color.red,
    fontSize: 10,
  },
  card: {
    height: 190,
    width: 155,
    marginHorizontal: 10,
    alignItems: 'center',
    elevation: 35,
  },
  cardHead: {
    width: '100%',
    height: 80,
  },
  cardHeadImg: {
    width: '100%',
    height: 80,
    resizeMode: 'cover',
  },
  cardBody: {
    backgroundColor: 'white',
    width: '100%',
    flex: 1,
    padding: 5,
  },
  cardBodyTitle: {
    fontSize: 12,
    fontWeight: 'bold',
    marginBottom: 10,
    height: 45,
  },
  cardBodyData: {
    fontSize: 12,
  },
  clockIcon: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
});
