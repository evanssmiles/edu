import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {colors} from '../../styling';
import {Input, Button} from '../../component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

import BouncyCheckbox from 'react-native-bouncy-checkbox';
import IconClock from '../../assets/icon/iconClockGrey.svg';
import {CartLoading} from './CartDetail';
import {baseUrl} from './SeminarList';

const style = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  content: {
    padding: 20,
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    marginRight: 10,
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
  heading: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  referral: {
    textAlign: 'center',
    fontSize: 32,
    fontWeight: 'bold',
    marginVertical: 25,
  },
  warningTxt: {
    color: colors.color.red,
    lineHeight: 16,
  },
  spaceY: {
    marginVertical: 10,
  },
  absoluteBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    padding: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'white',
  },
});

const header = StyleSheet.create({
  container: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    backgroundColor: 'white',
  },
  btn: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: colors.color.grey2,
    marginRight: 10,
  },
  txt: {
    color: colors.color.grey,
  },
  txtActive: {
    color: colors.color.greenPrimary,
  },
  btnActive: {
    borderColor: colors.color.greenPrimary,
  },
});

const body = StyleSheet.create({
  content: {
    padding: 20,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: colors.color.grey3,
  },
  contentNew: {
    backgroundColor: colors.color.greenComponent,
  },
  preview: {
    height: 80,
    width: 80,
    marginRight: 15,
    backgroundColor: colors.color.grey2,
    borderRadius: 15,
  },
  previewDesc: {
    flex: 1,
  },
  head: {
    marginBottom: 15,
  },
  main: {},
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  desc: {
    fontSize: 14,
    // fontWeight: 'bold',
  },
  spaceY: {
    marginVertical: 5,
  },
});

export default function PaymentDirect({navigation, data}) {
  // console.log(data.seminar.data.eventId, '<<<133');

  const [loading, setLoading] = useState(true);
  const [haveReferral, setHaveReferral] = useState(false);
  const [referral, setReferral] = useState('');
  const [error, setError] = useState('');
  const [token, setToken] = useState('');

  const [item, setItem] = useState(null);

  useEffect(async () => {
    let getToken = await AsyncStorage.getItem('userToken');
    setToken(getToken);
    axios({
      url: `${baseUrl}/v1/transaction/event/summary/buynow`,
      method: 'get',
      headers: {
        token: getToken,
      },
    })
      .then(function (res) {
        if (res.status < 300) {
          setItem(res.data.result);
        }
        setLoading(false);
      })
      .catch(function (err) {
        console.log(err, '<<<161');
      });
  }, []);

  console.log(item, '<<1666');

  function nextStep() {
    navigation.navigate('PaymentStack', {
      screen: 'Pembayaran',
      data: {item: item.DetailTransaction, buyNow: 'buynow'},
    });
  }

  if (loading) {
    return <CartLoading />;
  }

  return (
    item && (
      <View style={[style.container]}>
        <View style={[header.container]}>
          <Text>Pilih Pembayaran</Text>
        </View>
        <ScrollView contentContainerStyle={{paddingBottom: 150}}>
          <View style={[body.content]}>
            <View style={[style.inline, body.main]}>
              <Image style={[body.preview]} />
              <View style={[body.previewDesc]}>
                <Text style={[body.desc, body.spaceY]} numberOfLines={1}>
                  {item?.DetailTransaction[0].title}
                </Text>
                <View style={[style.inline, body.spaceY]}>
                  <IconClock width={12} height={12} style={{marginRight: 10}} />
                  <Text style={{fontSize: 12}}>
                    {item?.DetailTransaction[0].seminarDate}
                  </Text>
                </View>
                <View
                  style={[style.inline, {alignItems: 'center', marginTop: 10}]}>
                  <Text
                    style={{
                      flex: 1,
                      textAlign: 'right',
                      marginRight: 10,
                      fontSize: 12,
                      textDecorationLine: 'line-through',
                    }}>
                    {item?.DetailTransaction[0].oriPrice}
                  </Text>
                  <Text
                    style={{
                      textAlign: 'right',
                      fontSize: 16,
                      fontWeight: 'bold',
                    }}>
                    {item?.DetailTransaction[0].payPrice}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View
            style={[
              {
                backgroundColor: 'white',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
                padding: 20,
                marginTop: 15,
              },
            ]}>
            <TextInput
              editable
              placeholder="Masukkan kode promo"
              style={{
                borderWidth: 0,
                borderBottomWidth: 1,
                borderColor: colors.color.grey2,
                marginRight: 15,
                flex: 1,
                padding: 0,
              }}
            />
            <TouchableOpacity
              style={{
                borderColor: colors.color.greenPrimary,
                borderWidth: 1,
                borderRadius: 15,
                paddingVertical: 5,
                paddingHorizontal: 20,
              }}
              disabled={true}>
              <Text style={{color: colors.color.greenPrimary}}>Terapkan</Text>
            </TouchableOpacity>
          </View>
          <Text
            style={{
              paddingHorizontal: 20,
              marginVertical: 10,
              fontWeight: 'bold',
            }}>
            Rincian Pembayaran
          </Text>
          <View
            style={{
              paddingHorizontal: 20,
              paddingVertical: 15,
              backgroundColor: 'white',
            }}>
            <View
              style={[
                style.inline,
                {justifyContent: 'space-between', marginBottom: 10},
              ]}>
              <Text>Total Harga (1 Item)</Text>
              <Text>{item?.DetailTransaction[0].payPrice}</Text>
            </View>
            <TouchableOpacity
              style={[style.inline, {justifyContent: 'space-between'}]}>
              <Text>Biaya Layanan</Text>
              <Text style={{color: colors.color.grey2}}>Pilih Pembayaran</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <View style={[style.absoluteBottom]}>
          <View
            style={[
              style.inline,
              {
                justifyContent: 'space-between',
                alignItems: 'flex-start',
                marginBottom: 10,
              },
            ]}>
            <Text>Total Harga</Text>
            <View style={{justifyContent: 'flex-end'}}>
              <Text
                style={[
                  {fontSize: 24, fontWeight: 'bold', textAlign: 'right'},
                ]}>
                {item?.DetailTransaction[0].payPrice}
              </Text>
              <Text style={[{color: colors.color.grey, textAlign: 'right'}]}>
                Dapatkan {item.TotalPoint} poin
              </Text>
            </View>
          </View>
          <Button.filledButton
            style={[]}
            title="Pilih Pembayaran"
            onPress={() => nextStep()}
          />
        </View>
      </View>
    )
  );
}

/**
notes
percobaan beli sekarang dan tidak ada barang di Cart
bug 
tidak bisa pilih service fee saat beli sekarang
salin virtual account 
lihat detail 
cara pembayaran (nunggu mas aryo)
balik ke halaman detail tombol beli sekarang masih bisa, harusnya disable aja 22nya
e wallet blm bisa
udah checkout, masih masuk rekomendasi

 * percobaan masukin keranjang, beli satu item
 *
 * barang sudah di keranjang beli sekarang (tidak bisa masuk ke cart biasa)
 * cara pembayaran masih dummy
 * cart nyampur dgn beli sekarang, di detail Pembelian
 *
 * TransactionDetail.js salin VA blm, lihat detail belom
 *
 * beli sekarang, blm bayar, tapi mask detail transaction, walaupun jumlahnya ga nambah
 */
