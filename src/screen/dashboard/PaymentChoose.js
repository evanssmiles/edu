import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {colors} from '../../styling';
import {Input, Button} from '../../component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

import BouncyCheckbox from 'react-native-bouncy-checkbox';
import IconInfoRed from '../../assets/icon/infoRed.svg';
import IconInfoGrey from '../../assets/icon/infoGrey.svg';
import IconSuccess from '../../assets/icon/checkedCircleGreen.svg';
import IconClock from '../../assets/icon/iconClockGrey.svg';
import IconTrash from '../../assets/icon/trash.svg';
import IconPlus from '../../assets/icon/plus.svg';
import McLogo from '../../assets/img/mastercard-light.svg';
import VsLogo from '../../assets/img/visa-light.png';
import GopayLogo from '../../assets/img/gopayLogo.png';
import {CartLoading} from './CartDetail';
import {baseUrl} from './SeminarList';

const style = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  content: {
    padding: 20,
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    marginRight: 10,
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
  heading: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  referral: {
    textAlign: 'center',
    fontSize: 32,
    fontWeight: 'bold',
    marginVertical: 25,
  },
  warningTxt: {
    color: colors.color.red,
    lineHeight: 16,
  },
  spaceY: {
    marginVertical: 10,
  },
  absoluteBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    padding: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'white',
  },
});

const floatBtn = StyleSheet.create({
  button: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 20,
    elevation: 7,
    backgroundColor: 'white',
    width: 180,
  },
});

const header = StyleSheet.create({
  container: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    backgroundColor: 'white',
  },
  btn: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: colors.color.grey2,
    marginRight: 10,
  },
  txt: {
    color: colors.color.grey,
  },
  txtActive: {
    color: colors.color.greenPrimary,
  },
  btnActive: {
    borderColor: colors.color.greenPrimary,
  },
});

const body = StyleSheet.create({
  content: {
    padding: 20,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: colors.color.grey3,
  },
  contentNew: {
    backgroundColor: colors.color.greenComponent,
  },
  preview: {
    height: 80,
    width: 80,
    marginRight: 15,
    backgroundColor: colors.color.grey2,
    borderRadius: 15,
  },
  previewDesc: {
    flex: 1,
  },
  head: {
    marginBottom: 15,
  },
  main: {},
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  desc: {
    fontSize: 14,
    // fontWeight: 'bold',
  },
  spaceY: {
    marginVertical: 5,
  },
});

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}

export default function PaymentChoose({navigation, data}) {
  const [item, setItem] = useState(data);
  console.log(item, '<<<<<159');
  const [paymentOpt, setPaymentOpt] = useState(null);
  const [paymentSelected, setPaymentSelected] = useState(null);
  const [serviceFee, setServiceFee] = useState(null);
  const [totalHarga, setTotalHarga] = useState(0);
  const [loading, setLoading] = useState(true);

  const updatePrice = async ({id}) => {
    console.log(id, '<<<<166');
    let getToken = await AsyncStorage.getItem('userToken');
    axios({
      url: baseUrl + '/v1/transaction/pay/' + id + '/priceservice',
      method: 'GET',
      headers: {
        token: getToken,
      },
    })
      .then(function (res) {
        if (res.status < 300) {
          console.log(res.data.result, '<<<<<179');
          setServiceFee(res.data.result.serviceFeePrice);
          setTotalHarga(res.data.result.paidPrice);
        }
        setLoading(false);
      })
      .catch(function (err) {
        console.log('error is : ', err.response.data);
        setLoading(false);
      });
  };
  const updatePriceBuyNow = async ({id}) => {
    console.log(id, '<<<<166');
    let getToken = await AsyncStorage.getItem('userToken');
    axios({
      url: baseUrl + '/v1/transaction/pay/' + id + '/priceservice?type=buynow',
      method: 'GET',
      headers: {
        token: getToken,
      },
    })
      .then(function (res) {
        if (res.status < 300) {
          console.log(res.data.result, '<<<<<179');
          setServiceFee(res.data.result.serviceFeePrice);
          setTotalHarga(res.data.result.paidPrice);
        }
        setLoading(false);
      })
      .catch(function (err) {
        console.log('error is : ', err.response.data);
        setLoading(false);
      });
  };

  const totalTotal = arr => {
    const newArr = arr
      .map(e => e.payPrice)
      .map(e => +e.substring(3) * 1000)
      .reduce((a, b) => a + b, 0);

    return newArr;
  };

  const checkRadio = (parent, child) => {
    let collect = paymentOpt;
    collect.map((val, key) => {
      val.mercent.map((v, k) => {
        v.checked = false;
      });
    });
    collect[parent].mercent[child].checked = true;
    setPaymentOpt(collect);
    setPaymentOpt([...paymentOpt]);
    setPaymentSelected(collect[parent].mercent[child]);
    if (item.buyNow) {
      updatePriceBuyNow({id: collect[parent].mercent[child].mercentId});
    } else {
      updatePrice({id: collect[parent].mercent[child].mercentId});
    }
  };

  useEffect(async () => {
    let getToken = await AsyncStorage.getItem('userToken');
    axios({
      url: baseUrl + '/v1/transaction/pay/methode',
      method: 'GET',
      headers: {
        token: getToken,
      },
    })
      .then(function (res) {
        if (res.status < 300) {
          console.log(res.data);
          let collect = res.data.result;
          collect.map((val, key) => {
            val.mercent.map((v, k) => {
              v.checked = false;
            });
          });
          setPaymentOpt(collect);
        }
        setLoading(false);
      })
      .catch(function (err) {
        console.log('error is : ', err.response);
        setLoading(false);
      });
  }, []);

  function nextStep() {
    console.log(paymentSelected, '<<<<<244');
    navigation.navigate('PaymentStack', {
      data: {item: data.item, payment: paymentSelected, buyNow: data.buyNow},
      screen: 'Waiting',
    });
  }

  if (loading) {
    return <CartLoading />;
  }

  console.log(serviceFee, '<<<<165');

  return (
    <View style={[style.container]}>
      <View style={[header.container]}>
        <Text>Pembayaran</Text>
      </View>
      <ScrollView contentContainerStyle={{paddingBottom: 200}}>
        <View style={{flex: 1}}>
          {paymentOpt != null && (
            <View style={{flex: 1}}>
              {paymentOpt.map((val, key) => {
                return (
                  <View key={key}>
                    <Text
                      style={{
                        paddingHorizontal: 20,
                        marginVertical: 10,
                        fontWeight: 'bold',
                      }}>
                      {val.Transactiontype}
                    </Text>
                    {val.mercent.map((v, k) => {
                      return (
                        <View
                          key={k}
                          style={{
                            paddingHorizontal: 20,
                            paddingVertical: 15,
                            backgroundColor: 'white',
                            borderBottomWidth: 1,
                            borderBottomColor: colors.color.grey3,
                          }}>
                          <View
                            style={[
                              style.inline,
                              {marginBottom: 10, alignItems: 'center'},
                            ]}>
                            <BouncyCheckbox
                              size={18}
                              fillColor={colors.color.greenPrimary}
                              unfillColor="#FFFFFF"
                              text=""
                              isChecked={v.checked}
                              disableBuiltInState
                              onPress={() => checkRadio(key, k)}
                              iconStyle={{borderColor: colors.color.grey2}}
                            />
                            <Image
                              source={{uri: v.logo}}
                              style={{
                                marginRight: 5,
                                width: 25,
                                height: 10,
                                resizeMode: 'contain',
                              }}
                            />
                            <Text>{v.name}</Text>
                          </View>
                        </View>
                      );
                    })}
                  </View>
                );
              })}
            </View>
          )}
        </View>
        <Text
          style={{
            paddingHorizontal: 20,
            marginVertical: 10,
            fontWeight: 'bold',
          }}>
          Rincian Pembayaran
        </Text>
        <View
          style={{
            paddingHorizontal: 20,
            paddingVertical: 15,
            backgroundColor: 'white',
          }}>
          <View
            style={[
              style.inline,
              {justifyContent: 'space-between', marginBottom: 10},
            ]}>
            <Text>Total Harga ({data.item.length} Item)</Text>
            <Text>Rp{numberWithCommas(totalTotal(data.item))}</Text>
          </View>
          <TouchableOpacity
            style={[style.inline, {justifyContent: 'space-between'}]}>
            <Text>Biaya Layanan</Text>
            <Text
              style={[
                serviceFee == null
                  ? {color: colors.color.grey2}
                  : {color: 'black'},
              ]}>
              {serviceFee == null ? 'Pilih Pembayaran' : serviceFee}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <View style={[style.absoluteBottom]}>
        <View
          style={[
            style.inline,
            {
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              marginBottom: 10,
            },
          ]}>
          <Text>Total Harga</Text>
          <View style={{justifyContent: 'flex-end'}}>
            <Text
              style={[{fontSize: 24, fontWeight: 'bold', textAlign: 'right'}]}>
              Rp{numberWithCommas(totalTotal(data.item) + +serviceFee)}
            </Text>
            {/* <Text style={[{color: colors.color.grey, textAlign: 'right'}]}>
              Dapatkan 32 poin
            </Text> */}
          </View>
        </View>
        <Button.filledButton
          style={[]}
          title="Bayar Sekarang"
          onPress={() => nextStep()}
          disabled={paymentSelected == null ? true : false}
        />
      </View>
    </View>
  );
}
