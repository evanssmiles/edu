import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import {colors} from '../../styling';
import {
  SeminarList,
  MySeminarList,
  WebinarExplore,
  baseUrl,
} from './SeminarList';
import moment from 'moment';
import DateRangePicker from 'rnv-date-range-picker';
import dateIcon from '../../assets/icon/dateIcon.png';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {SliderWidth} from '../Welcome';
import axios from 'axios';
// import SeminarByDate from '../../component/SeminarByDate';

const {width} = Dimensions.get('window');
const height = width * 0.6;

const general = StyleSheet.create({
  inline: {
    flexDirection: 'row',
  },
});

export default function SeminarExplore({navigation, route}) {
  const [active, setActive] = useState(0);
  const [selectedRange, setRange] = useState({});
  const [showCalendar, setShowCalendar] = useState(false);
  const [banner, setBanner] = useState([]);
  const [seminarsByDate, setSeminarsByDate] = useState([]);
  const [msg, setMsg] = useState('');

  const changePage = ({nativeEvent}) => {
    const slide = Math.ceil(
      nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width,
    );

    if (slide != active) {
      setActive(slide);
    }
  };

  const url = `${baseUrl}/v2/event/list?type=all&typeevent=seminar&subtype=none&sort=desc&status=all&datestart=${selectedRange.firstDate}&dateend=${selectedRange.secondDate}&page=1&limite=100`;

  const bannerUrl = `${baseUrl}/v2/event/list/seminar/carusel`;

  useEffect(async () => {
    let token = await AsyncStorage.getItem('userToken');
    axios({
      url: bannerUrl,
      method: 'GET',
      headers: {
        token: token,
      },
    })
      .then(function (res) {
        setBanner(res.data.result);
      })
      .catch(function (err) {
        console.log(err.response);
      });
  }, []);

  useEffect(async () => {
    let token = await AsyncStorage.getItem('userToken');
    if (selectedRange.firstDate && selectedRange.secondDate) {
      axios({
        url: url,
        method: 'GET',
        headers: {
          token: token,
        },
      })
        .then(function (res) {
          console.log(res.data, '<<<>>>RESULT');
          if (res.data.result.length == 0) {
            setMsg('Event tidak ditemukan');
            setSeminarsByDate([]);
            setShowCalendar(false);
            setTimeout(() => {
              setMsg('');
            }, 1500);
          } else {
            setSeminarsByDate(res.data.result);
            setShowCalendar(false);
          }
        })
        .catch(function (err) {
          console.log(err.response);
        });
    }
  }, [selectedRange]);

  const handlePress = () => {
    setShowCalendar(!showCalendar);
    setSeminarsByDate([]);
  };

  return (
    <View style={{flex: 1, width: '100%'}}>
      <View style={{width, height, marginBottom: 50}}>
        <ScrollView
          onScroll={changePage}
          pagingEnabled
          horizontal={true}
          showsHorizontalScrollIndicator={false}>
          {banner.map((e, i) => (
            <Image
              style={{width, height, resizeMode: 'cover'}}
              source={{
                uri: `${e.thumbnail}`,
              }}
              key={i}
            />
          ))}
        </ScrollView>
        <View style={exploreStyles.dotContainer}>
          {banner.map((e, i) => (
            <Text
              key={i}
              style={i == active ? exploreStyles.dotActive : exploreStyles.dot}>
              ⬤
            </Text>
          ))}
        </View>
      </View>

      {msg != '' && <Text style={{alignSelf: 'center'}}>{msg}</Text>}

      {/* <View style={exploreStyles.dateContainer}>
        <Text style={exploreStyles.date}>{moment().format('LL')}</Text>
        <TouchableOpacity>
          <View style={exploreStyles.calendar}>
            <Image source={dateIcon} />
            <Text style={exploreStyles.dateInput}>MM/DD/YY - MM/DD/YY</Text>
          </View>
        </TouchableOpacity>
      </View> */}

      <ScrollView>
        {showCalendar && (
          <View style={{paddingHorizontal: 20}}>
            <DateRangePicker
              onSelectDateRange={range => {
                setRange(range);
              }}
              responseFormat="YYYY-MM-DD"
              // maxDate={moment()}
              minDate={moment().subtract(30, 'days')}
            />
          </View>
        )}

        {seminarsByDate.length == 0 && (
          <WebinarExplore navigation={navigation} />
        )}

        {/* <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            alignItems: 'center',
            paddingLeft: 23,
          }}>
          {/* {seminarsByDate.map(e => (
            <SeminarByDate navigation={navigation} item={e} key={e.id} />
          ))}
        </View> */}
      </ScrollView>
    </View>
  );
}

export const exploreStyles = StyleSheet.create({
  dot: {
    color: '#888',
    margin: 3,
  },
  dotActive: {
    color: '#00af68',
    margin: 3,
  },
  dotContainer: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 45,
    left: 15,
    alignSelf: 'flex-start',
  },
  dateContainer: {
    height: 82,
    width: 371,
    backgroundColor: '#fff',
    alignSelf: 'center',
    position: 'absolute',
    top: 190,
    borderRadius: 12,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  date: {
    fontSize: 18,
    color: '#232323',
  },
  calendar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dateInput: {
    color: '#6C757C',
    fontSize: 16,
    marginLeft: 5,
  },
});
