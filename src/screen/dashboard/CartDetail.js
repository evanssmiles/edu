import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {colors} from '../../styling';
import {Input, Button} from '../../component';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import IconInfoRed from '../../assets/icon/infoRed.svg';
import IconInfoGrey from '../../assets/icon/infoGrey.svg';
import IconSuccess from '../../assets/icon/checkedCircleGreen.svg';
import IconClock from '../../assets/icon/iconClockGrey.svg';
import IconTrash from '../../assets/icon/trash.svg';
import IconPlus from '../../assets/icon/plus.svg';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {useFocusEffect} from '@react-navigation/native';
import {baseUrl} from './SeminarList';

const style = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  content: {
    padding: 20,
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    marginRight: 10,
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
  heading: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  referral: {
    textAlign: 'center',
    fontSize: 32,
    fontWeight: 'bold',
    marginVertical: 25,
  },
  warningTxt: {
    color: colors.color.red,
    lineHeight: 16,
  },
  spaceY: {
    marginVertical: 10,
  },
  absoluteBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    padding: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'white',
  },
});

const floatBtn = StyleSheet.create({
  button: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 20,
    elevation: 7,
    backgroundColor: 'white',
    width: 180,
  },
});

const header = StyleSheet.create({
  container: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    backgroundColor: 'white',
  },
  btn: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: colors.color.grey2,
    marginRight: 10,
  },
  txt: {
    color: colors.color.grey,
  },
  txtActive: {
    color: colors.color.greenPrimary,
  },
  btnActive: {
    borderColor: colors.color.greenPrimary,
  },
});

const body = StyleSheet.create({
  content: {
    padding: 20,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: colors.color.grey3,
  },
  contentNew: {
    backgroundColor: colors.color.greenComponent,
  },
  preview: {
    height: 80,
    width: 80,
    marginRight: 15,
    backgroundColor: colors.color.grey2,
    borderRadius: 15,
  },
  previewDesc: {
    flex: 1,
  },
  head: {
    marginBottom: 15,
  },
  main: {},
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  desc: {
    fontSize: 14,
    // fontWeight: 'bold',
  },
  spaceY: {
    marginVertical: 5,
  },
});

const circle = {
  height: 5,
  width: 5,
  borderRadius: 5 / 2,
  backgroundColor: colors.color.grey,
  marginHorizontal: 5,
  marginTop: 3,
};

export const CartLoading = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <ActivityIndicator size="large" color={colors.color.greenPrimary} />
    </View>
  );
};
export default function CartDetail({navigation}) {
  const [loading, setLoading] = useState(true);
  const [haveReferral, setHaveReferral] = useState(false);
  const [referral, setReferral] = useState('');
  const [error, setError] = useState('');
  const [selectAll, setSelectAll] = useState(true);
  const [itemToRemove, setItemToRemove] = useState('');
  const [item, setItem] = useState([]);
  const [totalPay, setTotalPay] = useState('');

  const [type, setType] = useState(['Info', 'Aktivitas']);

  const fetchKeranjang = async () => {
    const toke = await AsyncStorage.getItem('userToken');
    axios({
      url: baseUrl + '/v2/transaction/cart',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        token: await AsyncStorage.getItem('userToken'),
      },
    })
      .then(function (response) {
        let collect = response.data.result.DetailTransaction;
        if (Array.isArray(collect)) {
          setItem(collect);
          setTotalPay(response.data.result.TotalPriceChecked);
        }
        setLoading(false);
      })
      .catch(function (error) {
        console.log(error.response);
      });
  };

  useEffect(() => {
    fetchKeranjang();
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      return () => fetchKeranjang();
    }, []),
  );

  const checkRadio = async key => {
    let collect = item;
    axios({
      url:
        baseUrl + '/v2/transaction/cart/' + collect[key].IdDtlTr + '/checked',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        token: await AsyncStorage.getItem('userToken'),
      },
    })
      .then(function (response) {
        setTotalPay(response.data.result.TotalPriceChecked);
      })
      .catch(function (error) {
        console.log(error.response);
      });

    if (collect[key].checked == 'CHECKED' && selectAll) setSelectAll(false);
    collect[key].checked =
      collect[key].checked == 'CHECKED' ? 'UNCHECKED' : 'CHECKED';
    setItem(collect);
    setItem([...item]);
  };

  const checkAll = () => {
    setSelectAll(!selectAll);
    console.log(selectAll);
    let collect = item;
    collect.map((val, key) => {
      return (val.checked = !selectAll ? 'CHECKED' : 'UNCHECKED');
    });
    setItem(collect);
    setItem([...item]);
  };

  const [modalVisible, setModalVisible] = useState(false);

  // const deleteAllCart = async () => {
  //   axios({
  //     url: baseUrl+ '/v2/transaction/cart/remove/all',
  //     method: 'DELETE',
  //     headers: {
  //       'Content-Type': 'application/json',
  //       token: await AsyncStorage.getItem('userToken'),
  //     },
  //   })
  //     .then(function (response) {
  //       setItem([]);
  //       setModalVisible(false);
  //     })
  //     .catch(function (error) {
  //       console.log(error.response);
  //     });
  // };

  const deleteItem = async id => {
    console.log(id, '<<<<250');

    axios({
      url: baseUrl + `/v1/transaction/event/${id}/remove`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        token: await AsyncStorage.getItem('userToken'),
      },
    })
      .then(function (response) {
        console.log(response, '<<<<261');
        const newArr = item.filter(e => e.IdDtlTr !== id);
        setItem(newArr);
        fetchKeranjang();
        setModalVisible(false);
      })
      .catch(function (error) {
        console.log(error.response);
      });
  };

  /**
   * keranjang -> beli -> summary (P Detail)
   *
   */

  const proceedCart = () => {
    let passingData = [];
    item.map((val, key) => {
      if (val.checked == 'CHECKED') {
        passingData.push(val);
      }
    });

    console.log(passingData, '<<<<296 summary');

    navigation.push('PaymentStack', {
      data: {item: passingData},
      screen: 'Summary',
    });
  };

  if (loading) {
    return <CartLoading></CartLoading>;
  }

  return (
    <View style={[style.container]}>
      <Modal
        isVisible={modalVisible}
        onBackdropPress={() => setModalVisible(false)}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View style={{backgroundColor: 'white', padding: 20}}>
            <Text style={{textAlign: 'center', fontSize: 14}}>
              Hapus Event dari keranjang?
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 50,
                justifyContent: 'space-between',
                width: '100%',
              }}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  borderWidth: 1,
                  borderColor: colors.color.greenPrimary,
                  borderRadius: 15,
                  marginHorizontal: 5,
                  paddingVertical: 10,
                }}
                onPress={() => deleteItem(itemToRemove)}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: colors.color.greenPrimary,
                  }}>
                  Ya
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1,
                  borderWidth: 1,
                  backgroundColor: colors.color.greenPrimary,
                  borderRadius: 15,
                  marginHorizontal: 5,
                  paddingVertical: 10,
                  borderWidth: 0,
                }}
                onPress={() => setModalVisible(false)}>
                <Text style={{textAlign: 'center', color: 'white'}}>Tidak</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      {loading ? null : (
        <>
          {item.length > 0 ? (
            <>
              <View style={[header.container]}>
                <BouncyCheckbox
                  size={18}
                  fillColor={colors.color.greenPrimary}
                  unfillColor="#FFFFFF"
                  text=""
                  isChecked={selectAll}
                  disableBuiltInState
                  onPress={() => checkAll()}
                  iconStyle={{borderColor: colors.color.grey2, borderRadius: 5}}
                />
                <Text style={{flex: 1}}>Pilih Semua</Text>
              </View>
              <ScrollView>
                {item.map((val, key) => {
                  return (
                    <View
                      style={[body.content, key == 0 ? body.contentNew : null]}
                      key={key}>
                      <View style={[style.inline, body.main]}>
                        <BouncyCheckbox
                          size={18}
                          fillColor={colors.color.greenPrimary}
                          unfillColor="#FFFFFF"
                          iconStyle={{
                            borderColor: colors.color.grey2,
                            borderRadius: 5,
                          }}
                          isChecked={
                            item[key].checked == 'CHECKED' ? true : false
                          }
                          disableBuiltInState
                          onPress={() => checkRadio(key)}
                        />
                        <Image style={[body.preview]} />
                        <View style={[body.previewDesc]}>
                          <Text
                            style={[body.desc, body.spaceY]}
                            numberOfLines={1}>
                            {val.title}
                          </Text>
                          <View style={[style.inline, body.spaceY]}>
                            <IconClock
                              width={12}
                              height={12}
                              style={{marginRight: 10}}
                            />
                            <Text style={{fontSize: 12}}>
                              {val.seminarDate}
                            </Text>
                          </View>
                          <View
                            style={[
                              style.inline,
                              {alignItems: 'center', marginTop: 10},
                            ]}>
                            <Text
                              style={{
                                flex: 1,
                                textAlign: 'right',
                                marginRight: 10,
                                fontSize: 12,
                                textDecorationLine: 'line-through',
                              }}>
                              {val.oriPrice}
                            </Text>
                            <Text
                              style={{
                                textAlign: 'right',
                                fontSize: 16,
                                fontWeight: 'bold',
                              }}>
                              {val.payPrice}
                            </Text>
                            <IconTrash
                              style={{marginLeft: 10}}
                              onPress={() => {
                                setItemToRemove(val.IdDtlTr);
                                setModalVisible(true);
                              }}
                            />
                          </View>
                        </View>
                      </View>
                    </View>
                  );
                })}
                <TouchableOpacity
                  style={[
                    {
                      backgroundColor: 'white',
                      width: '100%',
                      alignItems: 'center',
                      justifyContent: 'center',
                      flexDirection: 'row',
                      padding: 20,
                    },
                  ]}>
                  <IconPlus width={14} height={14} style={{marginRight: 10}} />
                  <Text style={[style.txtGreen]}>Tambah Lagi</Text>
                </TouchableOpacity>
              </ScrollView>
              <View style={[style.absoluteBottom]}>
                <View
                  style={[
                    style.inline,
                    {
                      justifyContent: 'space-between',
                      alignItems: 'flex-start',
                      marginBottom: 10,
                    },
                  ]}>
                  <Text>Total Harga</Text>
                  <View style={{justifyContent: 'flex-end'}}>
                    <Text
                      style={[
                        {fontSize: 24, fontWeight: 'bold', textAlign: 'right'},
                      ]}>
                      {totalPay}
                    </Text>
                    {/* <Text
                      style={[{color: colors.color.grey, textAlign: 'right'}]}>
                      Dapatkan 32 poin
                    </Text> */}
                  </View>
                </View>
                <Button.filledButton
                  style={[]}
                  title="Beli"
                  onPress={() => proceedCart()}
                />
              </View>
            </>
          ) : (
            <View
              style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <Text>Anda belum menambah apapun ke keranjang</Text>
              <TouchableOpacity
                style={{
                  marginTop: 15,
                  padding: 15,
                  backgroundColor: colors.color.greenPrimary,
                }}
                onPress={() =>
                  navigation.navigate('SeminarStack', {
                    screen: 'ExploreSeminar',
                    props: {type: 'webinar'},
                  })
                }>
                <Text style={{color: 'white'}}>Explore Seminar</Text>
              </TouchableOpacity>
            </View>
          )}
        </>
      )}
    </View>
  );
}
