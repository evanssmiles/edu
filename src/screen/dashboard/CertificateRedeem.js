import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  ActivityIndicator,
  Image,
  Alert,
  PermissionsAndroid,
  BackHandler,
  TouchableWithoutFeedback,
} from 'react-native';
import {Rating, AirbnbRating} from 'react-native-ratings';
import {colors} from '../../styling';
import {Button} from '../../component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {TouchableOpacity} from 'react-native';
import Clipboard from '@react-native-clipboard/clipboard';
import sertifikat from '../../assets/img/image_sertifikat.png';
import ClosePage from '../../assets/icon/closeIcon.svg';
import ImgDummyCertificate from '../../assets/img/image_sertifikat-dummy.png';

import RNFetchBlob from 'rn-fetch-blob';
import {baseUrl} from './SeminarList';

export function CertificateRedeem({
  navigation,
  id,
  published,
  certificateData,
}) {
  useEffect(() => {
    const backAction = () => {
      navigation.navigate('SeminarStack', {
        screen: 'Eksplor Seminar',
        id: id,
        justPublished: true,
      });
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const backToSeminar = () => {
    navigation.navigate('SeminarStack', {
      screen: 'Eksplor Seminar',
      id: id,
      justPublished: true,
    });
  };

  const [loading, setLoading] = useState(false);
  const [step, setStep] = useState(published);
  const [successMsg, setSuccessMsg] = useState(
    'Penerbitan Sertifikat Anda Berhasil!',
  );
  const [error, setError] = useState({
    errorName: '',
  });

  const [data, setData] = useState({
    certificateUserName: '',
    comment: '',
    eventRate: '',
  });
  const [certificate, setCertificate] = useState(certificateData); //set after publish certificate
  const [certificateUrl, setCertificateUrl] = useState('');
  // copy to clipboard
  const [copiedText, setCopiedText] = useState('');

  function nextStep({passValue}) {
    setData({
      ...data,
      comment: passValue.myComment,
      eventRate: passValue.myRating,
    });
    setStep(2);
  }

  const publishCertificate = async ({myName}) => {
    setLoading(true);
    axios({
      url: baseUrl + '/v1/1/event/sertifikat/' + id + '/create',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        token: await AsyncStorage.getItem('userToken'),
      },
      data: JSON.stringify({
        certificateUserName: myName,
        comment: data.comment,
        eventRate: data.eventRate,
      }),
    })
      .then(function (response) {
        console.log(response);
        setCertificate(response.data.result);
        setLoading(false);
        setStep(3);
      })
      .catch(function (error) {
        // let msg = error.response.data.message.join(', ')
        // setError({
        //     ...error,
        //     errorName: msg,
        // })
        console.log(error, '<<<121');
        setLoading(false);
      });
  };

  const SeminarReviewForm = () => {
    const [rating, setRating] = useState(0);
    const [comment, setComment] = useState('');

    let formReview = {
      myComment: comment,
      myRating: rating,
    };

    const [error, setError] = useState({
      errorRating: '',
      errorComment: '',
    });

    const validatingInput = () => {
      let error = {
        rating: '',
        comment: '',
      };
      if (parseInt(rating) < 1) {
        error.rating = 'Mohon mengisi rating acara ini';
      } else {
        error.rating = '';
      }
      if (comment.length < 30) {
        error.comment = 'Minimal komentar adalah 30 karakter';
      } else {
        error.comment = '';
      }
      if (error.rating != '' || error.comment != '') {
        setError({
          ...error,
          errorRating: error.rating,
          errorComment: error.comment,
        });
        return;
      } else {
        nextStep({passValue: formReview});
      }
    };

    return (
      <View style={[styles.container]}>
        <View style={[styles.titleContainer]}>
          <Text style={[styles.title]}>Pembelajaran Daring yang Efektif</Text>
        </View>
        <View style={[styles.bodyContainer]}>
          <Text style={[styles.title]}>
            Berikan Rating dan Ulasan Anda untuk Online Course kami
          </Text>
          <AirbnbRating
            count={5}
            reviews={[]}
            defaultRating={rating}
            size={30}
            onFinishRating={rating => {
              setRating(rating);
            }}
          />
          <Text
            style={{
              textAlign: 'center',
              //   marginVertical: 5,
              color: colors.color.red,
              fontSize: 12,
            }}>
            {error.errorRating}
          </Text>
          <TextInput
            style={styles.textArea}
            multiline={true}
            numberOfLines={10}
            placeholder="Tulis tanggapan Anda (Min. 30 Karakter)"
            placeholderTextColor={colors.color.grey}
            onChangeText={inp => {
              setComment(inp);
            }}
            value={comment}
          />
          <Text
            style={{
              textAlign: 'center',
              //   margin: 5,
              color: colors.color.red,
              fontSize: 12,
            }}>
            {error.errorComment}
          </Text>
        </View>
        <View style={[styles.titleContainer, styles.fixBottom]}>
          <Button.filledButton
            title="Selanjutnya"
            onPress={() => {
              validatingInput();
            }}
          />
        </View>
      </View>
    );
  };

  const CertificateNameInput = () => {
    const [name, setName] = useState('');

    return (
      <View style={[styles.container]}>
        <View style={[styles.titleContainer]}>
          <Text style={[styles.title]}>Pembelajaran Daring yang Efektif</Text>
        </View>
        <View style={[styles.bodyContainer]}>
          <Text style={[styles.title]}>
            Tulis nama Anda yang akan digunakan pada E-sertifikat (pastikan
            penulisan benar)
          </Text>
          <TextInput
            style={[styles.textInput]}
            onChangeText={name => {
              setName(name);
            }}
            value={name}
          />
          <Text style={{margin: 5, color: colors.color.red, fontSize: 12}}>
            {error.errorName}
          </Text>
        </View>
        <View style={[styles.titleContainer, styles.fixBottom]}>
          <Button.filledButton
            title="Selanjutnya"
            onPress={() => publishCertificate({myName: name})}
          />
        </View>
      </View>
    );
  };

  const GetCertificate = async () => {
    axios({
      url: baseUrl + '/v1/1/event/sertifikat/' + id + '/pdf',
      method: 'GET',
      headers: {
        token: await AsyncStorage.getItem('userToken'),
      },
    })
      .then(function (response) {
        console.log(response);
        setCertificateUrl(response.data.result.Image);
      })
      .catch(function (error) {
        console.log(error.response);
      });
  };

  const CertificatePreview = () => {
    GetCertificate();
    const [getUrl, setGetUrl] = useState('');

    const copyToClipboard = () => {
      Clipboard.setString(
        certificate.sertificateSeriCode == undefined
          ? certificate.sertificateCode
          : certificate.sertificateSeriCode,
      );
    };
    return (
      <View style={styles.container}>
        <View
          style={{
            height: 60,
            width: '100%',
            flexDirection: 'row',
            position: 'relative',
            elevation: 1,
            borderBottomWidth: 1,
            borderBottomColor: colors.color.grey2,
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
          }}>
          <TouchableOpacity
            onPress={() => backToSeminar()}
            style={{position: 'absolute', left: 30}}>
            <ClosePage width={15} height={15} />
          </TouchableOpacity>
          <Text style={{color: 'black', fontSize: 18}}>Sertifikat</Text>
        </View>
        <View style={{flex: 1, padding: 30}}>
          <Text style={styles.nomorId}>No ID Sertifikat</Text>
          <View
            style={{
              flexDirection: 'row',
              marginBottom: 70,
              justifyContent: 'space-between',
            }}>
            <Text style={styles.nomorText} numberOfLines={1}>
              {certificate.sertificateSeriCode == undefined
                ? certificate.sertificateCode
                : certificate.sertificateSeriCode}
            </Text>
            <TouchableOpacity onPress={copyToClipboard}>
              <Text style={styles.salin}>SALIN</Text>
            </TouchableOpacity>
          </View>
          <Image
            source={ImgDummyCertificate}
            style={{
              width: '100%',
              resizeMode: 'contain',
              height: 220,
              alignSelf: 'center',
            }}
          />
        </View>

        <View style={styles.cetakContainer}>
          <Button.filledButton
            onPress={() => historyDownload()}
            title="Simpan sebagai PDF"
            disabled={certificateUrl == '' ? true : false}
          />
        </View>
      </View>
    );
  };

  const LoadingCertificate = () => {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <ActivityIndicator size="large" color={colors.color.greenPrimary} />
      </View>
    );
  };

  if (loading) {
    return <LoadingCertificate></LoadingCertificate>;
  }

  // save file pdf

  function historyDownload() {
    //Function to check the platform
    //If iOS the start downloading
    //If Android then ask for runtime permission
    if (Platform.OS === 'ios') {
      this.downloadHistory();
    } else {
      try {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'storage title',
            message: 'storage_permission',
          },
        ).then(granted => {
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //Once user grant the permission start downloading
            console.log('Storage Permission Granted.');
            downloadHistory();
          } else {
            Alert.alert('storage_permission');
          }
        });
      } catch (err) {
        //To handle permission related issue
        console.log('error', err);
      }
    }
  }

  async function downloadHistory() {
    const {config, fs} = RNFetchBlob;
    let seminarName =
      certificate.sertificateData == undefined
        ? certificate.title
        : certificate.sertificateData.title;
    let PictureDir = fs.dirs.PictureDir;
    let date = new Date();
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        //Related to the Android only
        useDownloadManager: true,
        notification: true,
        mediaScannable: true,
        path: PictureDir + '/Sertifikat ' + seminarName,
        description: seminarName + ' Proedu Sertifikat',
      },
    };
    config(options)
      .fetch('GET', certificateUrl)
      .then(res => {
        //Showing alert after successful downloading
        console.log('res -> ', JSON.stringify(res));
        Alert.alert('', 'Sertifikat berhasil disimpan.');
      });
  }

  return (
    <>
      {(() => {
        switch (step) {
          case 1:
            return <SeminarReviewForm></SeminarReviewForm>;
          case 2:
            return <CertificateNameInput></CertificateNameInput>;
          case 3:
            return <CertificatePreview />;
          default:
            return null;
        }
      })()}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  fixBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
  },
  titleContainer: {
    padding: 20,
    backgroundColor: 'white',
  },
  bodyContainer: {
    padding: 20,
  },
  title: {
    fontSize: 16,
  },
  textArea: {
    // height: 150,
    // marginTop: 35,
    margin: 12,
    borderWidth: 1,
    borderColor: colors.color.grey2,
    borderRadius: 15,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    color: colors.color.black,
    padding: 10,
    textAlignVertical: 'top',
  },
  textInput: {
    marginTop: 35,
    borderWidth: 1,
    borderColor: colors.color.grey2,
    borderRadius: 15,
    backgroundColor: 'white',
    color: colors.color.black,
    padding: 10,
  },
  successContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 4,
  },
  successText: {
    fontSize: 24,
    color: colors.color.black,
    fontWeight: 'bold',
  },
  cetakContainer: {
    justifyContent: 'center',
    backgroundColor: 'white',
    width: '100%',
    padding: 10,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    elevation: 10,
  },
  nomorId: {
    color: colors.color.grey,
    fontSize: 16,
    marginBottom: 10,
  },
  nomorText: {
    fontSize: 20,
    color: colors.color.blueDark,
    fontWeight: 'bold',
    width: '70%',
  },
  salin: {
    color: colors.color.greenPrimary,
    marginLeft: 5,
    fontSize: 20,
  },
});
