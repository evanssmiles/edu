import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import {colors} from '../../styling';
import {baseUrl, OnlineCourseExplore} from './SeminarList';
import moment from 'moment';
import {exploreStyles} from './SeminarExplore';
const {width} = Dimensions.get('window');
const height = width * 0.6;
import dateIcon from '../../assets/icon/dateIcon.png';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DateRangePicker from 'rnv-date-range-picker';
// import SeminarByDate from '../../component/SeminarByDate';

const imagez = [];

const general = StyleSheet.create({
  inline: {
    flexDirection: 'row',
  },
});

export default function OnlineExplore({navigation, route}) {
  const [active, setActive] = useState(0);
  const [selectedRange, setRange] = useState({});
  const [showCalendar, setShowCalendar] = useState(false);
  const [banner, setBanner] = useState([]);
  const [seminarsByDate, setSeminarsByDate] = useState([]);
  const [msg, setMsg] = useState('');

  const changePage = ({nativeEvent}) => {
    const slide = Math.ceil(
      nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width,
    );

    if (slide != active) {
      setActive(slide);
    }
  };

  const url = `${baseUrl}/v2/event/list?type=all&typeevent=onlinecourse&subtype=none&sort=desc&status=all&datestart=${selectedRange.firstDate}&dateend=${selectedRange.secondDate}&page=1&limite=100`;

  const bannerUrl = `${baseUrl}/v2/event/list/onlinecourse/carusel`;

  useEffect(async () => {
    let token = await AsyncStorage.getItem('userToken');
    axios({
      url: bannerUrl,
      method: 'GET',
      headers: {
        token: token,
      },
    })
      .then(function (res) {
        setBanner(res.data.result);
      })
      .catch(function (err) {
        console.log(err.response);
      });
  }, []);

  useEffect(async () => {
    let token = await AsyncStorage.getItem('userToken');
    if (selectedRange.firstDate && selectedRange.secondDate) {
      axios({
        url: url,
        method: 'GET',
        headers: {
          token: token,
        },
      })
        .then(function (res) {
          if (res.data.result.length == 0) {
            setMsg('Event tidak ditemukan');
            setSeminarsByDate([]);
            setShowCalendar(false);
            setTimeout(() => {
              setMsg('');
            }, 1500);
          } else {
            setSeminarsByDate(res.data.result);
            setShowCalendar(false);
          }
        })
        .catch(function (err) {
          console.log(err.response);
        });
    }
  }, [selectedRange]);

  const handlePress = () => {
    setShowCalendar(!showCalendar);
    setSeminarsByDate([]);
  };
  return (
    <View style={{flex: 1, width: '100%'}}>
      <View style={{width, height, marginBottom: 50}}>
        <ScrollView
          onScroll={changePage}
          pagingEnabled
          horizontal={true}
          showsHorizontalScrollIndicator={false}>
          {banner.map((e, i) => (
            <Image
              style={{width, height, resizeMode: 'cover'}}
              source={{
                uri: `${e.thumbnail}`,
              }}
              key={i}
            />
          ))}
        </ScrollView>
        <View style={exploreStyles.dotContainer}>
          {banner.map((e, i) => (
            <Text
              key={i}
              style={i == active ? exploreStyles.dotActive : exploreStyles.dot}>
              ⬤
            </Text>
          ))}
        </View>
      </View>

      {/* <View style={exploreStyles.dateContainer}>
        <Text style={exploreStyles.date}>{moment().format('LL')}</Text>
        <TouchableOpacity>
          <View style={exploreStyles.calendar}>
            <Image source={dateIcon} />
            <Text style={exploreStyles.dateInput}>MM/DD/YY - MM/DD/YY</Text>
          </View>
        </TouchableOpacity>
      </View> */}

      {msg != '' && (
        <Text style={{alignSelf: 'center', color: '#6C757C'}}>{msg}</Text>
      )}

      <ScrollView>
        {showCalendar && (
          <View style={{paddingHorizontal: 20}}>
            <DateRangePicker
              onSelectDateRange={range => {
                setRange(range);
              }}
              responseFormat="YYYY-MM-DD"
              // maxDate={moment()}
              minDate={moment().subtract(30, 'days')}
            />
          </View>
        )}

        {seminarsByDate.length == 0 && (
          <OnlineCourseExplore navigation={navigation} />
        )}

        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            alignItems: 'center',
            paddingLeft: 23,
          }}>
          {/* {seminarsByDate.map(e => (
            <SeminarByDate
              navigation={navigation}
              item={e}
              online={true}
              key={e.id}
            />
          ))} */}
        </View>
      </ScrollView>
    </View>
  );
}
