import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import {colors} from '../../styling';
import {Input, Button} from '../../component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Clipboard from '@react-native-clipboard/clipboard';
import IconClock from '../../assets/icon/iconClockGrey.svg';
import chevDown from '../../assets/icon/chevron-down.png';
import {CartLoading} from './CartDetail';
import {baseUrl} from './SeminarList';

const style = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  content: {
    padding: 20,
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    marginRight: 10,
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
  heading: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  referral: {
    textAlign: 'center',
    fontSize: 32,
    fontWeight: 'bold',
    marginVertical: 25,
  },
  warningTxt: {
    color: colors.color.red,
    lineHeight: 16,
  },
  spaceY: {
    marginVertical: 10,
  },
  absoluteBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    padding: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'white',
  },
});

const header = StyleSheet.create({
  container: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    backgroundColor: 'white',
  },
  btn: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: colors.color.grey2,
    marginRight: 10,
  },
  txt: {
    color: colors.color.grey,
  },
  txtActive: {
    color: colors.color.greenPrimary,
  },
  btnActive: {
    borderColor: colors.color.greenPrimary,
  },
});

const body = StyleSheet.create({
  content: {
    padding: 20,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: colors.color.grey3,
  },
  contentNew: {
    backgroundColor: colors.color.greenComponent,
  },
  preview: {
    height: 80,
    width: 80,
    marginRight: 15,
    backgroundColor: colors.color.grey2,
    borderRadius: 15,
  },
  previewDesc: {
    flex: 1,
  },
  head: {
    marginBottom: 15,
  },
  main: {},
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  desc: {
    fontSize: 14,
    // fontWeight: 'bold',
  },
  spaceY: {
    marginVertical: 5,
  },
});

export default function TransactionDetail({navigation, data}) {
  const [detail, setDetail] = useState(null);
  const [item, setItem] = useState(['banana', 'apple', 'helo']);
  const [loading, setLoading] = useState(true);
  const [mBanking, setMBanking] = useState(false);
  useEffect(async () => {
    let getToken = await AsyncStorage.getItem('userToken');
    axios({
      url: baseUrl + '/v2/transaction/' + data.id + '/detail',
      method: 'GET',
      headers: {
        token: getToken,
      },
    })
      .then(response => {
        console.log(response.data.result, '<<<<<<139');
        setDetail(response.data.result);
        setLoading(false);
      })
      .catch(error => {
        console.log(error.response);
      });
  }, []);

  if (loading) {
    return <CartLoading />;
  }

  const copyToClipboard = n => {
    Clipboard.setString(n);
    ToastAndroid.show('Teks berhasil disalin', ToastAndroid.SHORT);
  };

  console.log(detail, '<<<<<176');

  return (
    <>
      <View>
        <View style={[header.container, {elevation: 3}]}>
          <Text>Detail Pembelian</Text>
        </View>
        <ScrollView contentContainerStyle={[{paddingBottom: 50}]}>
          <Text
            style={{
              paddingHorizontal: 20,
              marginVertical: 10,
              fontWeight: 'bold',
            }}>
            Info Transaksi
          </Text>
          <View
            style={{
              paddingHorizontal: 20,
              paddingVertical: 15,
              backgroundColor: 'white',
              borderBottomWidth: 1,
              borderBottomColor: colors.color.grey2,
            }}>
            <Text style={{color: colors.color.grey}}>No. Invoice</Text>
            <Text style={[style.txtGreen]}>{detail.id}</Text>
          </View>
          <View
            style={{
              paddingHorizontal: 20,
              paddingVertical: 15,
              backgroundColor: 'white',
            }}>
            <Text style={{color: colors.color.grey}}>Tanggal Pembelian</Text>
            <Text style={[]}>{detail.date}</Text>
          </View>
          <Text
            style={{
              paddingHorizontal: 20,
              marginVertical: 10,
              fontWeight: 'bold',
            }}>
            Status Pembelian
          </Text>
          <View
            style={{
              paddingHorizontal: 20,
              paddingVertical: 15,
              backgroundColor: 'white',
              borderBottomWidth: 1,
              borderBottomColor: colors.color.grey2,
            }}>
            <View style={{alignSelf: 'flex-start'}}>
              <Text
                style={{
                  color: colors.color.grey,
                  padding: 7,
                  borderWidth: 1,
                  borderColor: colors.color.grey,
                  borderRadius: 10,
                }}>
                {detail.paidStatusMessage}
              </Text>
            </View>
            <View
              style={{
                alignSelf: 'flex-start',
                backgroundColor: colors.color.yellowThin,
                paddingVertical: 7,
                paddingHorizontal: 15,
                borderRadius: 5,
                marginTop: 10,
              }}>
              <Text style={[]}>Bayar sebelum {detail.expDate}</Text>
            </View>
          </View>
          <>
            <Text
              style={{
                paddingHorizontal: 20,
                marginVertical: 10,
                fontWeight: 'bold',
              }}>
              Metode Pembayaran
            </Text>
            <View
              style={{
                paddingHorizontal: 20,
                paddingVertical: 15,
                backgroundColor: 'white',
                borderBottomWidth: 1,
                borderBottomColor: colors.color.grey2,
              }}>
              <View
                style={[
                  style.inline,
                  {justifyContent: 'space-between', marginBottom: 10},
                ]}>
                <View>
                  <Text style={[{color: colors.color.grey, marginBottom: 5}]}>
                    Virtual Account
                  </Text>
                  <Text>{detail.paidMethode.name}</Text>
                </View>
                <Image
                  style={{width: 45, height: 15, resizeMode: 'contain'}}
                  source={{uri: detail.paidMethode.logo}}
                />
              </View>
            </View>
            {detail.paidMethode.name == 'Mandiri Virtual Account' && (
              <View
                style={{
                  paddingHorizontal: 20,
                  paddingVertical: 15,
                  backgroundColor: 'white',
                  borderBottomWidth: 1,
                  borderBottomColor: colors.color.grey2,
                }}>
                <View>
                  <Text style={[{color: colors.color.grey, marginBottom: 5}]}>
                    Biller Code
                  </Text>
                  <View
                    style={[
                      style.inline,
                      {justifyContent: 'space-between', marginBottom: 10},
                    ]}>
                    <Text>{detail.paidMethode.billerCode}</Text>
                    <TouchableOpacity
                      onPress={() =>
                        copyToClipboard(detail.paidMethode.billerCode)
                      }>
                      <Text
                        style={[
                          style.txtGreen,
                          {fontWeight: 'bold', letterSpacing: 3},
                        ]}>
                        SALIN
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            )}
            <View
              style={{
                paddingHorizontal: 20,
                paddingVertical: 15,
                backgroundColor: 'white',
                borderBottomWidth: 1,
                borderBottomColor: colors.color.grey2,
              }}>
              <View>
                <Text style={[{color: colors.color.grey, marginBottom: 5}]}>
                  Nomor Virtual Account
                </Text>
                <View
                  style={[
                    style.inline,
                    {justifyContent: 'space-between', marginBottom: 10},
                  ]}>
                  <Text style={{color: 'black'}}>
                    {detail.paidMethode.name == 'Mandiri Virtual Account'
                      ? detail.paidMethode.billKey
                      : detail.paidMethode.vaNumerb}
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      copyToClipboard(
                        detail.paidMethode.billKey ||
                          detail.paidMethode.vaNumerb,
                      )
                    }>
                    <Text
                      style={[
                        style.txtGreen,
                        {fontWeight: 'bold', letterSpacing: 3},
                      ]}>
                      SALIN
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View
              style={{
                paddingHorizontal: 20,
                paddingVertical: 15,
                backgroundColor: 'white',
                borderBottomWidth: 1,
                borderBottomColor: colors.color.grey2,
              }}>
              <View>
                <Text style={[{color: colors.color.grey, marginBottom: 5}]}>
                  Total Pembayaran
                </Text>
                <View
                  style={[
                    style.inline,
                    {justifyContent: 'space-between', marginBottom: 10},
                  ]}>
                  <Text style={{color: colors.color.red}}>
                    {detail.paidMethode.paid}
                  </Text>
                  {/* <TouchableOpacity>
                    <Text style={[style.txtGreen]}>Lihat Detail</Text>
                  </TouchableOpacity> */}
                </View>
              </View>
            </View>
          </>
          <>
            <Text
              style={{
                paddingHorizontal: 20,
                marginVertical: 10,
                fontWeight: 'bold',
              }}>
              Cara Pembayaran
            </Text>

            {detail.paidMethodeStep.map((val, key) => {
              return (
                <View
                  key={val.title}
                  style={{
                    paddingHorizontal: 20,
                    paddingVertical: 15,
                    backgroundColor: 'white',
                    borderBottomWidth: 1,
                    borderBottomColor: colors.color.grey2,
                  }}>
                  <View
                    style={[
                      !mBanking && style.inline,
                      {justifyContent: 'space-between'},
                    ]}>
                    <Text style={{marginBottom: 5}}>{val.title}</Text>
                    {mBanking && <Text>{val.description}</Text>}
                    {!mBanking && (
                      <TouchableOpacity onPress={() => setMBanking(!mBanking)}>
                        <View
                          style={{
                            width: 30,
                            height: 30,
                            alignItems: 'flex-end',
                            justifyContent: 'center',
                          }}>
                          <Image
                            style={{
                              width: 13,
                              height: 13,
                              resizeMode: 'contain',
                            }}
                            source={chevDown}
                          />
                        </View>
                      </TouchableOpacity>
                    )}
                    {mBanking && (
                      <TouchableOpacity onPress={() => setMBanking(!mBanking)}>
                        <Text
                          style={{
                            marginTop: 3,
                            alignSelf: 'flex-end',
                            letterSpacing: 1.5,
                            fontWeight: 'bold',
                            color: colors.color.black,
                          }}>
                          TUTUP
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
              );
            })}
          </>
          <>
            <Text
              style={{
                paddingHorizontal: 20,
                marginVertical: 10,
                fontWeight: 'bold',
              }}>
              Daftar Pembelian
            </Text>
            {detail.detailTransaction.map((val, key) => {
              // {"PaticipantSum": "187", "eventType": "onlinecourse", "final_price": "Rp 99.000", "oriprice": "Rp 149.000", "rating": 4.7, "seminarDate": "04 Mei 2021", "seminarDateWithDay": "Selasa, 04 Mei 2021", "seminarTime": "13:00", "thumbnail": "https://s3.ap-southeast-1.amazonaws.com/assets.proedu.id/event/20210503071917_thumbnail.png", "title": "Tanah Datar: Pelatihan KepSek & Sosialisasi ProEdu"}
              return (
                <View
                  style={[body.content, key == 0 ? body.contentNew : null]}
                  key={key}>
                  <View style={[style.inline, body.main]}>
                    <Image
                      style={[body.preview]}
                      source={{uri: val.thumbnail}}
                    />
                    <View style={[body.previewDesc]}>
                      <Text style={[body.desc, body.spaceY]} numberOfLines={1}>
                        {val.title}
                      </Text>
                      <View style={[style.inline, body.spaceY]}>
                        <IconClock
                          width={12}
                          height={12}
                          style={{marginRight: 10}}
                        />
                        <Text style={{fontSize: 12}}>{val.seminarDate}</Text>
                      </View>
                      <View
                        style={[
                          style.inline,
                          {alignItems: 'center', marginTop: 10},
                        ]}>
                        <Text
                          style={{
                            flex: 1,
                            textAlign: 'right',
                            marginRight: 10,
                            fontSize: 12,
                            textDecorationLine: 'line-through',
                          }}>
                          {val.oriprice}
                        </Text>
                        <Text
                          style={{
                            textAlign: 'right',
                            fontSize: 16,
                            fontWeight: 'bold',
                          }}>
                          {val.price}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              );
            })}
          </>
          <>
            <Text
              style={{
                paddingHorizontal: 20,
                marginVertical: 10,
                fontWeight: 'bold',
              }}>
              Rincian Pembayaran
            </Text>
            <View
              style={{
                paddingHorizontal: 20,
                paddingVertical: 15,
                backgroundColor: 'white',
              }}>
              <View
                style={[
                  style.inline,
                  {justifyContent: 'space-between', marginBottom: 10},
                ]}>
                <Text>Total Harga ({detail.countSeminar} Item)</Text>
                <Text>{detail.SumPrice}</Text>
              </View>
              <TouchableOpacity
                style={[
                  style.inline,
                  {justifyContent: 'space-between', marginBottom: 10},
                ]}>
                <Text>Biaya Layanan</Text>
                <Text style={{color: colors.color.grey2}}>
                  {detail.serviceFee}
                </Text>
              </TouchableOpacity>
              <View
                style={[
                  style.inline,
                  {justifyContent: 'space-between', marginBottom: 10},
                ]}>
                <Text>Total Pembayaran</Text>
                <Text>{detail.paidMethode.paid}</Text>
              </View>
            </View>
          </>
        </ScrollView>
      </View>
    </>
  );
}
