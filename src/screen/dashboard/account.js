import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {colors, font, input, btn} from '../../styling';
import {
  Header,
  Button,
  Input,
  iconButton,
  Context,
  TopBar,
  BottomNavigation,
} from '../../component';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

import iconProfile from '../../assets/icon/dummyProfile.png';
import iconLogout from '../../assets/icon/logout.png';
import iconNext from '../../assets/icon/next.png';

import {AuthContext} from '../../component/Context';
import {baseUrl} from './SeminarList';

export default function Account({navigation}) {
  const [data, setData] = useState({
    name: 'userName',
    email: 'userEmail@domain.tld',
  });

  const {signOut} = React.useContext(AuthContext);
  const [modalVisibility, setModalVisibility] = useState(false);

  const [tabActive, setTabActive] = useState('teacher');
  const tabStyle = StyleSheet.create({
    parent: {
      flexDirection: 'row',
      width: '100%',
      flex: 1,
      backgroundColor: colors.color.grey3,
    },
    tab: {
      width: 150,
      justifyContent: 'center',
      alignItems: 'center',
      borderBottomWidth: 2,
      borderColor: colors.color.grey,
    },
    tabActive: {
      borderBottomWidth: 2,
      borderColor: colors.color.yellowHover,
    },
    menu: {
      padding: 15,
    },
  });

  useEffect(async () => {
    let getToken = await AsyncStorage.getItem('userToken');
    axios({
      url: baseUrl + '/v2/account/token/verified',
      method: 'GET',
      headers: {
        token: getToken,
      },
    })
      .then(function (res) {
        res.data.result;
        setData({
          name: res.data.result.name,
          email: res.data.result.email,
        });
      })
      .catch(function (err) {
        console.log(err);
      });
  }, []);

  const handleSignOut = () => {
    setModalVisibility(true);
  };

  return (
    <View>
      <Modal
        isVisible={modalVisibility}
        onBackdropPress={() => setModalVisibility(false)}
        animationInTiming={500}
        animationOutTiming={500}>
        <View
          style={{
            backgroundColor: 'white',
            paddingVertical: 30,
            paddingHorizontal: 20,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 20,
          }}>
          <Text style={{fontSize: 16}}>Anda yakin ingin Log out ? </Text>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 50,
              justifyContent: 'space-between',
              width: '100%',
            }}>
            <TouchableOpacity
              style={{
                flex: 1,
                borderWidth: 1,
                borderColor: colors.color.greenPrimary,
                borderRadius: 15,
                marginHorizontal: 5,
                paddingVertical: 10,
              }}
              onPress={() => signOut()}>
              <Text
                style={{textAlign: 'center', color: colors.color.greenPrimary}}>
                Ya
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flex: 1,
                borderWidth: 1,
                backgroundColor: colors.color.greenPrimary,
                borderRadius: 15,
                marginHorizontal: 5,
                paddingVertical: 10,
              }}
              onPress={() => setModalVisibility(false)}>
              <Text style={{textAlign: 'center', color: 'white'}}>Kembali</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <ScrollView
        style={[tabStyle.parent]}
        horizontal={true}
        showsHorizontalScrollIndicator={false}>
        <View
          style={[
            tabStyle.tab,
            tabActive == 'teacher' ? tabStyle.tabActive : null,
          ]}>
          <TouchableOpacity
            style={[tabStyle.menu]}
            onPress={() => {
              setTabActive('teacher');
            }}>
            <Text>Akun Guru</Text>
          </TouchableOpacity>
        </View>
        <View
          style={[
            tabStyle.tab,
            tabActive == 'instructor' ? tabStyle.tabActive : null,
          ]}>
          <TouchableOpacity
            style={[tabStyle.menu]}
            onPress={() => {
              setTabActive('instructor');
            }}>
            <Text>Akun Instruktur</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <View style={{width: '100%', backgroundColor: colors.color.grey3}}>
        {tabActive == 'teacher' && (
          <View style={{alignItems: 'center', height: '100%'}}>
            <View
              style={{
                marginVertical: 15,
                padding: 10,
                height: 120,
                width: 120,
                borderRadius: 60,
                backgroundColor: 'transparent',
                borderColor: colors.color.grey,
                borderStyle: 'dashed',
                borderWidth: 1,
              }}>
              <View
                style={{
                  backgroundColor: 'red',
                  width: 100,
                  height: 100,
                  borderRadius: 50,
                  backgroundColor: colors.color.blueShape,
                  alignItems: 'center',
                  justifyContent: 'center',
                  position: 'relative',
                  overflow: 'hidden',
                }}>
                <View
                  style={{
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    backgroundColor: colors.color.grey,
                    width: '100%',
                    height: 25,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{color: 'white'}}>Edit</Text>
                </View>
                <Image
                  source={iconProfile}
                  style={{width: 50, height: 50, resizeMode: 'contain'}}
                />
              </View>
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontWeight: 'bold', fontSize: 18}}>
                {data.name}
              </Text>
              <Text>{data.email}</Text>
            </View>
            <View style={{marginVertical: 15}}>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  width: '80%',
                  padding: 10,
                  backgroundColor: 'white',
                }}>
                <Text style={{flex: 1}}>Data Diri</Text>
                <Image source={iconNext}></Image>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  width: '80%',
                  padding: 10,
                  backgroundColor: 'white',
                }}
                onPress={() => navigation.navigate('HonorerStack')}
                disabled={true}>
                <Text style={{flex: 1}}>Dokumen Guru Honorer</Text>
                <Image source={iconNext}></Image>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  width: '80%',
                  padding: 10,
                  backgroundColor: 'white',
                }}>
                <Text style={{flex: 1}}>Keamanan Akun</Text>
                <Image source={iconNext}></Image>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                position: 'absolute',
                bottom: 100,
                left: 25,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => {
                handleSignOut();
              }}>
              <Image source={iconLogout} style={{width: 20, height: 20}} />
              <Text>Log Out</Text>
            </TouchableOpacity>
          </View>
        )}
        {tabActive == 'instructor' && <View></View>}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  inline: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dashboardContainer: {
    height: '100%',
    position: 'relative',
  },
  mainContent: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  iconTxt: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    marginHorizontal: 7,
  },
  txtWarning: {
    color: colors.color.red,
    fontSize: 10,
  },
  card: {
    height: 190,
    width: 155,
    marginHorizontal: 10,
    alignItems: 'center',
    elevation: 35,
  },
  cardHead: {
    width: '100%',
    height: 80,
  },
  cardHeadImg: {
    width: '100%',
    height: 80,
    resizeMode: 'cover',
  },
  cardBody: {
    backgroundColor: 'white',
    width: '100%',
    flex: 1,
    padding: 5,
  },
  cardBodyTitle: {
    fontSize: 12,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  cardBodyData: {
    fontSize: 12,
  },
  clockIcon: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
});
