import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Image,
} from 'react-native';
import {colors} from '../../styling';
import {baseUrl, SearchEvent} from './SeminarList';
import {SliderWidth} from '../Welcome';
import {SearchInputEvent} from '../../component/Input';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import clockIcon from '../../assets/icon/clockgreen.png';
import starIcon from '../../assets/icon/star_filled.png';

const general = StyleSheet.create({
  inline: {
    flexDirection: 'row',
  },
});

export default function SeminarSearch({navigation, route}) {
  const [input, setInput] = useState('');
  const [searchResult, setSearchResult] = useState([]);
  const [msg, setMsg] = useState('');

  const [token, setToken] = useState(null);

  useEffect(() => {
    setTimeout(async () => {
      let token = await AsyncStorage.getItem('userToken');
      setToken(token);
    }, 250);
  }, []);

  const url = `${baseUrl}/v2/event/${input}/search`;

  useEffect(() => {
    if (input.length > 2) {
      axios({
        url: url,
        method: 'GET',
        headers: {
          token: token,
        },
      })
        .then(function (res) {
          // console.log(res.data, '<<<<');
          if (res.data.status === 'failed') {
            setMsg(res.data.message);
            setSearchResult([]);
          } else {
            setSearchResult(res.data.result);
            setMsg('');
          }
        })
        .catch(function (err) {
          console.log(err.response);
        });
    } else {
      setSearchResult([]);
      setMsg('');
    }
  }, [input]);

  console.log(msg, '<<result');

  return (
    <View style={styles.container}>
      <View>
        <SearchInputEvent
          style={styles.inputSearch}
          onChange={text => setInput(text)}
          autoFocus={true}
        />
      </View>
      {msg != '' && (
        <Text style={[styles.cardBodyTitle, {alignSelf: 'center'}]}>{msg}</Text>
      )}
      <View style={{paddingLeft: 3, paddingBottom: 65}}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              alignItems: 'center',
            }}>
            {searchResult.map((e, i) => {
              return (
                <View style={styles.item} key={i}>
                  <View style={styles.content}>
                    <Image
                      style={styles.rekomendasiImage}
                      source={{
                        uri: e.thumbnail == '' ? defaultImgEmpty : e.thumbnail,
                      }}
                    />
                    <View style={styles.jpContainer}>
                      <View style={styles.jpDiv}>
                        <Text style={styles.number}>{e.JP}</Text>
                      </View>
                      <Text style={styles.jpText}>JP</Text>
                    </View>
                  </View>
                  <View style={styles.info}>
                    <Text
                      style={[
                        styles.cardBodyTitle,
                        {color: colors.color.black},
                      ]}
                      numberOfLines={1}>
                      {e.title}
                    </Text>
                    {/* <View style={styles.timeContainer}>
                      <Image source={clockIcon} />
                      <Text style={styles.time}>
                        {e.schedules?.datehEventStartShort}{' '}
                        {e.schedules?.mounthEventStartShort} -{' '}
                        {e.schedules?.timeEventStart} WIB
                      </Text>
                    </View> */}

                    {e.originalPrice != 0 ? (
                      <Text style={styles.hargaCoret}>Rp{e.originalPrice}</Text>
                    ) : (
                      <View style={{height: 17}}></View>
                    )}

                    <Text style={styles.harga}>
                      {e.price == 0 ? 'FREE' : `Rp${e.price}`}
                    </Text>

                    {/* <View style={styles.ratingContainer}>
                      <Image
                        style={{width: 15, resizeMode: 'contain'}}
                        source={starIcon}
                      />
                      <Text style={styles.rating}>
                        {e.EventRating?.rateStarFloat}
                      </Text>
                      <Text style={styles.amount}>
                        ({e.EventRating?.users})
                      </Text>
                    </View> */}

                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('SeminarStack', {
                          screen: 'Eksplor Seminar',
                          id: e.EventId == undefined ? e.id : e.EventId,
                        });
                      }}>
                      <View style={styles.beliBtn}>
                        <Text style={styles.beliText}>Beli</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
    paddingHorizontal: 8,
  },
  jpContainer: {
    width: 49,
    height: 24,
    backgroundColor: '#002554',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 14,
    position: 'absolute',
    right: 5,
    top: 5,
  },
  number: {
    color: '#ccc',
    fontSize: 10,
    color: '#002554',
  },
  jpDiv: {
    backgroundColor: '#F9E246',
    borderRadius: 14,
    width: 24,
    height: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  jpText: {
    fontSize: 10,
    color: '#ccc',
    marginRight: 8,
  },
  onlineCard: {
    height: 160,
    width: 155,
    marginHorizontal: 10,
    alignItems: 'center',
    elevation: 35,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ratingText: {
    color: '#6C757C',
    fontSize: 12,
    marginRight: 5,
  },

  star: {
    width: 12,
    height: 12,
    marginRight: 5,
  },

  item: {
    height: 205,
    width: 155,
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 6,
    marginBottom: 15,
    marginLeft: 20,
  },
  content: {
    width: 155,
    height: 80,
    borderColor: '#333',
    borderWidth: 1,
    borderRadius: 8,
    alignItems: 'flex-end',
  },
  info: {
    padding: 8,
  },
  headline: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#232323',
    marginBottom: 10,
  },
  timeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 23,
  },
  time: {
    fontSize: 12,
    color: '#232323',
    marginLeft: 5,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  hargaCoret: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    textDecorationColor: '#6C757C',
    color: '#6C757C',
    fontSize: 12,
  },
  harga: {
    color: '#002F6B',
    fontSize: 16,
    fontWeight: 'bold',
  },
  rating: {
    fontSize: 16,
    marginHorizontal: 5,
    color: '#6C757C',
  },
  amount: {
    fontSize: 16,
    color: '#6C757C',
  },
  beliBtn: {
    width: 139,
    height: 32,
    backgroundColor: '#00AF68',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  beliText: {
    color: '#fff',
    fontSize: 16,
  },

  rekomendasiImage: {
    width: '100%',
    height: 80,
    resizeMode: 'cover',
    borderRadius: 8,
  },

  capacity: {
    fontSize: 12,
    color: 'grey',
  },
  registered: {
    fontSize: 12,
    color: '#fff',
    marginLeft: 5,
  },
  thumbnail: {
    width: 153,
    height: 78,
    borderRadius: 8,
  },
  kuotaText: {
    fontSize: 10,
    color: '#fff',
  },

  kuotaContainer: {
    height: 28,
    backgroundColor: '#9FC359',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 5,
    marginTop: 4,
    marginBottom: 8,
  },

  packageContainer: {
    height: 120,
    width: 273,
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 6,
    marginHorizontal: 8,
    flexDirection: 'row',
  },

  thumbnailPackage: {
    height: 120,
    width: 120,
    resizeMode: 'cover',
    borderRadius: 8,
    marginRight: 12,
  },

  jpContainerPackage: {
    width: 49,
    height: 24,
    backgroundColor: '#002554',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 14,
    position: 'absolute',
    top: 5,
    left: 65,
  },

  cardBodyTitle: {
    fontSize: 12,
    fontWeight: 'bold',
    marginBottom: 10,
    // height: 45,
  },
});
