import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {colors} from '../../styling';
import {Input, Button} from '../../component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

import BouncyCheckbox from 'react-native-bouncy-checkbox';
import IconClock from '../../assets/icon/iconClockGrey.svg';
import {baseUrl} from './SeminarList';

const style = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  content: {
    padding: 20,
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    marginRight: 10,
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
  heading: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  referral: {
    textAlign: 'center',
    fontSize: 32,
    fontWeight: 'bold',
    marginVertical: 25,
  },
  warningTxt: {
    color: colors.color.red,
    lineHeight: 16,
  },
  spaceY: {
    marginVertical: 10,
  },
  absoluteBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    padding: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'white',
  },
});

const floatBtn = StyleSheet.create({
  button: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 20,
    elevation: 7,
    backgroundColor: 'white',
    width: 180,
  },
});

const header = StyleSheet.create({
  container: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    width: '100%',
    backgroundColor: 'white',
  },
  btn: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: colors.color.grey2,
    marginRight: 10,
  },
  txt: {
    color: colors.color.grey,
  },
  txtActive: {
    color: colors.color.greenPrimary,
  },
  btnActive: {
    borderColor: colors.color.greenPrimary,
  },
});

const body = StyleSheet.create({
  content: {
    padding: 20,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: colors.color.grey3,
  },
  contentNew: {
    backgroundColor: colors.color.greenComponent,
  },
  preview: {
    height: 80,
    width: 80,
    marginRight: 15,
    backgroundColor: colors.color.grey2,
    borderRadius: 15,
  },
  previewDesc: {
    flex: 1,
  },
  head: {
    marginBottom: 15,
  },
  main: {},
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  desc: {
    fontSize: 14,
    // fontWeight: 'bold',
  },
  spaceY: {
    marginVertical: 5,
  },
});

const circle = {
  height: 5,
  width: 5,
  borderRadius: 5 / 2,
  backgroundColor: colors.color.grey,
  marginHorizontal: 5,
  marginTop: 3,
};

const MerchantList = ({myToken}) => {
  const [paymentOpt, setPaymentOpt] = useState(null);
  useEffect(() => {
    axios({
      url: baseUrl + '/v1/transaction/pay/methode',
      method: 'GET',
      headers: {
        token: myToken,
      },
    })
      .then(function (res) {
        if (res.status < 300) {
          console.log(res.data);
          let collect = res.data.result;
          collect.map((val, key) => {});
          setPaymentOpt(res.data.result);
        }
      })
      .catch(function (err) {
        console.log('error is : ', err.response);
      });
  }, []);
  return (
    <View style={{flex: 1, marginBottom: 50}}>
      {paymentOpt != null && (
        <View style={{flex: 1}}>
          {paymentOpt.map((val, key) => {
            return (
              <>
                <Text
                  style={{
                    paddingHorizontal: 20,
                    marginVertical: 10,
                    fontWeight: 'bold',
                  }}>
                  {val.Transactiontype}
                </Text>
                {val.mercent.map((v, k) => {
                  return (
                    <View
                      style={{
                        paddingHorizontal: 20,
                        paddingVertical: 15,
                        backgroundColor: 'white',
                        borderBottomWidth: 1,
                        borderBottomColor: colors.color.grey3,
                      }}>
                      <View
                        style={[
                          style.inline,
                          {marginBottom: 10, alignItems: 'center'},
                        ]}>
                        <BouncyCheckbox
                          size={18}
                          fillColor={colors.color.greenPrimary}
                          unfillColor="#FFFFFF"
                          text=""
                          iconStyle={{borderColor: colors.color.grey2}}
                        />
                        <Image
                          source={{uri: v.logo}}
                          style={{
                            marginRight: 5,
                            width: 25,
                            height: 10,
                            resizeMode: 'contain',
                          }}
                        />
                        <Text>{v.name}</Text>
                      </View>
                    </View>
                  );
                })}
              </>
            );
          })}
        </View>
      )}
    </View>
  );
};

export default function PaymentDetail({navigation, data}) {
  const [loading, setLoading] = useState(true);
  const [haveReferral, setHaveReferral] = useState(false);
  const [referral, setReferral] = useState('');
  const [error, setError] = useState('');
  const [token, setToken] = useState('');

  const [item, setItem] = useState(null);

  const checkRadio = key => {
    let collect = item;
    collect[key].status = !collect[key].status;
    setItem(collect);
    console.log(item[key].status);
  };

  useEffect(async () => {
    let getToken = await AsyncStorage.getItem('userToken');
    setToken(getToken);
    axios({
      url: baseUrl + '/v2/transaction/cart/summary',
      method: 'GET',
      headers: {
        token: getToken,
      },
    })
      .then(function (res) {
        console.log(res);
        if (res.status < 300) {
          setItem(res.data.result);
          setLoading(false);
        }
      })
      .catch(function (err) {
        console.log('error is : ', res.response);
      });
  }, []);

  function nextStep() {
    navigation.navigate('PaymentStack', {screen: 'Pembayaran'});
  }

  console.log(item, '<<<<276');

  return (
    <View style={[style.container]}>
      <View style={[header.container]}>
        <Text>Pilih Pembayaran</Text>
      </View>
      {!loading ? (
        <>
          <ScrollView contentContainerStyle={{paddingBottom: 150}}>
            {data.item.map((val, key) => {
              return (
                <View
                  style={[body.content, key == 0 ? body.contentNew : null]}
                  key={key}>
                  <View style={[style.inline, body.main]}>
                    <Image
                      style={[body.preview]}
                      source={{uri: val.thumbnail}}
                    />
                    <View style={[body.previewDesc]}>
                      <Text style={[body.desc, body.spaceY]} numberOfLines={1}>
                        {val.title}
                      </Text>
                      <View style={[style.inline, body.spaceY]}>
                        <IconClock
                          width={12}
                          height={12}
                          style={{marginRight: 10}}
                        />
                        <Text style={{fontSize: 12}}>{val.seminarDate}</Text>
                      </View>
                      <View
                        style={[
                          style.inline,
                          {alignItems: 'center', marginTop: 10},
                        ]}>
                        <Text
                          style={{
                            flex: 1,
                            textAlign: 'right',
                            marginRight: 10,
                            fontSize: 12,
                            textDecorationLine: 'line-through',
                          }}>
                          Rp. {val.oriPrice}
                        </Text>
                        <Text
                          style={{
                            textAlign: 'right',
                            fontSize: 16,
                            fontWeight: 'bold',
                          }}>
                          Rp. {val.payPrice}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              );
            })}
            <View
              style={[
                {
                  backgroundColor: 'white',
                  width: '100%',
                  alignItems: 'center',
                  justifyContent: 'center',
                  flexDirection: 'row',
                  padding: 20,
                  marginTop: 15,
                },
              ]}>
              <TextInput
                editable
                placeholder="Masukkan kode promo"
                style={{
                  borderWidth: 0,
                  borderBottomWidth: 1,
                  borderColor: colors.color.grey2,
                  marginRight: 15,
                  flex: 1,
                  padding: 0,
                }}
              />
              <TouchableOpacity
                style={{
                  borderColor: colors.color.greenPrimary,
                  borderWidth: 1,
                  borderRadius: 15,
                  paddingVertical: 5,
                  paddingHorizontal: 20,
                }}
                disabled={true}>
                <Text style={{color: colors.color.greenPrimary}}>Terapkan</Text>
              </TouchableOpacity>
            </View>
            <Text
              style={{
                paddingHorizontal: 20,
                marginVertical: 10,
                fontWeight: 'bold',
              }}>
              Rincian Pembayaran
            </Text>
            <View
              style={{
                paddingHorizontal: 20,
                paddingVertical: 15,
                backgroundColor: 'white',
              }}>
              <View
                style={[
                  style.inline,
                  {justifyContent: 'space-between', marginBottom: 10},
                ]}>
                <Text>Total Harga ({item.DetailTransaction.length} Item)</Text>
                <Text>{item.TotalSummaryPrice}</Text>
              </View>
              <TouchableOpacity
                style={[style.inline, {justifyContent: 'space-between'}]}>
                <Text>Biaya Layanan</Text>
                <Text style={{color: colors.color.grey2}}>
                  Pilih Pembayaran
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <View style={[style.absoluteBottom]}>
            <View
              style={[
                style.inline,
                {
                  justifyContent: 'space-between',
                  alignItems: 'flex-start',
                  marginBottom: 10,
                },
              ]}>
              <Text>Total Harga</Text>
              <View style={{justifyContent: 'flex-end'}}>
                <Text
                  style={[
                    {fontSize: 24, fontWeight: 'bold', textAlign: 'right'},
                  ]}>
                  {item == null ? '' : item.TotalSummaryPrice}
                </Text>
                <Text style={[{color: colors.color.grey, textAlign: 'right'}]}>
                  Dapatkan {item.TotalPoint} poin
                </Text>
              </View>
            </View>
            <Button.filledButton
              style={[]}
              title="Pilih Pembayaran"
              onPress={() => nextStep()}
            />
          </View>
        </>
      ) : null}
    </View>
  );
}
