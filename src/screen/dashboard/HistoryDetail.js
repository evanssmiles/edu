import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import {colors} from '../../styling';
import IconInfoRed from '../../assets/icon/infoRed.svg';
import IconInfoGrey from '../../assets/icon/infoGrey.svg';
import IconSuccess from '../../assets/icon/checkedCircleGreen.svg';
import IconStar from '../../assets/icon/star.svg';
import IconClock from '../../assets/icon/iconClockGrey.svg';

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {baseUrl} from './SeminarList';

const style = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
  },
  content: {
    padding: 20,
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    marginRight: 10,
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
  heading: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  referral: {
    textAlign: 'center',
    fontSize: 32,
    fontWeight: 'bold',
    marginVertical: 25,
  },
  warningTxt: {
    color: colors.color.red,
    lineHeight: 16,
  },
  spaceY: {
    marginVertical: 10,
  },
  absoluteBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    padding: 25,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
});

const header = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: '100%',
  },
  btn: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: colors.color.grey2,
    marginRight: 10,
  },
  txt: {
    color: colors.color.grey,
  },
  txtActive: {
    color: colors.color.greenPrimary,
  },
  btnActive: {
    borderColor: colors.color.greenPrimary,
  },
});

const body = StyleSheet.create({
  content: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: colors.color.grey3,
  },
  contentNew: {
    backgroundColor: colors.color.greenComponent,
  },
  preview: {
    height: 100,
    width: 100,
    marginRight: 15,
    // backgroundColor: colors.color.grey2,
    borderRadius: 15,
  },
  previewShop: {
    height: 80,
    width: 80,
    marginRight: 15,
    backgroundColor: colors.color.grey2,
    borderRadius: 15,
  },
  previewDesc: {
    flex: 1,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  desc: {
    fontSize: 12,
  },
  spaceY: {
    marginVertical: 5,
  },
});

const tabStyle = StyleSheet.create({
  parent: {
    flexDirection: 'row',
    width: '100%',
    height: 50,
    backgroundColor: colors.color.grey3,
  },
  tab: {
    width: 150,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    borderBottomWidth: 2,
    borderColor: colors.color.grey,
  },
  tabActive: {
    borderBottomWidth: 2,
    borderColor: colors.color.yellowHover,
  },
  menu: {
    padding: 15,
  },
});

export default function HistoryDetail({navigation}) {
  const wait = timeout => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  };
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(500).then(() => setRefreshing(false));
  }, []);

  const [loading, setLoading] = useState(true);
  const [haveReferral, setHaveReferral] = useState(false);
  const [referral, setReferral] = useState('');
  const [error, setError] = useState('');

  const [menu, setMenu] = useState('event');

  const [item, setItem] = useState([1, 2, 3, 4, 5]);
  const [type, setType] = useState(['Info', 'Aktivitas']);

  const getListTransaction = async () => {
    let getToken = await AsyncStorage.getItem('userToken');
    axios({
      url: baseUrl + '/v2/transaction/list',
      method: 'GET',
      headers: {
        token: getToken,
      },
    })
      .then(response => {
        setItem(response.data.result);
        setMenu('transaction');
      })
      .catch(error => {
        console.log(error.response);
      });
  };

  function setTabActive(menu) {
    if (menu == 'transaction') {
      getListTransaction();
    } else {
      setMenu(menu);
    }
  }

  const detailTransaction = ({id}) => {
    navigation.navigate('TransactionStack', {screen: 'Detail', data: {id: id}});
  };

  if (refreshing) {
    getListTransaction();
  }

  return (
    <View style={[style.container]}>
      <View style={[header.container]}>
        <View style={tabStyle.parent}>
          <View
            style={[tabStyle.tab, menu == 'event' ? tabStyle.tabActive : null]}>
            <TouchableOpacity
              style={[tabStyle.menu]}
              onPress={() => {
                setTabActive('event');
              }}>
              <Text>Seminar</Text>
            </TouchableOpacity>
          </View>
          <View
            style={[
              tabStyle.tab,
              menu == 'transaction' ? tabStyle.tabActive : null,
            ]}>
            <TouchableOpacity
              style={[tabStyle.menu]}
              onPress={() => {
                setTabActive('transaction');
              }}>
              <Text>Transaksi</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      {menu == 'event' ? (
        <ScrollView>
          {/* {item.map((val, key) => {
            return (
              <View
                style={[body.content, key == 0 ? body.contentNew : null]}
                key={key}>
                <View style={[style.inline, body.main]}>
                  <Image style={[body.preview]} />
                  <View style={[body.previewDesc]}>
                    <Text style={[body.title, body.spaceY]}>
                      Pembelajaran Daring yang Efektif
                    </Text>
                    <View style={[style.inline, body.spaceY]}>
                      <IconStar
                        width={12}
                        height={12}
                        style={{marginRight: 10}}
                      />
                      <Text style={[body.desc, body.spaceY]}>4.7 (000)</Text>
                    </View>
                    <View style={[style.inline, body.spaceY]}>
                      <IconClock
                        width={12}
                        height={12}
                        style={{marginRight: 10}}
                      />
                      <Text>29 November 2022</Text>
                    </View>
                  </View>
                </View>
              </View>
            );
          })} */}
        </ScrollView>
      ) : (
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          {!refreshing &&
            item.map((val, key) => {
              return (
                <TouchableOpacity
                  style={[
                    body.content,
                    {marginBottom: 20},
                    key == 0 ? body.contentNew : null,
                  ]}
                  key={key}
                  onPress={() => detailTransaction({id: val.idEncrypt})}>
                  <View
                    style={[
                      style.inline,
                      style.spaceY,
                      {justifyContent: 'space-between'},
                    ]}>
                    {/* <Image
                      style={[body.preview]}
                      source={{uri: val.thumbnail}}
                    /> */}
                    <Text numberOfLines={1} style={{maxWidth: '50%'}}>
                      {val.id}
                    </Text>
                    <Text style={{color: colors.color.grey}}>{val.date}</Text>
                  </View>
                  <View
                    style={[
                      style.inline,
                      {paddingHorizontal: 10, paddingVertical: 5},
                      val.paidstatus == 'PENDING'
                        ? {backgroundColor: colors.color.yellowThin}
                        : null,
                      val.paidstatus == 'SETTLEMENT'
                        ? {backgroundColor: colors.color.greenComponent}
                        : null,
                      val.paidstatus == 'CANCELLED'
                        ? {backgroundColor: colors.color.red}
                        : null,
                    ]}>
                    {val.paidstatus == 'PENDING' && <IconInfoGrey />}
                    {val.paidstatus == 'SETTLEMENT' && <IconSuccess />}
                    {val.paidstatus != 'PENDING' &&
                      val.paidstatus != 'SETTLEMENT' && <IconInfoRed />}
                    <Text style={{marginLeft: 5, color: colors.color.black}}>
                      {val.paidstatusMessage}
                    </Text>
                  </View>
                  <View style={[style.inline, style.spaceY, body.main]}>
                    <Image
                      style={[body.preview]}
                      source={{
                        uri: val.detail.summary.thumbnail
                          ? val.detail.summary.thumbnail
                          : 'https://us.123rf.com/450wm/pavelstasevich/pavelstasevich1811/pavelstasevich181101028/112815904-no-image-available-icon-flat-vector-illustration.jpg?ver=6',
                      }}
                    />
                    <View style={[body.previewDesc]}>
                      <Text
                        style={[body.title, body.spaceY, {fontWeight: '400'}]}
                        numberOfLines={1}>
                        {val.detail.summary.title}
                      </Text>
                      <View style={[style.inline, body.spaceY]}>
                        <IconClock
                          width={12}
                          height={12}
                          style={{marginRight: 10}}
                        />
                        <Text style={{fontSize: 12}}>
                          {val.detail.summary.seminarDate}
                        </Text>
                      </View>
                      <View
                        style={[
                          style.inline,
                          {
                            alignItems: 'center',
                            marginTop: 10,
                            justifyContent: 'flex-end',
                          },
                        ]}>
                        {/* <Text
                        style={{
                          flex: 1,
                          textAlign: 'right',
                          marginRight: 10,
                          fontSize: 12,
                          textDecorationLine: 'line-through',
                        }}>
                        Rp. 200.000
                      </Text> */}
                        <Text
                          style={{
                            fontSize: 16,
                            fontWeight: 'bold',
                          }}>
                          {val.TotalPrice}
                        </Text>
                      </View>
                    </View>
                  </View>
                  <Text
                    style={{
                      paddingBottom: 5,
                      borderBottomWidth: 1,
                      borderBottomColor: colors.color.grey2,
                      color: colors.color.grey,
                    }}>
                    {''}
                  </Text>
                  <View
                    style={[
                      style.inline,
                      style.spaceY,
                      {justifyContent: 'space-between'},
                    ]}>
                    <Text>Total Pembelian</Text>
                    <Text style={{color: colors.color.grey}}>
                      Rp{numberWithCommas(val.fullPaid)}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })}
        </ScrollView>
      )}
    </View>
  );
}
