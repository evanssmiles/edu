import React, {useState, useEffect} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useRoute} from '@react-navigation/native';

import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {colors} from '../../styling';
import ContentLoader, {Rect} from 'react-content-loader/native';
import clockIcon from '../../assets/icon/clockgreen.png';
import clockWarning from '../../assets/icon/clockWarning.png';
import exploreSeminar from '../../assets/icon/eksplorSeminar.png';
import exploreECourse from '../../assets/icon/eksplorOnlineCourse.png';
import starIcon from '../../assets/icon/star_filled.png';
//export const baseUrl = 'https://uat.proedu.id/proeduid_api';
export const baseUrl = 'https://api.proedu.id';

export const Loader = () => {
  return (
    <View style={[styling.loaderContainer]}>
      <ContentLoader viewBox="0 0 400 235" backgroundColor={colors.color.grey2}>
        <Rect x="0" y="0" rx="4" ry="4" width="255" height="30" />
        <Rect x="0" y="45" rx="4" ry="4" width="155" height="185" />
        <Rect x="160" y="45" rx="4" ry="4" width="155" height="185" />
        <Rect x="320" y="45" rx="4" ry="4" width="155" height="185" />
      </ContentLoader>
    </View>
  );
};

function ListOfItems({item, title, navigation}) {
  const defaultImgEmpty =
    'https://blog.zumrotutholibin.or.id/wp-content/uploads/2018/10/header-image-1-2.png';
  let items = item;
  let html;
  if (items == undefined || items.length < 1) {
    html = <Text style={styling.emptyText}>Belum Ada {title}</Text>;
  } else {
    html = items.map((item, key) => (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('SeminarStack', {
            screen: 'Eksplor Seminar',
            id: item.EventId == undefined ? item.id : item.EventId,
          });
        }}
        key={key}>
        <View style={styles.card}>
          <View style={styles.cardHead}>
            <Image
              style={styles.cardHeadImg}
              source={{
                uri: item.thumbnail == '' ? defaultImgEmpty : item.thumbnail,
              }}
            />
          </View>
          <View
            style={[
              styles.cardBody,
              {borderBottomEndRadius: 8, borderBottomStartRadius: 8},
            ]}>
            <Text style={styles.cardBodyTitle} numberOfLines={1}>
              {item.title}
            </Text>
            <View style={styles.inline}>
              <Image style={styles.clockIcon} source={clockIcon} />
              <Text style={styles.cardBodyData}>
                {item.dateTimeStart == undefined ? '' : item.dateTimeStart}
              </Text>
            </View>
            <View style={[styles.inline, {marginTop: 15, flex: 1}]}>
              <Image style={styles.clockIcon} source={clockWarning} />
              <Text style={styles.txtWarning}>
                {item.countDate === undefined ? 'LIVE' : item.countDate}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    ));
  }
  return (
    <View
      style={[
        styling.listContainer,
        styling.horizontalHeight,
        items == undefined || items.length < 1 ? styling.emptyList : null,
      ]}>
      <Text
        style={{
          paddingHorizontal: 10,
          fontWeight: 'bold',
          fontSize: 14,
          marginBottom: 15,
        }}>
        {title}
      </Text>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {html}
      </ScrollView>
    </View>
  );
}

function OnlineCourseItems({item, title, navigation}) {
  // {"EventId": "eyJpdiI6Ik9FNGg2MlZiMFhjZ3pMWUVzOVFWTEE9PSIsInZhbHVlIjoib2M0YkZBQmVJbDZhcW1iakdQdjluUT09IiwibWFjIjoiZjFjMDE1YjVlYTU2MTc5ZDliNWUzNTU5YzFhODEzOGJhZTJhMDUwMTI0NzBhODQyMzNhZWNhMmJjN2EzMjUxNSJ9", "JP": "2", "Progressfinis": 0, "altenatUrl": "sang_desainer_kelas", "banner": "", "quota": null, "rating": 4.8, "thumbnail": "https://s3.ap-southeast-1.amazonaws.com/assets.proedu.id/event/20210503172542_thumbnail.png", "title": "Guru - Sang Desainer Kelas", "usersRegistration": "71"}
  const defaultImgEmpty =
    'https://blog.zumrotutholibin.or.id/wp-content/uploads/2018/10/header-image-1-2.png';
  let items = item;
  let html;
  if (items == undefined || items.length < 1) {
    html = <Text style={styling.emptyText}>Belum Ada {title}</Text>;
  } else {
    html = items.map((item, key) => (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('SeminarStack', {
            screen: 'Eksplor Seminar',
            id: item.EventId == undefined ? item.id : item.EventId,
          });
        }}
        key={key}>
        <View style={styles.onlineCard}>
          <View style={styles.cardHead}>
            <Image
              style={styles.cardHeadImg}
              source={{
                uri: item.thumbnail == '' ? defaultImgEmpty : item.thumbnail,
              }}
            />
            <View style={styles.jpContainer}>
              <View style={styles.jpDiv}>
                <Text style={styles.number}>{item.JP}</Text>
              </View>
              <Text style={styles.jpText}>JP</Text>
            </View>
          </View>
          <View style={styles.cardBody}>
            <Text style={styles.cardBodyTitle} numberOfLines={1}>
              {item.title}
            </Text>

            <View style={styles.ratingContainer}>
              <Image style={styles.star} source={starIcon} />
              <Text style={styles.ratingText}>
                {item.EventRating?.rateStarFloat}
              </Text>
              <Text style={styles.ratingText}>
                {item.rating ? item.rating : '-'}
              </Text>
            </View>
          </View>
          <View
            style={{
              height: 20,
              backgroundColor: colors.color.grey3,
              width: '100%',
              padding: 2,
              borderBottomEndRadius: 8,
              borderBottomStartRadius: 8,
            }}>
            <Text
              style={{
                fontSize: 10,
                alignSelf: 'center',
                color: colors.color.greenPrimary,
              }}>
              {item.Progressfinis}%
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    ));
  }
  return (
    <View
      style={[
        styling.listContainer,
        styling.horizontalHeight,
        items == undefined || items.length < 1 ? styling.emptyList : null,
      ]}>
      <Text
        style={{
          paddingHorizontal: 10,
          fontWeight: 'bold',
          fontSize: 14,
          marginBottom: 15,
        }}>
        {title}
      </Text>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {html}
      </ScrollView>
    </View>
  );
}

function RekomendasiItem({item, title, navigation}) {
  const route = useRoute();
  const defaultImgEmpty =
    'https://blog.zumrotutholibin.or.id/wp-content/uploads/2018/10/header-image-1-2.png';
  let items = item;
  let html;
  if (items == undefined || items.length < 1) {
    html = <Text style={styling.emptyText}>Belum Ada {title}</Text>;
  } else {
    html = items.map((item, key) => (
      <View style={styles.item} key={key}>
        <View style={styles.content}>
          <Image
            style={styles.rekomendasiImage}
            source={{
              uri: item.thumbnail == '' ? defaultImgEmpty : item.thumbnail,
            }}
          />
          <View style={styles.jpContainer}>
            <View style={styles.jpDiv}>
              <Text style={styles.number}>{item.JP}</Text>
            </View>
            <Text style={styles.jpText}>JP</Text>
          </View>
        </View>
        <View style={styles.info}>
          <Text
            style={[styles.cardBodyTitle, {color: colors.color.black}]}
            numberOfLines={1}>
            {item.title}
          </Text>
          <View style={styles.timeContainer}>
            <Image source={clockIcon} />
            <Text style={styles.time}>
              {item.schedules?.datehEventStartShort}{' '}
              {item.schedules?.mounthEventStartShort} -{' '}
              {item.schedules?.timeEventStart} WIB
            </Text>
          </View>
          <Text style={styles.hargaCoret}>
            {!item.priceOri.includes('FREE') ? item.priceOri : ''}
          </Text>
          <Text style={styles.harga}>{item.price}</Text>
          <View style={styles.ratingContainer}>
            <Image
              style={{width: 15, resizeMode: 'contain'}}
              source={starIcon}
            />
            <Text style={styles.rating}>{item.EventRating?.rateStarFloat}</Text>
            <Text style={styles.amount}>({item.EventRating?.users})</Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              if (route.name === 'Eksplor Seminar') {
                navigation.replace('SeminarStack', {
                  screen: 'Eksplor Seminar',
                  id: item.EventId == undefined ? item.id : item.EventId,
                });
              } else {
                navigation.navigate('SeminarStack', {
                  screen: 'Eksplor Seminar',
                  id: item.EventId == undefined ? item.id : item.EventId,
                });
              }
            }}>
            <View style={styles.beliBtn}>
              <Text style={styles.beliText}>Beli</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    ));
  }
  return (
    <View
      style={[
        styling.listContainer,
        items == undefined || items.length < 1 ? styling.emptyList : null,
        {height: 320},
      ]}>
      <Text
        style={{
          paddingHorizontal: 10,
          fontWeight: 'bold',
          fontSize: 14,
          marginBottom: 15,
        }}>
        {title}
      </Text>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {html}
      </ScrollView>
    </View>
  );
}

function SeminarDefault({item, title, navigation, online}) {
  const defaultImgEmpty =
    'https://blog.zumrotutholibin.or.id/wp-content/uploads/2018/10/header-image-1-2.png';
  let items = item;
  let html;
  const heightOnline = 245;
  if (items == undefined || items.length < 1) {
    html = <Text style={styling.emptyText}>Belum Ada {title}</Text>;
  } else {
    html = items.map((item, key) => (
      <TouchableOpacity
        style={[styles.item, {height: online ? 240 : 273}]}
        key={key}
        onPress={() => {
          navigation.navigate('SeminarStack', {
            screen: 'Eksplor Seminar',
            id: item.EventId == undefined ? item.id : item.EventId,
          });
        }}>
        <View style={styles.content}>
          <Image
            style={styles.thumbnail}
            source={{
              uri: item.thumbnail == '' ? defaultImgEmpty : item.thumbnail,
            }}
          />
          <View style={styles.jpContainer}>
            <View style={styles.jpDiv}>
              <Text style={styles.number}>{item.JP}</Text>
            </View>
            <Text style={styles.jpText}>JP</Text>
          </View>
        </View>
        <View style={styles.info}>
          <Text
            style={[styles.cardBodyTitle, {color: colors.color.black}]}
            numberOfLines={1}>
            {item.title}
          </Text>
          <View style={styles.timeContainer}>
            <Image source={clockIcon} />

            <Text style={styles.time}>
              {item.schedules?.datehEventStartShort}{' '}
              {item.schedules?.mounthEventStartShort} -{' '}
              {item.schedules?.timeEventStart} WIB
            </Text>
          </View>
          {item.priceOri !== 'FREE' ? (
            <Text style={styles.hargaCoret}>{item.priceOri}</Text>
          ) : (
            <View style={{height: 16, backgroundColor: 'white'}}></View>
          )}

          <Text style={styles.harga}>
            {item.priceDisc ? item.priceDisc : 'FREE'}
          </Text>

          {!online && (
            <View style={styles.kuotaContainer}>
              <Text style={styles.kuotaText}>Kuota Peserta</Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                {/* <Ionicons name="person-outline" size={12} color="#ececec" /> */}
                <Text style={styles.registered}>
                  {item.UsersRegisted?.registed}
                </Text>
                <Text style={styles.capacity}>
                  /{item.UsersRegisted?.quota}
                </Text>
              </View>
            </View>
          )}

          <TouchableOpacity
            onPress={() => {
              navigation.navigate('SeminarStack', {
                screen: 'Eksplor Seminar',
                id: item.EventId == undefined ? item.id : item.EventId,
              });
            }}>
            <View style={styles.beliBtn}>
              <Text style={styles.beliText}>Daftar</Text>
            </View>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    ));
  }
  return (
    <View
      style={[
        styling.listContainer,
        items == undefined || items.length < 1 ? styling.emptyList : null,
        {height: 320},
      ]}>
      <Text
        style={{
          paddingHorizontal: 10,
          fontWeight: 'bold',
          fontSize: 14,
          marginBottom: 15,
        }}>
        {title}
      </Text>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {html}
      </ScrollView>
    </View>
  );
}

function SeminarPackage({item, title, navigation}) {
  const defaultImgEmpty =
    'https://blog.zumrotutholibin.or.id/wp-content/uploads/2018/10/header-image-1-2.png';
  let items = item;
  let html;
  if (items == undefined || items.length < 1) {
    html = <Text style={styling.emptyText}>Belum Ada {title}</Text>;
  } else {
    html = items.map((item, key) => (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('SeminarStack', {
            screen: 'Eksplor Seminar',
            id: item.EventId == undefined ? item.id : item.EventId,
          });
        }}
        key={key}>
        <View style={styles.packageContainer}>
          <View>
            <Image
              style={styles.thumbnailPackage}
              source={{
                uri: item.thumbnail == '' ? defaultImgEmpty : item.thumbnail,
              }}
            />
            <View style={styles.jpContainerPackage}>
              <View style={styles.jpDiv}>
                <Text style={styles.number}>{item.JP}</Text>
              </View>
              <Text style={styles.jpText}>JP</Text>
            </View>
          </View>

          <View>
            <Text
              style={[styles.cardBodyTitle, {width: 120, marginBottom: 2}]}
              numberOfLines={1}>
              {item.title}
            </Text>
            <View style={[styles.timeContainer, {marginBottom: 0}]}>
              <Image source={clockIcon} />

              <Text style={styles.time}>
                {item.schedules?.datehEventStartShort}{' '}
                {item.schedules?.mounthEventStartShort} -{' '}
                {item.schedules?.timeEventStart} WIB
              </Text>
            </View>
            <View style={[styles.ratingContainer, {flex: 1}]}>
              <Image
                style={{width: 15, resizeMode: 'contain'}}
                source={starIcon}
              />
              <Text style={styles.rating}>
                {item.EventRating?.rateStarFloat}
              </Text>
              <Text style={styles.amount}>({item.EventRating?.users})</Text>
            </View>
            <Text style={[styles.hargaCoret, {alignSelf: 'flex-end'}]}>
              {item.priceOri}
            </Text>
            <Text style={[styles.harga, {alignSelf: 'flex-end'}]}>
              {item.price}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    ));
  }
  return (
    <View
      style={[
        styling.listContainer,
        items == undefined || items.length < 1 ? styling.emptyList : null,
        {height: 180},
      ]}>
      <Text
        style={{
          paddingHorizontal: 10,
          fontWeight: 'bold',
          fontSize: 14,
          marginBottom: 15,
        }}>
        {title}
      </Text>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {html}
      </ScrollView>
    </View>
  );
}

function SeminarGet({token, type, seminar, navigation}) {
  const [loading, setLoading] = useState(true);
  const [listData, setListData] = useState(null);

  let title;
  let url;
  let parent;
  let component;
  let view = 'seminar';
  if (seminar != undefined) {
    parent = seminar;
  }
  switch (type) {
    case 'SEMINAR':
      title = 'Seminar Akan Datang';
      url =
        baseUrl +
        '/v2/event/list?type=single&typeevent=seminar&subtype=none&sort=asc&status=all&page=1&limite=100';
      component = (
        <SeminarDefault item={listData} title={title} navigation={navigation} />
      );
      break;
    case 'COURSE_ONLINE':
      title = 'Rekomendasi Online Course Terbaik';
      url =
        baseUrl +
        '/v2/event/list?type=all&typeevent=onlinecourse&subtype=none&sort=desc&status=all&page=1&limite=100';
      view = 'ecourse';
      component = (
        <RekomendasiItem
          item={listData}
          title={title}
          navigation={navigation}
        />
      );
      break;
    case 'PACKAGE':
      title = 'Paket Seminar';
      url =
        baseUrl +
        '/v2/event/list?type=package&typeevent=all&subtype=none&sort=desc&status=all&page=1&limite=100';
      view = 'package';
      component = (
        <SeminarPackage item={listData} title={title} navigation={navigation} />
      );
      break;
    case 'MY_RUNNING_SEMINAR':
      title = 'Seminar Sedang Berlangsung';
      url =
        baseUrl +
        '/v1/1/event/list/participant?type=all&status=running&sort=asc&offset=1';
      component = (
        <ListOfItems item={listData} title={title} navigation={navigation} />
      );
      break;
    case 'MY_SEMINAR':
      title = 'Seminar Saya yang Akan Datang';
      url =
        baseUrl +
        '/v2/event/list/participant/approach?type=seminar&sort=asc&limit=5&page=1';
      component = (
        <ListOfItems item={listData} title={title} navigation={navigation} />
      );
      break;
    case 'MY_COURSE_ONLINE':
      title = 'Online Course Anda';
      url =
        baseUrl +
        '/v2/event/list/participant/approach?type=onlinecourse&sort=desc&limit=5&page=1';
      view = 'ecourse';
      component = (
        <OnlineCourseItems
          item={listData}
          title={title}
          navigation={navigation}
        />
      );
      break;
    case 'MY_EVENT_DONE':
      title = 'Seminar dan Online Course yang Selesai';
      url =
        baseUrl +
        '/v2/event/list/participant?type=all&status=closed&sort=desc&offset=1';
      component = (
        <ListOfItems item={listData} title={title} navigation={navigation} />
      );
      break;
    case 'SEMINAR_RELATED':
      title = 'Rekomendasi Terkait';
      url = baseUrl + '/v1/event/detail/' + parent + '/relate';
      component = (
        <RekomendasiItem
          item={listData}
          title={title}
          navigation={navigation}
        />
      );
      break;
    case 'ONLINE_POPULAR':
      title = 'Online Course Populer';
      url =
        baseUrl +
        '/v2/event/list?type=all&typeevent=onlinecourse&subtype=popular&sort=desc&status=all&page=1&limite=100';
      component = (
        <SeminarDefault
          item={listData}
          online={true}
          title={title}
          navigation={navigation}
        />
      );
      break;

    default:
      break;
  }
  useEffect(() => {
    axios({
      url: url,
      method: 'GET',
      headers: {
        token: token,
      },
    })
      .then(function (res) {
        let temporary = Array.isArray(res.data.result)
          ? res.data.result
          : res.data.result.content;
        if (type == 'SEMINAR') {
          let collect_webinar = [];
          temporary.map(item => {
            if (item.eventStatus != 'CLOSED') collect_webinar.push(item);
          });
          setListData(collect_webinar);
        } else {
          setListData(temporary);
        }
        setLoading(false);
      })
      .catch(function (err) {
        console.log(err.response);
      });

    return () => {
      setListData([]);
    };
  }, []);
  return (
    <View style={{flex: 1}}>
      {/* {loading ? (
        <Loader />
      ) : title.includes('Online Course Anda') ? (
        <OnlineCourseItems
          item={listData}
          title={title}
          navigation={navigation}
        />
      ) : title.includes('Rekomendasi Online') ? (
        <RekomendasiItem
          item={listData}
          title={title}
          navigation={navigation}
        />
      ) : (
        <ListOfItems item={listData} title={title} navigation={navigation} />
      )} */}

      {loading ? <Loader /> : component}
    </View>
  );
}

export function MySeminarList({navigation}) {
  const [token, setToken] = useState(null);
  useEffect(() => {
    setTimeout(async () => {
      let token = await AsyncStorage.getItem('userToken');
      setToken(token);
    }, 250);
  }, []);
  return (
    <View style={{width: '100%', flex: 1}}>
      {token == null ? null : (
        <View>
          <SeminarGet
            token={token}
            type="MY_RUNNING_SEMINAR"
            navigation={navigation}
          />
          <SeminarGet token={token} type="MY_SEMINAR" navigation={navigation} />
          <SeminarGet
            token={token}
            type="MY_COURSE_ONLINE"
            navigation={navigation}
          />
          <SeminarGet
            token={token}
            type="MY_EVENT_DONE"
            navigation={navigation}
          />
        </View>
      )}
    </View>
  );
}

export function SeminarList({navigation}) {
  const [token, setToken] = useState(null);
  useEffect(() => {
    setTimeout(async () => {
      let token = await AsyncStorage.getItem('userToken');
      setToken(token);
    }, 250);
  }, []);
  return (
    <View style={{width: '100%', flex: 1}}>
      {token == null ? null : (
        <View>
          <SeminarGet token={token} type="SEMINAR" navigation={navigation} />
          <SeminarGet token={token} type="PACKAGE" navigation={navigation} />
          <SeminarGet
            token={token}
            type="COURSE_ONLINE"
            navigation={navigation}
          />
        </View>
      )}
    </View>
  );
}

export function WebinarExplore({navigation}) {
  const [token, setToken] = useState(null);
  useEffect(() => {
    setTimeout(async () => {
      let token = await AsyncStorage.getItem('userToken');
      setToken(token);
    }, 250);
  }, []);
  return (
    <View style={{width: '100%', flex: 1}}>
      {token == null ? null : (
        <View>
          <SeminarGet token={token} type="SEMINAR" navigation={navigation} />
          <SeminarGet token={token} type="PACKAGE" navigation={navigation} />
        </View>
      )}
    </View>
  );
}

export function OnlineCourseExplore({navigation}) {
  const [token, setToken] = useState(null);
  useEffect(() => {
    setTimeout(async () => {
      let token = await AsyncStorage.getItem('userToken');
      setToken(token);
    }, 250);
  }, []);
  return (
    <View style={{width: '100%', flex: 1}}>
      {token == null ? null : (
        <View>
          <SeminarGet
            token={token}
            type="ONLINE_POPULAR"
            navigation={navigation}
          />
        </View>
      )}
    </View>
  );
}

export function SearchEvent({navigation, input}) {
  const [token, setToken] = useState(null);

  useEffect(() => {
    setTimeout(async () => {
      let token = await AsyncStorage.getItem('userToken');
      setToken(token);
    }, 250);
  }, []);
  return (
    <View style={{width: '100%', flex: 1}}>
      {token == null ? null : (
        <View>
          <SeminarGet
            token={token}
            type="SEARCH"
            input={input}
            navigation={navigation}
          />
        </View>
      )}
    </View>
  );
}

export function SeminarRecommendation({navigation, seminar}) {
  const [token, setToken] = useState(null);

  useEffect(async () => {
    let token = await AsyncStorage.getItem('userToken');
    setTimeout(() => {
      setToken(token);
    }, 250);

    return () => {
      setToken(null);
    };
  }, []);
  return (
    <View style={{width: '100%', flex: 1}}>
      {token == null ? null : (
        <View>
          <SeminarGet
            navigation={navigation}
            token={token}
            seminar={seminar}
            type="SEMINAR_RELATED"
          />
        </View>
      )}
    </View>
  );
}

const styling = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  loaderContainer: {
    width: '100%',
    height: 235,
    paddingHorizontal: 20,
    paddingVertical: 0,
    marginTop: 0,
  },
  listContainer: {
    width: '100%',
    paddingHorizontal: 10,
    marginVertical: 15,
  },
  horizontalHeight: {
    height: 230,
  },
  emptyList: {
    height: 'auto',
    flex: 1,
    marginBottom: 50,
  },
  emptyText: {
    fontStyle: 'italic',
    marginHorizontal: 10,
  },
});

const styles = StyleSheet.create({
  inline: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  txtWarning: {
    color: colors.color.red,
    fontSize: 10,
  },
  card: {
    height: 190,
    width: 155,
    marginHorizontal: 10,
    alignItems: 'center',
    elevation: 35,
  },
  cardHead: {
    width: '100%',
    height: 80,
  },
  cardHeadImg: {
    width: '100%',
    height: 80,
    resizeMode: 'cover',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  cardBody: {
    backgroundColor: 'white',
    width: '100%',
    flex: 1,
    padding: 5,
  },
  cardBodyTitle: {
    fontSize: 12,
    fontWeight: 'bold',
    marginBottom: 10,
    // height: 45,
  },
  cardBodyData: {
    fontSize: 12,
  },
  clockIcon: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
  jpContainer: {
    width: 49,
    height: 24,
    backgroundColor: '#002554',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 14,
    position: 'absolute',
    right: 5,
    top: 5,
  },
  number: {
    color: '#ccc',
    fontSize: 10,
    color: '#002554',
  },
  jpDiv: {
    backgroundColor: '#F9E246',
    borderRadius: 14,
    width: 24,
    height: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  jpText: {
    fontSize: 10,
    color: '#ccc',
    marginRight: 8,
  },
  onlineCard: {
    height: 160,
    width: 155,
    marginHorizontal: 10,
    alignItems: 'center',
    elevation: 35,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ratingText: {
    color: '#6C757C',
    fontSize: 12,
    marginRight: 5,
  },

  star: {
    width: 12,
    height: 12,
    marginRight: 5,
  },

  item: {
    height: 270,
    width: 155,
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 6,
    marginHorizontal: 8,
  },
  content: {
    width: 155,
    height: 80,
    borderColor: '#333',
    borderWidth: 1,
    borderRadius: 8,
    alignItems: 'flex-end',
  },
  info: {
    padding: 8,
  },
  headline: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#232323',
    marginBottom: 10,
  },
  timeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 23,
  },
  time: {
    fontSize: 12,
    color: '#232323',
    marginLeft: 5,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  hargaCoret: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    textDecorationColor: '#6C757C',
    color: '#6C757C',
    fontSize: 12,
  },
  harga: {
    color: '#002F6B',
    fontSize: 16,
    fontWeight: 'bold',
  },
  rating: {
    fontSize: 16,
    marginHorizontal: 5,
    color: '#6C757C',
  },
  amount: {
    fontSize: 16,
    color: '#6C757C',
  },
  beliBtn: {
    width: 139,
    height: 32,
    backgroundColor: '#00AF68',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  beliText: {
    color: '#fff',
    fontSize: 16,
  },

  rekomendasiImage: {
    width: '100%',
    height: 80,
    resizeMode: 'cover',
    borderRadius: 8,
  },

  capacity: {
    fontSize: 12,
    color: 'grey',
  },
  registered: {
    fontSize: 12,
    color: '#fff',
    marginLeft: 5,
  },
  thumbnail: {
    width: 153,
    height: 78,
    borderRadius: 8,
  },
  kuotaText: {
    fontSize: 10,
    color: '#fff',
  },

  kuotaContainer: {
    height: 28,
    backgroundColor: '#9FC359',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 5,
    marginTop: 4,
    marginBottom: 8,
  },

  packageContainer: {
    height: 120,
    width: 273,
    backgroundColor: '#fff',
    borderRadius: 8,
    elevation: 6,
    marginHorizontal: 8,
    flexDirection: 'row',
  },

  thumbnailPackage: {
    height: 120,
    width: 120,
    resizeMode: 'cover',
    borderRadius: 8,
    marginRight: 12,
  },

  jpContainerPackage: {
    width: 49,
    height: 24,
    backgroundColor: '#002554',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 14,
    position: 'absolute',
    top: 5,
    left: 65,
  },
});
