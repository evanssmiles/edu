import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  Linking,
  StyleSheet,
  RefreshControl,
  ToastAndroid,
} from 'react-native';
import {colors} from '../../styling';
import {Button} from '../../component';
import {baseUrl, SeminarRecommendation} from './SeminarList';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Video from 'react-native-video';
import Modal from 'react-native-modal';
import {Rating, AirbnbRating} from 'react-native-ratings';

import LinearGradient from 'react-native-linear-gradient';
import dateIcon from '../../assets/icon/dateIcon.png';
import videoIcon from '../../assets/icon/playIcon.png';
import docIcon from '../../assets/icon/docIcon.png';
import certiIcon from '../../assets/icon/checkGreenLine.png';
import arrowForward from '../../assets/icon/arrow_forward_black.png';
import arrowDown from '../../assets/icon/arrow_downward.png';
import checkIcon from '../../assets/icon/checkGreen.png';
import userIcon from '../../assets/icon/person.png';
import starFill from '../../assets/icon/star_filled.png';
import starIcon from '../../assets/icon/star.png';
import iconInfoGrey from '../../assets/icon/infoGrey.png';
import iconInfoRed from '../../assets/icon/infoRed.png';
import {color} from '../../styling/color';

function SeminarRelated({token, seminarId, navigation}) {
  return (
    <SeminarRecommendation
      navigation={navigation}
      token={token}
      seminar={seminarId}
    />
  );
}

function SeminarDetail({navigation, id, justPublished}) {
  const wait = timeout => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  };
  const [refreshing, setRefreshing] = React.useState(false);
  const [zoomLink, setZoomLink] = useState(null);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  const defaultImgEmpty =
    'https://blog.zumrotutholibin.or.id/wp-content/uploads/2018/10/header-image-1-2.png';
  let getToken;
  const [loading, setLoading] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [certificateData, setCertificateData] = useState(null);
  const [seminarStatus, setSeminarStatus] = useState({
    seminarId: null,
    type: 'webinar',
    isJoin: false,
    isDone: false,
    taskFinish: false,
    certificatePublished: false,
    data: null,
  });
  const [activityTask, setActivityTask] = useState(null);

  useEffect(async () => {
    getToken = await AsyncStorage.getItem('userToken');
    axios({
      url: baseUrl + '/v1/1/event/detail/' + id,
      method: 'GET',
      headers: {
        token: getToken,
      },
    })
      .then(function (response) {
        console.log(response.data.result, '<<<<86');
        let collect = {
          id: response.data.result.eventId,
          type: 'webinar',
          join: false,
          done: false,
          publish: false,
          task: false,
          data: null,
        };
        if (response.data.message == 'CERTIFICATE') {
          setCertificateData(response.data.result.certificate);
          collect.data = response.data.result.description;
          collect.join = true;
          if (
            response.data.result.description.SuperRedeem != undefined &&
            !response.data.result.description.SuperRedeem
          ) {
            collect.type = 'online_course';
            axios({
              // get percentage of task completion
              url: baseUrl + '/v1/1/event/detail/' + id + '/percent',
              method: 'GET',
              headers: {
                token: getToken,
              },
            })
              .then(function (res) {
                collect.task = res.data.result;
              })
              .catch(function (err) {
                console.log(err.response);
              });
            axios({
              //getting all task list
              url: baseUrl + '/v1/1/event/detail/' + id + '/activity',
              method: 'get',
              headers: {
                token: getToken,
              },
            })
              .then(function (res) {
                if (res.status < 300) {
                  // console.log(res.data.result[0].sub, '<<<<<133');
                  setActivityTask(res.data.result[0].sub);
                }
              })
              .catch(function (err) {
                console.log(id, '<<<138');
                console.log(err.response);
              });
          }
          if (
            response.data.result.certificate.CERTIFICATE != undefined &&
            response.data.result.certificate.CERTIFICATE == 'TRUE'
          ) {
            collect.publish = true;
          }
          if (response.data.result.description.eventStatus == 'CLOSED') {
            collect.done = true;
          }
        } else if (response.data.message == 'EVENT') {
          collect.data = response.data.result;
          if (response.data.result.eventStatus == 'CLOSED') {
            collect.type = 'online_course';
          }
        }
        setSeminarStatus({
          seminarId: collect.id,
          type: collect.type,
          isJoin: collect.join,
          isDone: collect.done,
          taskFinish: collect.task,
          certificatePublished: collect.publish,
          data: collect.data,
        });
        setLoading(false);
      })
      .catch(function (error) {
        console.log(error.response);
      });
  }, []);

  console.log(certificateData, '<<<<169');

  const [canPressJoin, setCanPressJoin] = useState(true);
  const JoinSeminar = async () => {
    setCanPressJoin(false);
    getToken = await AsyncStorage.getItem('userToken');
    axios({
      url: baseUrl + '/v2/event/' + seminarStatus.seminarId + '/joinus',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        token: getToken,
      },
    })
      .then(function (res) {
        setSeminarStatus({
          ...seminarStatus,
          isJoin: true,
        });
        setCanPressJoin(true);
        setZoomLink(res.data.result.zoomLink);
      })
      .catch(function (error) {
        console.log(error.res);
        setCanPressJoin(true);
      });
  };

  console.log(seminarStatus.certificatePublished, '<<<<196');

  const addToCart = async () => {
    getToken = await AsyncStorage.getItem('userToken');
    axios({
      url:
        baseUrl + '/v1/transaction/event/' + seminarStatus.seminarId + '/add',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        token: getToken,
      },
    })
      .then(function (response) {
        if (response.status < 300) {
          setModalVisible(true);
        }
      })
      .catch(function (error) {
        ToastAndroid.show('Event ini sudah anda beli', ToastAndroid.SHORT);
      });
  };

  const BuyNow = async data => {
    let getToken = await AsyncStorage.getItem('userToken');
    axios({
      url: baseUrl + `/v1/transaction/event/${data.data.eventId}/buynow`,
      method: 'post',
      headers: {
        token: getToken,
      },
    })
      .then(function (res) {
        if (res.status < 300) {
          navigation.replace('PaymentStack', {
            screen: 'Direct',
            data: {seminar: seminarStatus, buyNow: 'buynow'},
          });
        }
        setLoading(false);
      })
      .catch(function (err) {
        ToastAndroid.show('Event ini sudah anda beli', ToastAndroid.SHORT);
      });
  };

  const regex = /(<([^>]+)>)/gi;

  const OnlineCourse = () => {
    let bottomSpaceContainer = {paddingBottom: 220};
    if (!seminarStatus.isJoin && seminarStatus.data.EventTypePrice != 'PAID') {
      bottomSpaceContainer = {paddingBottom: 135};
    } else if (seminarStatus.isJoin) {
      bottomSpaceContainer = {paddingBottom: 85};
    }

    return (
      <View style={[styles.mainContainer, bottomSpaceContainer]}>
        <ScrollView
          style={[styles.mainContainer]}
          nestedScrollEnabled={true}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          {!seminarStatus.isJoin && <VideoPreview data={seminarStatus.data} />}
          <PrimaryInformation data={seminarStatus.data} />
          <SecondaryTabInformation data={seminarStatus.data} />
        </ScrollView>
        <BottomAction data={seminarStatus.data} eventType="online_course" />
      </View>
    );
  };

  const Webinar = () => {
    let bottomSpaceContainer = {paddingBottom: 220};
    if (
      (seminarStatus.isJoin || seminarStatus.data.EventTypePrice != 'PAID') &&
      !seminarStatus.certificatePublished &&
      !justPublished
    ) {
      bottomSpaceContainer = {paddingBottom: 150};
    } else if (
      seminarStatus.isJoin &&
      seminarStatus.isDone &&
      (seminarStatus.certificatePublished || justPublished)
    ) {
      bottomSpaceContainer = {paddingBottom: 85};
    }

    return (
      <View style={[styles.mainContainer, bottomSpaceContainer]}>
        <ScrollView
          style={[styles.mainContainer]}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          {!seminarStatus.isJoin && <VideoPreview data={seminarStatus.data} />}
          <PrimaryInformation data={seminarStatus.data} />
          <Description data={seminarStatus.data} />
          <SpeakerInformation data={seminarStatus.data} />
          <SeminarRelated
            navigation={navigation}
            token={getToken}
            seminarId={seminarStatus.seminarId}
          />
        </ScrollView>
        <BottomAction data={seminarStatus.data} eventType="webinar" />
      </View>
    );
  };

  const VideoPreview = ({data}) => {
    const [videos, setVideos] = useState(data.assets.video);
    return (
      <>
        {videos == null ? (
          <></>
        ) : (
          <Video
            source={{uri: data.assets.video}}
            style={{width: '100%', height: 200}}
            // controls={true}
            resizeMode={'cover'}
          />
        )}
      </>
    );
  };

  const PrimaryInformation = ({data}) => {
    return (
      <View style={[styles.descContainer]}>
        <View style={[styles.inline, {alignItems: 'flex-start'}]}>
          <View style={{flex: 1}}>
            <Text style={[styles.title]}>{data.title}</Text>
            <Text>
              Oleh:&nbsp;
              {data.Speaker.map((item, key) => {
                let txt =
                  key + 1 < data.Speaker.length ? item.name + ', ' : item.name;
                return txt;
              })}
            </Text>
            <View style={[styles.inline]}>
              <Image source={dateIcon} />
              <Text style={[styles.txtIcon]}>
                {data.dateEventStart},{' '}
                {data.timeEventStart + ' - ' + data.timeEventEnd + ' WIB'}
              </Text>
            </View>
            <Text style={[styles.txtGreenLime]}>
              {data.reteEvent + ' Poin'}
            </Text>
          </View>
          <View
            style={[
              {
                backgroundColor: colors.color.greenShape,
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 10,
                width: 70,
                borderRadius: 15,
              },
            ]}>
            <Image source={userIcon} style={{width: 15, height: 15}} />
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Text style={{fontSize: 16, color: 'white'}}>50</Text>
              <Text style={{fontSize: 16, color: colors.color.grey}}>/200</Text>
            </View>
          </View>
        </View>
        <View style={[styles.inline]}>
          <View style={[styles.inline, styles.pillsTab]}>
            <Image source={videoIcon} />
            <Text style={[styles.txtIcon]}>1 Video</Text>
          </View>
          <View style={[styles.inline, styles.pillsTab]}>
            <Image source={docIcon} />
            <Text style={[styles.txtIcon]}>Dokumen</Text>
          </View>
          <View style={[styles.inline, styles.pillsTab]}>
            <Image source={certiIcon} />
            <Text style={[styles.txtIcon]}>E-Sertifikat</Text>
          </View>
        </View>
      </View>
    );
  };

  const SecondaryTabInformation = ({data}) => {
    const [tabActive, setTabActive] = useState('description');
    const tabStyle = StyleSheet.create({
      parent: {
        flexDirection: 'row',
        width: '100%',
        flex: 1,
        backgroundColor: colors.color.grey3,
      },
      tab: {
        width: 150,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 2,
        borderColor: colors.color.grey,
      },
      tabActive: {
        borderBottomWidth: 2,
        borderColor: colors.color.yellowHover,
      },
      menu: {
        padding: 15,
      },
    });
    return (
      <View>
        <ScrollView
          style={[tabStyle.parent]}
          horizontal={true}
          showsHorizontalScrollIndicator={false}>
          <View
            style={[
              tabStyle.tab,
              tabActive == 'description' ? tabStyle.tabActive : null,
            ]}>
            <TouchableOpacity
              style={[tabStyle.menu]}
              onPress={() => {
                setTabActive('description');
              }}>
              <Text>Deskripsi</Text>
            </TouchableOpacity>
          </View>
          <View
            style={[
              tabStyle.tab,
              tabActive == 'pemateri' ? tabStyle.tabActive : null,
            ]}>
            <TouchableOpacity
              style={[tabStyle.menu]}
              onPress={() => {
                setTabActive('pemateri');
              }}>
              <Text>Pemateri</Text>
            </TouchableOpacity>
          </View>
          {/* <View style={[tabStyle.tab, tabActive=='ulasan'?tabStyle.tabActive:null]}>
                        <TouchableOpacity style={[tabStyle.menu]} onPress={()=>{setTabActive('ulasan')}} >
                            <Text>Ulasan</Text>
                        </TouchableOpacity>
                    </View> */}
          <View
            style={[
              tabStyle.tab,
              tabActive == 'rekomendasi' ? tabStyle.tabActive : null,
            ]}>
            <TouchableOpacity
              style={[tabStyle.menu]}
              onPress={() => {
                setTabActive('rekomendasi');
              }}>
              <Text>Rekomendasi</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <View style={{width: '100%', flex: 1, backgroundColor: 'white'}}>
          {tabActive == 'description' && <Description data={data} />}
          {tabActive == 'pemateri' && <SpeakerInformation data={data} />}
          {/* { tabActive=='ulasan' && (<Review data={data} />) } */}
          {tabActive == 'rekomendasi' && (
            <SeminarRelated
              navigation={navigation}
              token={getToken}
              seminarId={seminarStatus.seminarId}
            />
          )}
        </View>
      </View>
    );
  };

  const Description = ({data}) => {
    return (
      <View style={[styles.descContainer, {marginTop: 15}]}>
        <Text style={[styles.title]}>Deskripsi</Text>
        <Text>{data.description.replace(regex, '')}</Text>
      </View>
    );
  };

  const Review = ({data}) => {
    const reviewStyle = StyleSheet.create({
      centerMidInline: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 2,
        marginRight: 5,
      },
      summary: {
        flexDirection: 'row',
        alignItems: 'center',
      },
      sumScore: {
        alignItems: 'center',
      },
      sumDetail: {
        flex: 1,
        marginLeft: 25,
        paddingVertical: 5,
      },
      totalScore: {
        fontSize: 24,
        color: colors.color.yellowPrimary,
        margin: 0,
        padding: 0,
      },
      maxScore: {
        fontSize: 16,
        color: colors.color.grey2,
        margin: 0,
        padding: 0,
        lineHeight: 26,
      },
      reviewerTotal: {
        fontSize: 12,
        marginTop: 5,
        color: colors.color.grey,
      },
      progressContainer: {
        height: 10,
        flex: 1,
        backgroundColor: 'white',
        borderColor: colors.color.grey2,
        borderWidth: 1,
        marginRight: 15,
      },
      progressBar: {
        width: '50%',
        flex: 1,
        backgroundColor: colors.color.grey2,
      },
      comment: {},
      imgStarBig: {
        width: 20,
        height: 20,
        marginRight: 5,
      },
      imgStarLittle: {
        width: 12,
        height: 12,
        marginRight: 1,
      },
    });

    const commentStyle = StyleSheet.create({
      container: {
        padding: 5,
        height: 300,
        paddingBottom: 5,
      },
      item: {
        height: 100,
        borderTopWidth: 1,
        borderTopColor: colors.color.grey3,
        marginVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
      },
      avatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 25,
      },
      detail: {
        flex: 1,
      },
      name: {
        fontWeight: 'bold',
        fontSize: 14,
        marginBottom: 5,
      },
      date: {
        flex: 1,
        fontSize: 10,
        color: colors.color.grey,
      },
      comment: {
        marginTop: 5,
        fontSize: 12,
      },
    });

    return (
      <View style={[styles.descContainer, {marginTop: 15}]}>
        <View style={[reviewStyle.summary]}>
          <View style={[reviewStyle.sumScore]}>
            <View style={[reviewStyle.centerMidInline]}>
              <Image source={starFill} style={reviewStyle.imgStarBig} />
              <Text style={[reviewStyle.totalScore]}>4.8</Text>
              <Text style={[reviewStyle.maxScore]}>/5</Text>
            </View>
            <Text style={[reviewStyle.reviewerTotal]}>175 Ulasan</Text>
          </View>
          <View style={[reviewStyle.sumDetail]}>
            <View style={[reviewStyle.centerMidInline]}>
              <View style={[reviewStyle.progressContainer]}>
                <View style={[reviewStyle.progressBar]} />
              </View>
              <View style={[reviewStyle.centerMidInline]}>
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
              </View>
              <Text>50%</Text>
            </View>
            <View style={[reviewStyle.centerMidInline]}>
              <View style={[reviewStyle.progressContainer]}>
                <View style={[reviewStyle.progressBar]} />
              </View>
              <View style={[reviewStyle.centerMidInline]}>
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starIcon} style={reviewStyle.imgStarLittle} />
              </View>
              <Text>50%</Text>
            </View>
            <View style={[reviewStyle.centerMidInline]}>
              <View style={[reviewStyle.progressContainer]}>
                <View style={[reviewStyle.progressBar]} />
              </View>
              <View style={[reviewStyle.centerMidInline]}>
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starIcon} style={reviewStyle.imgStarLittle} />
                <Image source={starIcon} style={reviewStyle.imgStarLittle} />
              </View>
              <Text>50%</Text>
            </View>
            <View style={[reviewStyle.centerMidInline]}>
              <View style={[reviewStyle.progressContainer]}>
                <View style={[reviewStyle.progressBar]} />
              </View>
              <View style={[reviewStyle.centerMidInline]}>
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starIcon} style={reviewStyle.imgStarLittle} />
                <Image source={starIcon} style={reviewStyle.imgStarLittle} />
                <Image source={starIcon} style={reviewStyle.imgStarLittle} />
              </View>
              <Text>50%</Text>
            </View>
            <View style={[reviewStyle.centerMidInline]}>
              <View style={[reviewStyle.progressContainer]}>
                <View style={[reviewStyle.progressBar]} />
              </View>
              <View style={[reviewStyle.centerMidInline]}>
                <Image source={starFill} style={reviewStyle.imgStarLittle} />
                <Image source={starIcon} style={reviewStyle.imgStarLittle} />
                <Image source={starIcon} style={reviewStyle.imgStarLittle} />
                <Image source={starIcon} style={reviewStyle.imgStarLittle} />
                <Image source={starIcon} style={reviewStyle.imgStarLittle} />
              </View>
              <Text>50%</Text>
            </View>
          </View>
        </View>
        <ScrollView
          style={[commentStyle.container]}
          nestedScrollEnabled={true}
          showsVerticalScrollIndicator={false}>
          <View style={[commentStyle.item]}>
            <Image
              style={[commentStyle.avatar]}
              source={{
                uri: 'https://sps.widyatama.ac.id/wp-content/uploads/2020/08/dummy-profile-pic-male1-300x300.jpg',
              }}></Image>
            <View style={[commentStyle.detail]}>
              <Text style={[commentStyle.name]}>Rudi Thamrin, B.A</Text>
              <View style={[reviewStyle.centerMidInline]}>
                <View style={[reviewStyle.centerMidInline]}>
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                </View>
                <Text style={[commentStyle.date]}>2 Bulan yang lalu</Text>
              </View>
              <Text style={[commentStyle.comment]}>Mantap! Bermanfaat</Text>
            </View>
          </View>
          <View style={[commentStyle.item]}>
            <Image
              style={[commentStyle.avatar]}
              source={{
                uri: 'https://sps.widyatama.ac.id/wp-content/uploads/2020/08/dummy-profile-pic-male1-300x300.jpg',
              }}></Image>
            <View style={[commentStyle.detail]}>
              <Text style={[commentStyle.name]}>Rudi Thamrin, B.A</Text>
              <View style={[reviewStyle.centerMidInline]}>
                <View style={[reviewStyle.centerMidInline]}>
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                </View>
                <Text style={[commentStyle.date]}>2 Bulan yang lalu</Text>
              </View>
              <Text style={[commentStyle.comment]}>Mantap! Bermanfaat</Text>
            </View>
          </View>
          <View style={[commentStyle.item]}>
            <Image
              style={[commentStyle.avatar]}
              source={{
                uri: 'https://sps.widyatama.ac.id/wp-content/uploads/2020/08/dummy-profile-pic-male1-300x300.jpg',
              }}></Image>
            <View style={[commentStyle.detail]}>
              <Text style={[commentStyle.name]}>Rudi Thamrin, B.A</Text>
              <View style={[reviewStyle.centerMidInline]}>
                <View style={[reviewStyle.centerMidInline]}>
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                </View>
                <Text style={[commentStyle.date]}>2 Bulan yang lalu</Text>
              </View>
              <Text style={[commentStyle.comment]}>Mantap! Bermanfaat</Text>
            </View>
          </View>
          <View style={[commentStyle.item]}>
            <Image
              style={[commentStyle.avatar]}
              source={{
                uri: 'https://sps.widyatama.ac.id/wp-content/uploads/2020/08/dummy-profile-pic-male1-300x300.jpg',
              }}></Image>
            <View style={[commentStyle.detail]}>
              <Text style={[commentStyle.name]}>Rudi Thamrin, B.A</Text>
              <View style={[reviewStyle.centerMidInline]}>
                <View style={[reviewStyle.centerMidInline]}>
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                </View>
                <Text style={[commentStyle.date]}>2 Bulan yang lalu</Text>
              </View>
              <Text style={[commentStyle.comment]}>Mantap! Bermanfaat</Text>
            </View>
          </View>
          <View style={[commentStyle.item]}>
            <Image
              style={[commentStyle.avatar]}
              source={{
                uri: 'https://sps.widyatama.ac.id/wp-content/uploads/2020/08/dummy-profile-pic-male1-300x300.jpg',
              }}></Image>
            <View style={[commentStyle.detail]}>
              <Text style={[commentStyle.name]}>Rudi Thamrin, B.A</Text>
              <View style={[reviewStyle.centerMidInline]}>
                <View style={[reviewStyle.centerMidInline]}>
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                  <Image source={starFill} style={reviewStyle.imgStarLittle} />
                </View>
                <Text style={[commentStyle.date]}>2 Bulan yang lalu</Text>
              </View>
              <Text style={[commentStyle.comment]}>Mantap! Bermanfaat</Text>
            </View>
          </View>
          <Button.filledWhite
            title="Lihat lebih banyak"
            style={{marginBottom: 25}}
          />
        </ScrollView>
      </View>
    );
  };

  const SpeakerInformation = ({data}) => {
    return (
      <View style={[styles.descContainer, {marginTop: 15}]}>
        <Text style={[styles.title]}>Pemateri</Text>
        {data.Speaker.map((item, key) => {
          return (
            <View style={[styles.inline, styles.inlineTop]} key={key}>
              <Image
                style={[styles.photoProfile]}
                source={{uri: item.photoProfiles}}
              />
              <View style={[styles.txtIcon, {flex: 1, marginLeft: 15}]}>
                <Text style={[styles.title]}>{item.name}</Text>
                <Text>{item.desc.replace(regex, '')}</Text>
              </View>
            </View>
          );
        })}
      </View>
    );
  };

  const BottomAction = ({data, eventType}) => {
    let type = 'pricing';
    let customStyle = null;
    let component = <BottomPublic data={data} eventType={eventType} />;

    if (!seminarStatus.isJoin) {
      if (data.EventTypePrice != 'PAID') customStyle = {height: 150};
    } else {
      type = 'join';
      customStyle =
        seminarStatus.type == 'webinar'
          ? {height: 155, backgroundColor: colors.color.blueShape}
          : {height: 125, backgroundColor: colors.color.blueShape};
      component = (
        <BottomJoin data={data} eventType={eventType} taskComplete={false} />
      );
      if (seminarStatus.isDone && seminarStatus.type == 'webinar') {
        //isDone is key used for seminar/webinar only
        type = 'redeem';
        customStyle = {height: 135, backgroundColor: colors.color.blueShape};
        component = <BottomJoin data={data} taskComplete={true} />;
        if (seminarStatus.certificatePublished || justPublished) {
          type = 'published';
          customStyle = {height: 85, backgroundColor: colors.color.blueShape};
          component = <BottomDone data={data} />;
        }
      }
    }

    return (
      <View style={[pricing.floatingBottom, customStyle]}>{component}</View>
    );
  };

  const CartInsert = ({eventType}) => {
    if (eventType == 'online_course') {
      return (
        <Button.filledButton
          title="+ Keranjang"
          onPress={() => {
            addToCart();
          }}
        />
      );
    }
    return (
      <Button.filledWhite
        disabled={true}
        title="+ Keranjang"
        onPress={() => {
          addToCart();
          replace;
          replace;
        }}
      />
    );
  };

  const CheckoutNow = ({eventType}) => {
    if (eventType == 'online_course') {
      return (
        <Button.filledWhite
          title="Beli Sekarang"
          // disabled={true}
          onPress={() => {
            BuyNow(seminarStatus);
          }}
        />
      );
    }
    return (
      <Button.filledButton
        title="Daftar Sekarang"
        onPress={() => {
          JoinSeminar();
        }}
      />
    );
  };

  const PurchaseOption = ({eventType}) => {
    if (eventType == 'online_course') {
      return (
        <View style={{paddingHorizontal: 15, marginVertical: 5}}>
          <CheckoutNow eventType={eventType} />
          <CartInsert eventType={eventType} />
        </View>
      );
    }
    return (
      <View style={{paddingHorizontal: 15, marginVertical: 5}}>
        <CartInsert eventType={eventType} />
        <CheckoutNow eventType={eventType} />
      </View>
    );
  };

  const JoinAsHonorer = () => {
    return (
      // <TouchableOpacity onPress={() => navigation.navigate('HonorerStack')}>
      <TouchableOpacity>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#FEBB0F', '#F9E246']}
          style={[pricing.btnHonorer]}>
          <Text
            style={{fontSize: 16, fontWeight: 'bold', marginHorizontal: 10}}>
            Daftar Gratis Untuk Guru Honorer
          </Text>
          <Image source={arrowForward} />
        </LinearGradient>
      </TouchableOpacity>
    );
  };

  const JoinFree = () => {
    return (
      <View style={{padding: 15}}>
        <Button.filledButton
          title="Ikuti Seminar"
          onPress={() => {
            JoinSeminar();
          }}
          disabled={
            seminarStatus.type == 'online_course' || !canPressJoin
              ? true
              : false
          }
        />
      </View>
    );
  };
  const ActionToEvent = ({data}) => {
    // console.log('determine action : ',activityTask)
    let passingData = {
      id: seminarStatus.data.eventId,
      title: seminarStatus.data.title,
      speaker: seminarStatus.data.Speaker,
      task: activityTask,
      published: seminarStatus.certificatePublished,
      certificateData,
    };

    if (seminarStatus.type == 'online_course') {
      return (
        <View
          style={{
            paddingHorizontal: 15,
            paddingVertical: 15,
            borderWidth: 1,
            borderColor: colors.color.bgColor,
            elevation: 6,
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            backgroundColor: 'white',
            flex: 1,
            justifyContent: 'center',
          }}>
          {activityTask == null && (
            <Text
              style={{
                fontSize: 12,
                color: colors.color.grey,
                textAlign: 'center',
              }}>
              Belum ada aktivitas saat ini, silakan kembali lain waktu
            </Text>
          )}
          {activityTask == null ? (
            <Button.filledButton disabled={true} title="Ke Aktivitas" />
          ) : (
            <Button.filledButton
              title="Ke Aktivitas"
              onPress={() => {
                navigation.navigate('ActivityStack', passingData);
              }}
            />
          )}
        </View>
      );
    }
    const openSeminarZoom = ({link}) => {
      // console.log(link)
      Linking.openURL(link);
    };
    let myLink;
    return (
      <View style={{flex: 1}}>
        <View
          style={[
            styles.inline,
            {
              backgroundColor: colors.color.blueShape,
              justifyContent: 'center',
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            },
          ]}>
          <Image source={checkIcon} />
          <Text style={[styles.txtIcon, {color: 'white'}]}>
            Anda Telah Terdaftar di Seminar Ini!
          </Text>
        </View>
        <View
          style={{
            paddingHorizontal: 15,
            paddingVertical: 5,
            borderWidth: 1,
            borderColor: 'black',
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            backgroundColor: 'white',
            flex: 1,
          }}>
          <View style={[styles.inline, {marginVertical: 0}]}>
            <Image source={iconInfoGrey}></Image>
            <Text
              style={[
                {fontSize: 12, color: colors.color.grey, marginHorizontal: 10},
              ]}>
              Seminar akan dimulai pada{' '}
              {seminarStatus.data.dateEventStart +
                ', ' +
                seminarStatus.data.timeEventStart +
                ' - ' +
                seminarStatus.data.timeEventEnd}
            </Text>
          </View>
          <Button.filledButton
            title="Masuk ke Seminar"
            onPress={() => {
              if (zoomLink) {
                myLink = zoomLink;
              } else {
                myLink = data.DetailDateEvent[0].LinkZoom;
              }

              openSeminarZoom({
                link: myLink,
              });
            }}
            disabled={data.eventStatus == 'RUNNING' ? false : true}
          />
        </View>
      </View>
    );
  };

  console.log(seminarStatus.seminarId, '<<<<1053');

  const ReadyToRedeem = () => {
    return (
      <View style={{flex: 1}}>
        <View
          style={[
            styles.inline,
            {
              backgroundColor: colors.color.blueShape,
              justifyContent: 'center',
              alignItems: 'center',
              borderTopLeftRadius: 15,
              borderTopRightRadius: 15,
            },
          ]}>
          <Image source={arrowDown} />
          <Text style={[styles.txtIcon, {color: 'white'}]}>
            Terbitkan Sertifikat Anda Sekarang!
          </Text>
        </View>
        <View
          style={{
            paddingHorizontal: 15,
            paddingVertical: 5,
            borderWidth: 1,
            borderColor: 'black',
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            backgroundColor: 'white',
            flex: 1,
          }}>
          <Button.filledButton
            onPress={() => {
              navigation.navigate('CertificateStack', {
                id: seminarStatus.seminarId,
                published: 1,
              });
            }}
            title="Terbitkan Sertifikat"
          />
        </View>
      </View>
    );
  };

  const BottomPublic = ({data, eventType}) => {
    return (
      <View style={{flex: 1}}>
        <View style={[styles.inline, pricing.price]}>
          <Text style={pricing.label}>Harga</Text>
          {data.EventTypePrice == 'PAID' ? (
            <View style={{alignItems: 'flex-end'}}>
              <Text style={pricing.cost}>
                {data.InfoPrice.DiscontPrice == ''
                  ? data.InfoPrice.OriginalPrice
                  : data.InfoPrice.DiscontPrice}
              </Text>
              <Text style={pricing.removedPrice}>
                {data.InfoPrice.DiscontPrice == ''
                  ? ''
                  : data.InfoPrice.OriginalPrice}
              </Text>
            </View>
          ) : (
            <Text style={pricing.cost}>Gratis</Text>
          )}
        </View>
        {data.EventTypePrice == 'PAID' ? (
          <PurchaseOption eventType={eventType} />
        ) : (
          <JoinFree />
        )}
        {data.EventTypePrice == 'PAID' && <JoinAsHonorer />}
      </View>
    );
  };

  const BottomJoin = ({data, eventType, taskComplete = false}) => {
    if (
      (seminarStatus.type == 'webinar' && seminarStatus.isDone) ||
      (seminarStatus.type == 'online_course' && seminarStatus.taskFinish)
    ) {
      return <ReadyToRedeem />;
    }
    return <ActionToEvent data={seminarStatus.data} />;
  };

  const BottomDone = () => {
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            padding: 15,
            borderWidth: 1,
            borderColor: 'white',
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            backgroundColor: 'white',
          }}>
          <Button.filledButton
            onPress={() => {
              navigation.navigate('CertificateStack', {
                id: seminarStatus.seminarId,
                published: 3,
                certificateData: certificateData,
              });
            }}
            title="Lihat Sertifikat"
          />
        </View>
      </View>
    );
  };

  return (
    <View style={styles.mainContainer}>
      <Modal
        isVisible={modalVisible}
        onBackdropPress={() => setModalVisible(false)}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View style={{backgroundColor: 'white', padding: 20}}>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 16,
                fontWeight: 'bold',
                marginBottom: 20,
              }}>
              Berhasil Ditambahkan!
            </Text>
            <Text style={{textAlign: 'center', fontSize: 14}}>
              {seminarStatus.data != null ? seminarStatus.data.title : ''} sudah
              berada di keranjang anda
            </Text>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 50,
                justifyContent: 'space-between',
                width: '100%',
              }}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  borderWidth: 1,
                  borderColor: colors.color.greenPrimary,
                  borderRadius: 15,
                  marginHorizontal: 5,
                  paddingVertical: 10,
                }}
                onPress={() => navigation.replace('CartStack')}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: colors.color.greenPrimary,
                  }}>
                  Lihat Keranjang
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1,
                  borderWidth: 1,
                  backgroundColor: colors.color.greenPrimary,
                  borderRadius: 15,
                  marginHorizontal: 5,
                  paddingVertical: 10,
                  borderWidth: 0,
                }}
                onPress={() => setModalVisible(false)}>
                <Text style={{textAlign: 'center', color: 'white'}}>Tutup</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      {!loading && seminarStatus.type == 'webinar' && <Webinar />}
      {!loading && seminarStatus.type == 'online_course' && <OnlineCourse />}
    </View>
  );
}

const pricing = StyleSheet.create({
  floatingBottom: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    height: 220,
    width: '100%',
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  price: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'white',
    width: '100%',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingHorizontal: 15,
    paddingVertical: 5,
    marginVertical: 0,
    borderTopWidth: 3,
    borderTopColor: colors.color.grey3,
  },
  honorer: {},
  label: {
    fontWeight: '800',
    fontSize: 16,
  },
  cost: {
    fontWeight: 'bold',
    fontSize: 24,
  },
  removedPrice: {
    textDecorationLine: 'line-through',
    color: colors.color.grey,
  },
  btnHonorer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    flexDirection: 'row',
  },
});

const styles = StyleSheet.create({
  inline: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  inlineTop: {
    alignItems: 'flex-start',
  },
  mainContainer: {
    width: '100%',
    flex: 1,
    position: 'relative',
  },
  descContainer: {
    padding: 20,
    backgroundColor: 'white',
  },
  banner: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 15,
  },
  pillsTab: {
    borderColor: colors.color.grey2,
    borderWidth: 1,
    borderRadius: 10,
    padding: 5,
    marginRight: 10,
  },
  txtGreenLime: {
    color: colors.color.greenShape,
  },
  txtIcon: {
    marginHorizontal: 5,
  },
  photoProfile: {
    width: 100,
    height: 100,
    resizeMode: 'cover',
    borderRadius: 50,
  },
});

export default SeminarDetail;

// return link zoom dan status seminar done
// download sertifikat blm jalan done
// cek apakah nonton ga nonton bisa terbit seminar

// masuk seminar dari halaman detail blm bisa
// seminar list 634 clean up
// Require cycle: node_modules/rn-fetch-blob/index.js -> node_modules/rn-fetch-blob/polyfill/index.js -> node_modules/rn-fetch-blob/polyfill/XMLHttpRequest.js -> node_modules/rn-fetch-blob/index.js
// kuncinya adalah host men-end-kan meeting sesuai dengan schedules, baru, seminar akan pindah ke section seminar yang Telah Selesai
// meeting di end host, pencet back, masih ada tombol masuk ke seminar, di detail
// peserta harus leave setelah waktu berakhir

/**
 * TES Berhasil
 * MASUK seminar agak telat, dari seminar sedang Berlangsung
 * leave dari seminar sesuai jadwal, button blm berubah
 * peserta leave zoom sblm waktunya masih dapet sertifikat
 *
 * mas fadil yang staging
 */
