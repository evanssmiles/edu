import React, {  useState, useEffect, useMemo } from 'react'
import { Text, View, ScrollView, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { colors, font, input, btn } from '../../styling';
import { Header, Button, Input, iconButton, Context, TopBar, BottomNavigation } from '../../component';
import axios from 'axios'
import ComingSoon from  '../../assets/img/comingSoon.svg'

export default function Evaluation() {

    return (
        <View style={{justifyContent: 'center', alignItems: 'center', flex:1, marginTop: -50 }}>
            <ComingSoon height={300} width={300}/>
            {/* <Image source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/1/1a/C%C3%B4ne_orange_-_under_construction.png'}} style={{width: 100, height: 100, resizeMode: 'contain'}}/> */}
        </View>
    )
}

const styles = StyleSheet.create({
    inline: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    dashboardContainer: {
        height: '100%',
        position: 'relative',
    },
    mainContent: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 30
    },
    iconTxt: {
        width: 14,
        height: 14,
        resizeMode: 'contain',
        marginHorizontal: 7,
    },
    txtWarning: {
        color: colors.color.red,
        fontSize: 10,
    },
    card: {
        height: 190,
        width: 155,
        marginHorizontal: 10,
        alignItems: 'center',
        elevation: 35,
    },
    cardHead: {
        width: '100%',
        height: 80,
    },
    cardHeadImg: {
        width: '100%',
        height: 80,
        resizeMode: 'cover'
    },
    cardBody: {
        backgroundColor: 'white',
        width: '100%',
        flex: 1,
        padding: 5,
    },
    cardBodyTitle: {
        fontSize: 12,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    cardBodyData: {
        fontSize: 12,
    },
    clockIcon: {
        width: 12,
        height: 12,
        resizeMode: 'contain',
        marginHorizontal: 5,
    }
})