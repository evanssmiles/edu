import React, {  useState, useEffect, useMemo } from 'react'
import { Text, View, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { colors, font, input, btn } from '../../styling';
import { Header, Button, Input, iconButton, Context } from '../../component';

const { signOut } = React.useContext(Context.AuthContext);
const DashboardStack = createStackNavigator();

export function Dashboard() {
    return (
        <DashboardStack.Navigator screenOptions={{headerShown: false}}>
            <DashboardStack.Screen name={'dashboard'}>
                <View style={{justifyContent: 'center', alignItems: 'center', height: '100%' }}>
                    <TouchableOpacity style={{backgroundColor: 'green', padding: 20}} onPress={signOut()}>
                        <Text style={{color: 'white'}}>Sign Out</Text>
                    </TouchableOpacity>
                </View>
            </DashboardStack.Screen>
        </DashboardStack.Navigator>
    )
}

