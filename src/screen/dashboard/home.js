import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
  RefreshControl,
} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {colors} from '../../styling';
import exploreSeminar from '../../assets/icon/eksplorSeminar.png';
import exploreECourse from '../../assets/icon/eksplorOnlineCourse.png';
// component list
import {SeminarList, MySeminarList} from './SeminarList';
import {RppList} from './RppList';
import SplashScreen from 'react-native-splash-screen';

const HeaderControl = ({navigation}) => {
  const goToExplore = type => {
    navigation.navigate('SeminarStack', {
      screen: 'ExploreSeminar',
      params: {type: type},
    });
  };

  return (
    <View style={{paddingHorizontal: 15, marginBottom: 35, width: '100%'}}>
      <View
        style={[
          styles.inline,
          {
            elevation: 5,
            height: 70,
            width: '100%',
            backgroundColor: 'white',
            borderRadius: 15,
            justifyContent: 'space-evenly',
          },
        ]}>
        <TouchableOpacity
          style={[styles.inline]}
          onPress={() => goToExplore('webinar')}>
          <Image style={styles.iconTxt} source={exploreSeminar} />
          <Text>Eksplor Seminar</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.inline,
            {
              height: '70%',
              paddingLeft: 5,
              borderLeftWidth: 1,
              borderColor: colors.color.grey3,
            },
          ]}
          onPress={() =>
            navigation.navigate('SeminarStack', {
              screen: 'Eksplor Online',
            })
          }>
          <Image style={styles.iconTxt} source={exploreECourse} />
          <Text>Eksplor Online Course</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default function Home({navigation}) {
  const wait = timeout => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  };
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(1000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  // useFocusEffect(() => {
  //   return <MySeminarList navigation={navigation} />;
  // });

  return (
    <View style={styles.dashboardContainer}>
      <View style={{width: '100%', flex: 1}}>
        <ScrollView
          contentContainerStyle={styles.mainContent}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          {!refreshing ? (
            <>
              <HeaderControl navigation={navigation} />
              <MySeminarList navigation={navigation} />
              {/* <RppList /> */}
              <SeminarList navigation={navigation} />
            </>
          ) : (
            <></>
          )}
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  inline: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dashboardContainer: {
    height: '100%',
    position: 'relative',
  },
  mainContent: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 70,
  },
  iconTxt: {
    width: 14,
    height: 14,
    resizeMode: 'contain',
    marginHorizontal: 7,
  },
  txtWarning: {
    color: colors.color.red,
    fontSize: 10,
  },
  card: {
    height: 190,
    width: 155,
    marginHorizontal: 10,
    alignItems: 'center',
    elevation: 35,
  },
  cardHead: {
    width: '100%',
    height: 80,
  },
  cardHeadImg: {
    width: '100%',
    height: 80,
    resizeMode: 'cover',
  },
  cardBody: {
    backgroundColor: 'white',
    width: '100%',
    flex: 1,
    padding: 5,
  },
  cardBodyTitle: {
    fontSize: 12,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  cardBodyData: {
    fontSize: 12,
  },
  clockIcon: {
    width: 12,
    height: 12,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
});
