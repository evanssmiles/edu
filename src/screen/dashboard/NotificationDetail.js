import React, {useState, useEffect} from 'react'
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import { colors } from '../../styling'
import { Input, Button } from '../../component'
import IconInfoRed from '../../assets/icon/infoRed.svg'
import IconInfoGrey from '../../assets/icon/infoGrey.svg'
import IconSuccess from '../../assets/icon/checkedCircleGreen.svg'
import IconClock from '../../assets/icon/iconClockGrey.svg'

const style = StyleSheet.create({
    container: {
        position: 'relative',
        flex: 1,
    },
    content: {
        padding: 20,
    },
    inline: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    icon: {
        marginRight: 10,
    },
    txtGreen: {
        color: colors.color.greenPrimary
    },
    heading: {
        fontSize: 14, 
        fontWeight: 'bold',
    },
    referral: {
        textAlign: 'center',
        fontSize: 32,
        fontWeight: 'bold',
        marginVertical: 25,
    },
    warningTxt: {
        color: colors.color.red,
        lineHeight: 16,
    },
    spaceY: {
        marginVertical: 10,
    },
    absoluteBottom: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        width: '100%',
        padding: 25,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    }
})

const floatBtn = StyleSheet.create({
    button: {
        paddingVertical: 10,
        paddingHorizontal: 15,
        borderRadius: 20,
        elevation: 7,
        backgroundColor: 'white',
        width: 180,
    }
})

const header = StyleSheet.create({
    container: {
        padding: 20,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: '100%',
    },
    btn: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: colors.color.grey2,
        marginRight: 10,
    },
    txt: {
        color: colors.color.grey
    },
    txtActive: {
        color: colors.color.greenPrimary,
    },
    btnActive: {
        borderColor: colors.color.greenPrimary,
    }
})

const body = StyleSheet.create({
    content: {
        padding: 20,
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderBottomColor: colors.color.grey3,
    },
    contentNew: {
        backgroundColor: colors.color.greenComponent
    },
    preview: {
        height: 100,
        width: 100,
        marginRight: 15,
        backgroundColor: colors.color.grey2,
    },
    previewDesc: {
        flex: 1,
    },
    head: {
        marginBottom: 15,
    },
    main: {
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
    },
    desc: {
        fontSize: 14,
        fontWeight: 'bold',
    },
    spaceY: {
        marginVertical: 5,
    }
})

const circle = {
    height: 5,
    width: 5,
    borderRadius: 5/2,
    backgroundColor: colors.color.grey,
    marginHorizontal: 5,
    marginTop: 3 
}

export default function NotificationDetail() {
    const [loading, setLoading] = useState(false)
    const [haveReferral, setHaveReferral] = useState(false)
    const [referral, setReferral] = useState('')
    const [error, setError] = useState('')

    const [item, setItem] = useState([1,2,3,4,5])
    const [type, setType] = useState(['Info', 'Aktivitas'])

    return (
        <View style={[style.container]}>
            <View style={[header.container]}>
                { type.map((item, key) => {
                    return (
                        <TouchableOpacity style={[header.btn, key==0?header.btnActive:null]} key={key}>
                            <Text style={[header.txt, key==0?style.txtGreen: null]}>{item}</Text>
                        </TouchableOpacity>
                    )
                })
                }
            </View>
            <ScrollView>
            {
                item.map((val, key) => {
                    return (
                        <View style={[body.content, key==0?body.contentNew:null]} key={key}>
                            <View style={[style.inline, body.head]}>
                                <IconInfoGrey />
                                <Text style={[{marginHorizontal:5}]}>info</Text>
                                <View style={[circle]}></View>
                                <Text>08.59</Text>
                            </View>
                            <View style={[style.inline, body.main]}>
                                <Image style={[body.preview]} />
                                <View style={[body.previewDesc]}>
                                    <Text style={[body.title, body.spaceY]}>Video Sudah Tersedia</Text>
                                    <Text style={[body.desc, body.spaceY]}>The Matrix of Impactful Educators  - Menjadi Edukator Terbaik</Text>
                                    <View style={[style.inline, body.spaceY]}> 
                                        <IconClock width={12} height={12} style={{marginRight: 10}} />
                                        <Text>29 November 2022</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    )
                })
            }
            </ScrollView>
            <View style={[style.absoluteBottom]}>
                <TouchableOpacity style={[floatBtn.button]}>
                    <Text style={[{textAlign: 'center'}]}>Tandai Lihat Semua</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}
