import React, {useState} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {Input, Button} from '../../component';
import {colors} from '../../styling';
import LinearGradient from 'react-native-linear-gradient';
import logo from '../../assets/img/logotext.png';
import checkGreen from '../../assets/icon/checkGreen.png';
import {openInbox} from 'react-native-email-link';
import axios from 'axios';
import {baseUrl} from '../dashboard/SeminarList';

export default function RequestPassword() {
  const [view, setView] = useState('form');
  const [disable, setDisable] = useState(true);
  const [sending, setSending] = useState(false);
  const [email, setEmail] = useState('');
  const handleEmailChange = email => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(email.trim()) === true) {
      setDisable(false);
    } else {
      setDisable(true);
    }
    setEmail(email);
  };

  const handleOpenEmail = () => {
    openInbox();
  };

  const requestEmail = () => {
    console.log('sending email to', email);
    let emailFinal = email.trim();
    setSending(true);
    setDisable(true);
    setTimeout(async () => {
      setSending(false);
      setDisable(false);
      // setView('notif')
      await axios({
        method: 'post',
        url: baseUrl + '/v2/account/forgot/password/req',
        data: JSON.stringify({
          reqMail: emailFinal,
          type: 'teacher',
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then(function (res) {
          console.log(res);
          if (res.status >= 200 && res.status < 405) {
            setView('notif');
          } else {
            console.log(res);
          }
        })
        .catch(function (error) {
          if (error.response) {
            console.log(error.response);
          } else if (error.request) {
            console.log(error.request);
          } else if (error.message) {
            console.log(error.message);
          }
        });
    }, 3000);
  };

  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={['#00AF68', '#1DDF90']}
      style={styles.container}>
      <Image style={styles.imgLogo} source={logo} />
      <View style={styles.subContainer}>
        {view == 'form' ? (
          <View>
            <Text style={[styles.title, styles.spaceBot]}>Lupa Password ?</Text>
            <Input.Input
              label="E-mail anda"
              onChange={email => {
                handleEmailChange(email);
              }}
            />
            <Button.filledButton
              title={sending ? 'Mengirim...' : 'Kirim Instruksi'}
              disabled={disable}
              onPress={() => {
                requestEmail();
              }}
            />
          </View>
        ) : (
          <View>
            <View style={styles.inline}>
              <Image source={checkGreen} style={styles.icon} />
              <Text style={styles.title}>Instruksi Terkirim</Text>
            </View>
            <View style={styles.centerText}>
              <Text style={[styles.centerText]}>
                Instruksi untuk mereset password anda telah kamu kirim ke
              </Text>
              <Text style={[styles.centerText, styles.title, styles.spaceY]}>
                {email}
              </Text>
              <Text style={[styles.centerText, styles.sub, styles.spaceBot]}>
                Anda akan menerima e-mail selambatnya dalam 5 menit. Mohon cek
                juga folder spam anda
              </Text>
            </View>
            <Button.filledButton
              title="Buka E-mail"
              disabled={false}
              onPress={() => {
                handleOpenEmail();
              }}
            />
          </View>
        )}
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    paddingHorizontal: 50,
    alignItems: 'center',
  },
  subContainer: {
    height: 'auto',
    width: '100%',
    borderRadius: 15,
    paddingVertical: 20,
    paddingHorizontal: 25,
    backgroundColor: 'white',
  },
  inline: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  centerText: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgLogo: {
    height: 200,
    width: 200,
    resizeMode: 'contain',
  },
  icon: {
    width: 18,
    height: 18,
    marginHorizontal: 5,
    resizeMode: 'contain',
  },
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
  sub: {
    color: colors.color.grey,
  },
  centerText: {
    textAlign: 'center',
  },
  spaceBot: {
    marginBottom: 25,
  },
  spaceY: {
    marginVertical: 25,
  },
});
