import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {colors, font, input, btn} from '../../styling';
import {Header, Button, Input, iconButton, ErrorMsg} from '../../component';
import {AuthContext} from '../../component/Context';
import {Alert} from 'react-native';
import { baseUrl } from '../dashboard/SeminarList';

function LoginPasswordScreen({route, navigation}) {
  const {email} = route.params;
  const {signIn} = React.useContext(AuthContext);

  const [error, setError] = useState({
    generalError: '',
    passError: '',
  });

  const [data, setData] = React.useState({
    email: '',
    password: '',
    validInput: false,
    check_textInputChange: false,
    secureTextEntry: true,
  });

  const textInputChange = val => {
    if (val.length != 0) {
      setData({
        ...data,
        email: val,
        check_textInputChange: true,
      });
    } else {
      setData({
        ...data,
        email: val,
        check_textInputChange: false,
      });
    }
  };

  const handlePasswordChange = val => {
    setData({
      ...data,
      password: val,
    });
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: data.secureTextEntry,
    });
  };

  const loginHandler = (email, password) => {
    let userPassword = password;
    let userEmail = email.trim();
    if (userPassword == '') {
      setError({
        ...error,
        passError: 'Password harus diisi',
      });
      return;
    }
    let userData = {
      email: userEmail,
      token: null,
      toCompletion: null,
      step: null,
    };

    fetch(baseUrl+ '/v2/account/login', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        email: userEmail,
        password: userPassword,
      }),
    })
      .then(response => response.json())
      .then(async json => {
        // console.log(json)
        if (json.status == 'success') {
          setError({
            ...error,
            passError: '',
          });
          try {
            userData.token = json.result.token;
            userData.toCompletion = json.result.onboarding.status;
            userData.step = json.result.onboarding.missing;
            signIn(userData);
          } catch (error) {
            console.log('fetching error');
            console.log(error);
          }
        } else {
          let errMsg;
          if (typeof json.message != 'string') {
            errMsg = Object.values(json.message).join(' & ');
          } else {
            errMsg = json.message;
          }
          setError({
            ...error,
            passError: errMsg,
          });
        }
      })
      .catch(error => {
        console.log('error is happening');
        console.error(error);
      });
  };

  return (
    <View style={styles.container}>
      <iconButton.iconButton
        name={'close'}
        onPress={() => {
          navigation.pop();
        }}
      />
      <Header.Header />
      {error.generalError != '' && (
        <ErrorMsg.ErrorTop msg={error.generalError} />
      )}
      <Input.FilledInput
        label={'Alamat Email'}
        value={email}
        styleLabel={{color: colors.color.grey}}
      />
      <Input.Input
        label="Password"
        placeholder="Minimal 8 Karakter"
        onChange={pass => {
          handlePasswordChange(pass);
        }}
        secureTextEntry
        error={error.passError}
      />
      <Text
        style={styles.labelButton}
        onPress={() => {
          navigation.navigate('RequestPass');
        }}>
        Lupa password ?
      </Text>
      <Button.filledButton
        title="Login"
        onPress={() => {
          loginHandler(email, data.password);
        }}
        disabled={data.validInput}
      />
      <View style={styles.inline}>
        <Text>Belum punya akun? </Text>
        <Text
          style={[styles.txtGreen]}
          onPress={() => {
            navigation.navigate('Register');
          }}>
          Daftar
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    height: '100%',
  },
  labelCenter: {
    ...font.textCenterBlack,
    color: colors.color.grey,
  },
  inline: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
  labelButton: {
    marginBottom: 15,
    paddingLeft: 3,
    color: colors.color.grey,
    textDecorationLine: 'underline',
  },
});

export default LoginPasswordScreen;
