import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { Input, Button } from '../../component'
import { colors } from '../../styling'
import logo from '../../assets/img/logotextgreen.png'

export default function ResetPassword() {
    return (
        <View style={styles.container}>
            <Image source={logo} style={styles.imgLogo} />
            <View style={styles.subContainer}>
                <Input.Input 
                    label='Password Baru' />
                <Input.Input 
                    label='Konfirmasi Password Baru' />
                <Button.filledButton
                    title='Ganti Password' 
                    disabled={true}/>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        height: '100%',
        paddingHorizontal: 10,
        alignItems: 'center',
    },
    subContainer: {
        height: 'auto',
        width: '100%',
        borderRadius: 15,
        paddingVertical:  20,
        paddingHorizontal: 15,
    },
    inline: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 20
    },
    centerText: {
        justifyContent: 'center',
        alignItems: 'center',
    },  
    imgLogo: {
        height: 200, 
        width: 200, 
        resizeMode: 'contain'
    },
    icon: {
        width: 18, 
        height: 18, 
        marginHorizontal: 5,
        resizeMode: 'contain'
    },
    title: {
        fontWeight: 'bold',
        textAlign: 'center',
    },
    sub: {
        color: colors.color.grey
    },
    centerText: {
        textAlign: 'center'
    },
    spaceBot: {
        marginBottom: 25,
    },
    spaceY: {
        marginVertical: 25,
    }
})