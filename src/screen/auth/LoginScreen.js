import React, {useState, useEffect, useMemo} from 'react';
import {Text, View, StyleSheet, ActivityIndicator, Alert} from 'react-native';
import {colors, font, input, btn} from '../../styling';
import {
  Header,
  Button,
  Input,
  iconButton,
  ErrorMsg,
  Context,
} from '../../component';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import logoBelajar from '../../assets/icon/belajarid.png';
import logoGoogle from '../../assets/icon/google.png';
import axios from 'axios';
import {baseUrl} from '../dashboard/SeminarList';

GoogleSignin.configure({
  webClientId:
    '554165875311-tgcl8iq89csj316jfoc05l6e5d9krjmh.apps.googleusercontent.com',
  offlineAccess: true,
});

function LoginScreen({navigation}) {
  const [email, setEmail] = useState('');
  const [disable, setDisable] = useState(true);
  const [error, setError] = useState({
    generalError: '',
    emailError: '',
  });

  const [userGoogleInfo, setUserGoogleInfo] = useState({});
  const [loaded, setLoaded] = React.useState(false);

  const [ssoData, setSsoData] = useState({
    userGoogleInfo: {},
    loaded: false,
  });

  const [message, setMessage] = useState('');

  const handleEmailChange = email => {
    setError({
      ...error,
      emailError: '',
    });
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(email.trim()) === true) {
      setDisable(false);
    } else {
      setDisable(true);
    }
    setEmail(email);
  };

  const handleNextLogin = () => {
    if (email == '') {
      setError({
        ...error,
        emailError: 'Email tidak boleh kosong',
      });
      return;
    } else {
      axios({
        url: baseUrl + '/v2/account/login/checker?email=' + email,
        method: 'get',
      })
        .then(function (response) {
          if (response.status < 300) {
            navigation.navigate('LoginPassword', {email: email});
          } else {
            setError({
              ...error,
              emailError: error.data.message,
            });
          }
        })
        .catch(function (err) {
          console.log('err : ', err.response);
          setError({
            ...error,
            emailError: err.response.data.message,
          });
        });
    }
  };

  const {signIn} = React.useContext(Context.AuthContext);

  var signInSSO = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const isSignedIn = await GoogleSignin.isSignedIn();
      if (isSignedIn) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      } else {
        const userInfo = await GoogleSignin.signIn();
        axios({
          method: 'POST',
          url: baseUrl + '/v2/account/login/google',
          headers: {
            token: userInfo.idToken,
          },
        })
          .then(function (res) {
            console.log('result nya : ', res);
            let userData = {
              email: userInfo.user.email,
              token: null,
              toCompletion: null,
              step: null,
            };
            if (res.status < 300) {
              if (res.data.status == 'success') {
                userData.token = res.data.result.token;
                userData.toCompletion = res.data.result.onboarding.status;
                userData.step = res.data.result.onboarding.missing;
                signIn(userData);
              }
            } else {
            }
          })
          .catch(function (error) {
            console.log(error.response.data);
            GoogleSignin.revokeAccess();
            GoogleSignin.signOut();
            Alert.alert('', error.response.data.message);
          });
        setUserGoogleInfo(userInfo);
        setLoaded(true);
        console.log(userInfo);
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('e 1');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('e 2');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('e 3');
      } else {
        console.log(error.message);
        console.log(error);
      }
    }
  };

  return (
    <View style={styles.container}>
      <iconButton.iconButton name={'close'} onPress={() => navigation.pop()} />
      <Header.Header />
      {error.generalError != '' && (
        <ErrorMsg.ErrorTop msg={error.generalError} />
      )}
      <Input.Input
        label="Alamat Email"
        placeholder="contoh@gmail.com"
        value={email}
        error={error.emailError}
        onChange={email => {
          handleEmailChange(email);
        }}
      />
      <Button.filledWhite
        title="Lanjut"
        onPress={() => {
          handleNextLogin();
        }}
        disabled={disable}
      />
      <Text style={styles.labelCenter}>Atau</Text>
      <Button.filledButtonIcon
        title="Login dengan Google"
        img={logoGoogle}
        onPress={() => {
          signInSSO();
        }}
      />
      <Button.filledButtonIcon
        title="Login dengan belajar.id"
        img={logoBelajar}
        onPress={() => {
          signInSSO();
        }}
      />
      <View style={styles.inline}>
        <Text>Belum punya akun? </Text>
        <Text
          style={[styles.txtGreen]}
          onPress={() => {
            navigation.navigate('Register');
          }}>
          Daftar
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    height: '100%',
  },
  labelCenter: {
    ...font.textCenterBlack,
    color: colors.color.grey,
  },
  inline: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
});

export default LoginScreen;
