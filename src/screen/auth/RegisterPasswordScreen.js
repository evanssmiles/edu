import React, {useState} from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import {layout, colors, font, input, btn} from '../../styling';
import {Button, Input, Header, ErrorMsg, iconButton} from '../../component';
import {openInbox} from 'react-native-email-link';
import {AuthContext} from '../../component/Context';
import mailSuccess from '../../assets/icon/successmail.png';
import checklist from '../../assets/icon/checkGreen.png';
import axios from 'axios';

import logoBelajar from '../../assets/icon/belajarid.png';
import logoGoogle from '../../assets/icon/google.png';
import {baseUrl} from '../dashboard/SeminarList';

function RegisterPasswordScreen({route, navigation}) {
  const [page, setPage] = useState('form');
  const [isResend, setIsResend] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [sent, setSent] = useState('Mengirim...');
  const [auth, setAuth] = useState('');
  const {email} = route.params;
  const [data, setData] = React.useState({
    email: email,
    password: '',
    repassword: '',
  });

  const [error, setError] = React.useState({
    email: '',
    password: '',
    repassword: '',
  });

  const {signUp} = React.useContext(AuthContext);

  const registerHandler = () => {
    let userPassword = data.password;
    let userRePass = data.repassword;
    let userEmail = data.email.trim();
    let userData = {
      email: userEmail,
      token: null,
      toCompletion: null,
    };

    fetch(baseUrl + '/v2/account/teacher/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: 'nama default',
        email: userEmail,
        password: userPassword,
        rePassword: userRePass,
      }),
    })
      .then(response => response.json())
      .then(json => {
        console.log(json);
        if (json.status == 'success') {
          setError({
            ...error,
            generalError: '',
          });
          try {
            userData.token = json.auth;
            setPage('notification');
            setAuth(userData.token);
            // signUp(userData)
          } catch (error) {
            console.log(error);
          }
        } else {
          let msg = Object.values(json.message);
          setError({
            ...error,
            generalError: msg.join(' & '),
          });
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  const resendMail = auth => {
    setIsLoading(true);
    let urlx = baseUrl + '/v2/account/email/verification/' + auth + '/resend';
    axios({
      method: 'GET',
      url: urlx,
    })
      .then(function (res) {
        console.log(res);
        if (res.status >= 200 && res.status < 405) {
          setIsResend(true);
          setSent('Telah Terkirim');
          setIsLoading(false);
        } else {
          console.log(res);
        }
      })
      .catch(function (error) {
        if (error.response) {
          console.log(error.response);
        } else if (error.request) {
          console.log(error.request);
        } else if (error.message) {
          console.log(error.message);
        }
      });
  };

  const handlePasswordChange = pass => {
    setData({
      ...data,
      password: pass,
    });
  };

  const handleRePasswordChange = repass => {
    setData({
      ...data,
      repassword: repass,
    });
  };

  return (
    <View style={layout.container}>
      {page == 'form' ? (
        <>
          <iconButton.iconButton
            name={'close'}
            onPress={() => {
              navigation.pop();
            }}
          />
          <Header.Header />
          {error.generalError != '' && (
            <ErrorMsg.ErrorTop msg={error.generalError} />
          )}
          <Input.FilledInput
            label="Alamat Email"
            styleLabel={{color: colors.color.grey}}
            value={email}
          />
          <Input.Input
            label="Password"
            placeholder="Minimal 8 Karakter"
            onChange={pass => {
              handlePasswordChange(pass);
            }}
            secureTextEntry
            error={error.password}
          />
          <Input.Input
            label="Konfirmasi Password"
            placeholder="Harus sama dengan password"
            onChange={re => {
              handleRePasswordChange(re);
            }}
            secureTextEntry
            error={error.repassword}
          />
          <Button.filledButton
            title="Daftar"
            onPress={() => {
              registerHandler();
            }}
          />
          <View style={styles.inline}>
            <Text>Sudah punya akun? </Text>
            <Text
              style={[styles.txtGreen]}
              onPress={() => {
                navigation.navigate('Login');
              }}>
              Masuk
            </Text>
          </View>
        </>
      ) : (
        <View style={styles.screenNotif}>
          <Text style={[styles.titleSuccess]}>
            Kami Telah Mengirim Verifikasi E-mail Anda
          </Text>
          <Image source={mailSuccess} style={styles.imgSuccess} />
          {isResend || isLoading ? (
            <View style={styles.inline}>
              {isResend ? (
                <Image source={checklist} style={{marginHorizontal: 5}} />
              ) : null}
              <Text style={isResend ? styles.txtGreen : null}>{sent}</Text>
            </View>
          ) : (
            <Text style={[styles.txtCenter]}>
              Mohon cek E-mail anda. Tidak menerima E-mail?
              <Text
                style={[styles.txtGreen]}
                onPress={() => {
                  resendMail(auth);
                }}>
                {' '}
                Kirim Ulang
              </Text>
            </Text>
          )}
          <Button.filledWhite
            title="Cek Email"
            style={[{marginTop: 30, width: '100%'}]}
            onPress={() => {
              openInbox();
            }}
            disabled={isLoading}
          />
          <Text style={[styles.spaceYSmall]}>Atau</Text>
          <Button.filledButtonIcon
            title="Daftar Dengan Google"
            img={logoGoogle}
            style={{width: '100%'}}
          />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    height: '100%',
  },
  labelCenter: {
    ...font.textCenterBlack,
    color: colors.color.grey,
  },
  inline: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  spaceYSmall: {
    marginVertical: 15,
  },
  spaceY: {
    marginVertical: 30,
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
  titleSuccess: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 50,
  },
  imgSuccess: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    marginVertical: 20,
  },
  screenNotif: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
    paddingHorizontal: 20,
  },
  txtCenter: {
    textAlign: 'center',
  },
});

export default RegisterPasswordScreen;
