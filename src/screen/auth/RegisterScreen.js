import React, {useState} from 'react';
import {Text, View, StyleSheet, Alert} from 'react-native';
import {layout, colors, font, input, btn} from '../../styling';
import {Button, Input, Header, Context, iconButton} from '../../component';

import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import logoBelajar from '../../assets/icon/belajarid.png';
import logoGoogle from '../../assets/icon/google.png';
import axios from 'axios';
import {baseUrl} from '../dashboard/SeminarList';

GoogleSignin.configure({
  webClientId:
    '554165875311-tgcl8iq89csj316jfoc05l6e5d9krjmh.apps.googleusercontent.com',
  offlineAccess: true,
});

function RegisterScreen({navigation}) {
  const [email, setEmail] = useState('');
  const [error, setError] = useState('');
  const [disable, setDisable] = useState(true);

  const handleEmailChange = email => {
    setError('');
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
    if (reg.test(email.trim()) === true) {
      setDisable(false);
    } else {
      setDisable(true);
    }
    setEmail(email);
  };

  const {signUp} = React.useContext(Context.AuthContext);

  var signUpSSO = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const isSignedIn = await GoogleSignin.isSignedIn();
      if (isSignedIn) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      } else {
        const userInfo = await GoogleSignin.signIn();
        axios({
          method: 'POST',
          url: baseUrl + '/v2/account/teacher/register/google',
          headers: {
            token: userInfo.idToken,
          },
        })
          .then(function (res) {
            console.log(res);
            let userData = {
              email: userInfo.user.email,
              token: null,
              toCompletion: null,
              step: null,
            };
            if (res.status >= 200 && res.status < 300) {
              if (res.data.status == 'success') {
                userData.token = res.data.result.token;
                userData.toCompletion = res.data.result.onboarding.status;
                userData.step = res.data.result.onboarding.missing;
                console.log(userData);
                signUp(userData);
              }
            } else {
            }
          })
          .catch(function (error) {
            console.log(error.response.data);
            GoogleSignin.revokeAccess();
            GoogleSignin.signOut();
            Alert.alert('', error.response.data.message);
          });
        // setUserGoogleInfo(userInfo);
        // setLoaded(true)
        console.log(userInfo);
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('e 1');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('e 2');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('e 3');
      } else {
        console.log(error.message);
        console.log(error);
      }
    }
  };

  var checkEmail = () => {
    axios({
      url: baseUrl + '/v2/account/register/checker?email=' + email,
      method: 'get',
    })
      .then(function (response) {
        if (response.status < 300) {
          navigation.navigate('RegisterPassword', {email: email});
        } else {
          setError('res : ', response.data.message);
        }
      })
      .catch(function (error) {
        console.log('err : ', error.response);
        setError(error.response.data.message);
      });
  };

  return (
    <View style={layout.container}>
      <iconButton.iconButton
        name={'close'}
        onPress={() => {
          navigation.pop();
        }}
      />
      <Header.Header />
      <Input.Input
        label="Alamat Email"
        placeholder="contoh@gmail.com"
        onChange={email => {
          handleEmailChange(email);
        }}
        error={error}
      />
      <Button.filledWhite
        title="Lanjut"
        disabled={disable}
        onPress={() => checkEmail()}
      />
      <Text style={font.textCenterBlack}>Atau</Text>
      <Button.filledButtonIcon
        title="Daftar dengan Google"
        img={logoGoogle}
        onPress={() => {
          signUpSSO();
        }}
      />
      <Button.filledButtonIcon
        title="Daftar dengan belajar.id"
        img={logoBelajar}
        onPress={() => {
          signUpSSO();
        }}
      />
      <View style={styles.inline}>
        <Text>Sudah punya akun? </Text>
        <Text
          style={[styles.txtGreen]}
          onPress={() => {
            navigation.navigate('Login');
          }}>
          Masuk
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    height: '100%',
  },
  labelCenter: {
    ...font.textCenterBlack,
    color: colors.color.grey,
  },
  inline: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  txtGreen: {
    color: colors.color.greenPrimary,
  },
});

export default RegisterScreen;
