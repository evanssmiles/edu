import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import {layout, colors, font, input, btn} from '../styling';
import {Button, Input, Header, Context} from '../component';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import ornament1 from '../assets/icon/ornament1.png';
import ornament2 from '../assets/icon/ornament2.png';
import * as constants from '../constants';

import SplashScreen from 'react-native-splash-screen';

export const SliderWidth = Dimensions.get('screen').width;

export default function Welcome({navigation}) {
  const [activeIndex, setActivateIndex] = useState(0);
  const [carouselState, setCarouselState] = useState([
    {
      title: constants.PelatihanBerkualitasSentence,
      text: '',
    },
    {
      title: constants.KapanpunDimanapun,
      text: '',
    }
  ]);

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  const _onPressCarousel = index => {
    // here handle carousel press
    this.carouselRef.current.snapToItem(index);
  };

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        // onPress={() => {
        //     this._onPressCarousel(index);
        // }}
        style={{
          backgroundColor: 'transparent',
          borderRadius: 20,
          height: 300,
          padding: 50,
        }}>
        <Text style={{fontSize: 20, fontWeight: 'bold'}}>{item.title}</Text>
        <Text>{item.text}</Text>
      </TouchableOpacity>
    );
  };

  const pagination = () => {
    const {entries, activeSlide} = this.state;
    return (
      <Pagination
        dotsLength={entries.length}
        activeDotIndex={activeSlide}
        containerStyle={{backgroundColor: 'rgba(0, 0, 0, 0.75)'}}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 8,
          backgroundColor: 'rgba(255, 255, 255, 0.92)',
        }}
        inactiveDotStyle={
          {
            // Define styles for inactive dots here
          }
        }
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  };

  return (
    <View style={{position: 'relative', height: '100%'}}>
      <Header.Welcome style={{paddingLeft: 30}} />
      <Carousel
        layout={'default'}
        loop={true}
        enableSnap={true}
        data={carouselState}
        sliderWidth={SliderWidth}
        itemWidth={SliderWidth}
        renderItem={renderItem}
        useScrollView
        onSnapToItem={index => setActivateIndex(index)}
        activeSlideAlignment="center"
        lockScrollWhileSnapping={true}
        autoplay={true}
      />
      <Pagination
        dotsLength={100}
        activeDotIndex={activeIndex}
        containerStyle={{backgroundColor: 'black'}}
        dotStyle={{
          width: 100,
          height: 100,
          borderRadius: 5,
          marginHorizontal: 8,
          backgroundColor: 'red',
        }}
        inactiveDotStyle={{
          backgroundColor: 'blue',
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
      <Image source={ornament1} style={[styles.ornament1, styles.orn1]} />
      <Image source={ornament2} style={[styles.ornament1, styles.orn2]} />
      <View style={styles.welcomeNav}>
        <Button.filledButton
          title="Login"
          onPress={() => {
            navigation.navigate('Login');
          }}
          style={{marginBottom: 30}}
        />
        <Button.filledWhite
          title="Daftar"
          onPress={() => {
            navigation.navigate('Register');
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  welcomeNav: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: colors.color.bgColor,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    width: '100%',
    paddingHorizontal: 30,
    paddingVertical: 50,
  },
  ornament: {
    width: 130,
    height: 130,
    resizeMode: 'contain',
  },
  orn1: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  orn2: {
    position: 'absolute',
    // top: '50%',
    left: 0,
    bottom: 200,
    zIndex: -1,
    // transform: [{translateY: -50}]
  },
});
