const redux = require('redux');
const createStore = redux.createStore;

const initialState = {
    value: 0, 
    age: 15,
}

//reducer
const rootReducer = (state = initialState, action) => {
    return state
}

//store 
const store = createStore(rootReducer)
console.log(store.getState())

//subscription
store.subscribe(() => {
    console.log('value changed : ', store.getState())
})

//dispatch
store.dispatch({type: 'ADD_AGE'})
