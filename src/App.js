import React, { useState, useEffect, useMemo } from 'react'
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { AuthStackNavigator } from './navigators/AuthStackNavigator';
import { DashboardStackNavigator } from './navigators/DashboardStackNavigator';
import { CompletionStackNavigator } from './navigators/CompletionStackNavigator';
import { SeminarStackNavigator } from './navigators/SeminarStackNavigator';
import { ActivityStackNavigator } from './navigators/ActivityStackNavigator';
import { CertificateStackNavigator } from './navigators/CertificateStackNavigator';
import { QuizStackNavigator } from './navigators/QuizStackNavigator';
import { CartStackNavigator } from './navigators/CartStackNavigator';
import { PaymentStackNavigator } from './navigators/PaymentStackNavigator';
import { TransactionStackNavigator } from './navigators/TransactionStackNavigator';
import { NotificationStackNavigator } from './navigators/NotificationStackNavigator';
import { ReferralStackNavigator } from './navigators/ReferralStackNavigator';
import { HonorerStackNavigator } from './navigators/HonorerStackNavigator';
import { HistoryStackNavigator } from './navigators/HistoryStackNavigator';

import { AuthContext } from './component/Context';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';
GoogleSignin.configure({
    webClientId: '554165875311-tgcl8iq89csj316jfoc05l6e5d9krjmh.apps.googleusercontent.com',
    offlineAccess: true,
})

const RootStack = createStackNavigator();

export default function App() {

    const initialLoginState = {
        isLoading: true,
        userCompletion: true,
        completionStep: null,
        userName: null, 
        userToken: null,
    }

    const loginReducer = (prevState, action) => {
        switch(action.type) {
            case 'RETRIEVE_TOKEN':
                return{
                    ...prevState,
                    userToken: action.token,
                    userCompletion: action.toCompletion,
                    completionStep: action.completionStep,
                    isLoading: false,
                };
            case 'LOGIN':
                return{
                    ...prevState,
                    userToken: action.token,
                    userEmail: action.id,
                    userCompletion: action.toCompletion,
                    completionStep: action.completionStep,
                    isLoading: false,
                };
            case 'REGISTER':
                return{
                    ...prevState,
                    userToken: action.token,
                    userEmail: action.id,
                    userCompletion: action.toCompletion,
                    completionStep: action.completionStep,
                    isLoading: false,
                };
            case 'LOGOUT':
                return{
                    ...prevState,
                    userToken: null,
                    userEmail: null,
                    userCompletion: false,
                    completionStep: null,
                    isLoading: false,
                };
            case 'COMPLETED':
                return{
                    ...prevState,
                    userCompletion: false,
                    completionStep: null,
                    isLoading: false,
                }
        }
    }

    const [loginState, dispatch] = React.useReducer(loginReducer, initialLoginState)

    const authContext = useMemo(() => ({
        signIn: async(userSignedIn) => {
            const userToken = userSignedIn.token
            const toCompletion = userSignedIn.toCompletion ? 'true' : 'false'
            const step = JSON.stringify(userSignedIn.step)  
            const userEmail = userSignedIn.email
            const items = [['userToken', userToken], ['goCompletion', toCompletion], ['stepCompletion', step]]
            try {
                await AsyncStorage.multiSet(items)
            } catch (error) {
                console.log("error fadil" + error)
            }
            dispatch({ type:"LOGIN", id: userEmail, token:  userToken, toCompletion: userSignedIn.toCompletion, completionStep: step})
        }, 
        signOut: async() => {
            try {
                await GoogleSignin.hasPlayServices();
                const isSignedIn = await GoogleSignin.isSignedIn();
                if (isSignedIn) {
                    await GoogleSignin.revokeAccess();
                    await GoogleSignin.signOut();
                } 
                await AsyncStorage.removeItem('userToken')
                await AsyncStorage.removeItem('goCompletion')
                await AsyncStorage.removeItem('stepCompletion')
            } catch (e) {
                console.log("error async")
                console.log(e)           
            }
            dispatch({ type:"LOGOUT" })
        }, 
        signUp: (userSignedUp) => {
            const userToken = userSignedUp.token
            const toCompletion = userSignedUp.toCompletion
            const userEmail = userSignedUp.email
            const items = [['userToken', userToken], ['goCompletion', 'true'], ['stepCompletion', '1']]
            try {
                AsyncStorage.multiSet(items)
            } catch (error) {
                console.log(error)
            }
            dispatch({type: 'REGISTER', id: userEmail, token: userToken, toCompletion: 'true', completionStep: '1'})
        }, 
        completeData: async() => {
            try {
                await AsyncStorage.setItem('goCompletion', 'false')                
            } catch (error) {
                console.log(error)
            }
            dispatch({type: 'COMPLETED'})
        }
    }), [])

    useEffect(() => {
        setTimeout(async() =>{
            let userToken
            let goCompletion
            let step
            userToken = null
            try {
                userToken = await AsyncStorage.getItem('userToken')
                goCompletion = await AsyncStorage.getItem('goCompletion')
                step = await AsyncStorage.getItem('stepCompletion')
                if (goCompletion != null && goCompletion == 'true') {
                    goCompletion = true
                } else if (goCompletion != null && goCompletion == 'false') {
                    goCompletion = false
                } else if (goCompletion == null){
                    goCompletion = false
                }
                // console.log('token is : ', userToken)
                // console.log('completion is : ', goCompletion)
                // console.log('completion step is : ', step)
            } catch (e) {
                console.log("error async")
                console.log(e)
            }
            dispatch({ type:'RETRIEVE_TOKEN', token: userToken,  toCompletion: goCompletion, completionStep: step})
            // console.log('default state token : ', loginState.userToken)
            // console.log('default state completion : ', loginState.userCompletion)
            // console.log('default state step completion : ', loginState.completionStep)
        }, 250)
    }, [])

    if (loginState.isLoading) {
        return (
            <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                <ActivityIndicator size="large" color="grey"/>
            </View>
        )
    }

    return (
        <AuthContext.Provider value={authContext}>
            <NavigationContainer>
                { loginState.userToken != null && !loginState.userCompletion  &&
                    (
                        <RootStack.Navigator screenOptions={{headerShown: false}}>
                            <RootStack.Screen name={'DashboardStack'} component={DashboardStackNavigator} />
                            <RootStack.Screen name={'SeminarStack'} component={SeminarStackNavigator} />
                            <RootStack.Screen name={'CertificateStack'} component={CertificateStackNavigator} />
                            <RootStack.Screen name={'ActivityStack'} component={ActivityStackNavigator} />
                            <RootStack.Screen name={'CartStack'} component={CartStackNavigator} />
                            <RootStack.Screen name={'PaymentStack'} component={PaymentStackNavigator} />
                            <RootStack.Screen name={'TransactionStack'} component={TransactionStackNavigator} />
                            <RootStack.Screen name={'NotificationStack'} component={NotificationStackNavigator} />
                            <RootStack.Screen name={'ReferralStack'} component={ReferralStackNavigator} />
                            <RootStack.Screen name={'HonorerStack'} component={HonorerStackNavigator} />
                            <RootStack.Screen name={'HistoryStack'} component={HistoryStackNavigator} />
                            <RootStack.Screen name={'QuizStack'} component={QuizStackNavigator} />
                        </RootStack.Navigator> 
                    )                           
                }
                { loginState.userToken != null && loginState.userCompletion &&
                    (
                        <RootStack.Navigator screenOptions={{headerShown: false}}>
                            <RootStack.Screen name={'CompletionStack'} component={CompletionStackNavigator} />
                        </RootStack.Navigator> 
                    )                           
                }
                { loginState.userToken == null &&
                    (
                        <RootStack.Navigator screenOptions={{headerShown: false}}>
                            <RootStack.Screen name={'AuthStack'} component={AuthStackNavigator} />
                        </RootStack.Navigator> 
                    )
                }
            </NavigationContainer>
        </AuthContext.Provider>
    )

}
