import React from 'react'
import { Image, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { btn, colors } from '../styling'

export function filledButton({title, style, styleFont, onPress, disabled=false}) {
    return (
        <TouchableOpacity style={[styles.button, disabled ? styles.buttonDisabled : null, style]} onPress={onPress} disabled={disabled}>
            <Text style={[styles.txtLight, styleFont]}>{title}</Text>
        </TouchableOpacity>
    )
}

export function filledWhite({title, style, onPress, disabled=false}) {
    return (
        <TouchableOpacity style={[styles.buttonWhite, {backgroundColor: 'white'}, disabled ? styles.buttonDisabled : null, style ]} onPress={onPress} disabled={disabled}>
            <Text style={[styles.txtGreen, disabled ? styles.txtLight : null]}>{title}</Text>
        </TouchableOpacity>
    )
}

export function filledButtonIcon({title, img, style, onPress}) {
    return (
        <TouchableOpacity style={[styles.buttonIcon, style]} onPress={onPress}>
            <Text style={styles.txtLight}>{title}</Text>
            <Image style={styles.iconBtn} source={img}/>
            {/* <Image style={styles.iconBtn} source={{uri: img}}/> */}
        </TouchableOpacity>
    )
}


export function textButton({label, style, onPress}) {
    return (
        <TouchableOpacity style={[styles.buttonTxt, styles.textLeft, style]} onPress={onPress}>
            <Text style={[styles.textLeft, style]}>{label}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: {
        ...btn.button,
        backgroundColor: colors.color.greenPrimary,
        marginVertical: 3,
    },
    buttonTxt: {
        ...btn.button,
        backgroundColor: 'transparent',
        marginVertical: 5,
    },
    buttonWhite: {
        ...btn.button,
        backgroundColor: 'white',
        marginVertical: 3,
        elevation: 5,
    },
    buttonIcon: {
        ...btn.buttonIcon,
        ...btn.button,
        backgroundColor: colors.color.greenPrimary,
    },
    buttonWhiteImg: {
        ...btn.button,
        ...btn.buttonShadow,
        backgroundColor: colors.color.white,
        flexDirection: 'row',
    },
    buttonSecondary: {
        ...btn.button,
        marginVertical: 5,
        backgroundColor: colors.color.greenShape,
    },
    buttonDisabled: {
        backgroundColor: colors.color.grey2,
        ...btn.buttonShadow, 
    },
    iconBtn: {
        height: 20,
        width: 20,
        marginHorizontal: 10,
        resizeMode: 'contain',
    },
    txtLight: {
        color: 'white',
        fontWeight: '900',
        fontSize: 20,
    },
    txtDark: {
        color: colors.color.black,
        fontWeight: '900',
        fontSize: 20,
    },
    txtGreen: {
        color: colors.color.greenPrimary,
        fontWeight: '900',
        fontSize: 20,
    },
    txtLeft: {
        textAlign: 'left'
    }
})