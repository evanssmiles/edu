import React from 'react'
import { Image, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { colors } from '../styling'
import Icon from 'react-native-ionicons'

export function iconButton({name, style, onPress}) {
    return (
        <TouchableOpacity style={[styles.container, style]} onPress={onPress}>
            <Icon name={name} style={styles.iconButton} />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        left: 25,
        top: 25,
    },
    iconButton : {
        color: colors.color.grey,
        fontSize: 20,
    }
})

