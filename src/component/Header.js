import React from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'
import { layout, font, image } from '../styling'
import LogoProedu from '../assets/img/logoProedu.svg';

export function Header() {
    return (
        <View style={styles.header}>
            <LogoProedu style={styles.icon} width={24} height={24} />
            <Text style={styles.headline}>Proedu</Text>
        </View>
    )
}

export function Welcome() {
    return (
        <View style={[styles.header, styles.welcoming]}>
            <LogoProedu style={styles.iconBig} />
            <Text style={styles.headlineBig}>Proedu</Text>
        </View>
    )    
}

const styles = StyleSheet.create({
    welcoming : {
        justifyContent: 'flex-start',
        paddingTop: 100,
    },
    headlineBig : {
        ...font.headline,
        fontSize: 42,
    },
    iconBig : {
        ...image.iconButton,
        width: 42,
        height: 42,
        marginLeft: 40,
    },
    header : {
        ...layout.flexRowCenter,
    },
    headline : {
        ...font.headline,
    },
    icon : {
        ...image.iconButton,
    }
})
