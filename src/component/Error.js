import React from 'react'
import { StyleSheet, View, Text, Image } from 'react-native'
import { layout, font, image, colors } from '../styling'

export function ErrorTop({msg}) {
    return (
        <View style={styles.header}>
            <Text style={styles.msgInpError}>{msg}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    header : {
        ...layout.flexRowCenter,
    },
    headline : {
        ...font.headline,
    },
    msgInpError: {
        color: colors.color.red,
        fontSize: 14,
        paddingLeft: 5,
        textAlign: 'center'
    }
})
