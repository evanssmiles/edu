import React, {useState} from 'react';
import {View, TextInput, Image, Text, StyleSheet} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {colors} from '../styling';
import searchIcon from '../assets/icon/search.png';
import IconCorrect from '../assets/icon/checkGreen.svg';
import IconWrong from '../assets/icon/closeRed.svg';

export function Input({
  label,
  placeholder,
  value,
  error = '',
  style,
  onChange,
  ...props
}) {
  // const [errorMsg, setErrorMsg] = useState(error)
  return (
    <View style={[styles.container, style]}>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        {...props}
        style={styles.input}
        placeholder={placeholder}
        placeholderTextColor={colors.color.grey2}
        value={value}
        onChangeText={onChange}
      />
      {error != '' && <Text style={styles.msgInpError}>{error}</Text>}
    </View>
  );
}

export function FilledInput({label, value, styleLabel, ...props}) {
  return (
    <View style={styles.container}>
      <Text style={[styles.label, styleLabel]}>{label}</Text>
      <TextInput
        {...props}
        style={styles.filled}
        value={value}
        disabled={true}
        editable={false}
      />
    </View>
  );
}

export function radioInput({
  label,
  options,
  value,
  error,
  onValueChange,
  ...props
}) {
  const [val, setVal] = React.useState(0);
  return (
    <View style={[styles.container]}>
      <Text style={styles.label}>{label}</Text>
      <RadioButton.Group
        style={[styles.inline]}
        onValueChange={onValueChange}
        value={value}>
        <View style={[styles.inline, {marginTop: 10}]}>
          {options.map((item, idx) => (
            <View style={[styles.inline]} key={idx}>
              <RadioButton
                value={idx}
                uncheckedColor={colors.color.grey}
                color={colors.color.greenPrimary}
                status={val == idx ? 'checked' : 'unchecked'}
              />
              <Text style={[styles.radioText]}>{item}</Text>
            </View>
          ))}
        </View>
      </RadioButton.Group>
      {error != '' && <Text style={styles.msgInpError}>{error}</Text>}
    </View>
  );
}

export function radioAnswer({
  label,
  options,
  value,
  dataPack,
  error,
  onValueChange,
  ...props
}) {
  const [val, setVal] = React.useState(1);
  const opt = StyleSheet.create({
    opt: {
      borderWidth: 1,
      borderColor: colors.color.grey2,
      borderRadius: 10,
      paddingVertical: 5,
      paddingHorizontal: 15,
      marginVertical: 3,
    },
    true: {
      borderColor: colors.color.greenPrimary,
    },
    false: {
      borderColor: colors.color.red,
    },
  });

  return (
    <View style={[]}>
      <Text style={styles.label}>{label}</Text>
      <RadioButton.Group
        style={[styles.inline]}
        onValueChange={onValueChange}
        value={value}>
        <View style={[{marginTop: 5}]}>
          {dataPack.map((item, idx) => (
            <View style={[styles.inline, opt.opt]} key={idx}>
              <RadioButton
                value={item.id}
                uncheckedColor={colors.color.grey}
                color={colors.color.greenPrimary}
                status={val == item.id ? 'checked' : 'unchecked'}
              />
              <Text style={[styles.radioText]}>{item.answer}</Text>
            </View>
          ))}
        </View>
      </RadioButton.Group>
      {error != '' && <Text style={styles.msgInpError}>{error}</Text>}
    </View>
  );
}

export function radioAnswered({
  label,
  options,
  value,
  error,
  onValueChange,
  ...props
}) {
  const [val, setVal] = React.useState(0);
  const opt = StyleSheet.create({
    opt: {
      borderWidth: 1,
      borderColor: colors.color.grey2,
      borderRadius: 10,
      paddingVertical: 5,
      paddingHorizontal: 15,
      marginVertical: 3,
    },
    true: {
      borderColor: colors.color.greenPrimary,
    },
    false: {
      borderColor: colors.color.red,
    },
  });

  return (
    <View style={[]}>
      <Text style={styles.label}>{label}</Text>
      <RadioButton.Group
        style={[styles.inline]}
        onValueChange={onValueChange}
        value={value}>
        <View style={[{marginTop: 5}]}>
          {options.map((item, idx) => (
            <View style={[styles.inline, opt.opt, opt.false]} key={idx}>
              <RadioButton
                value={idx}
                uncheckedColor={colors.color.grey}
                color={colors.color.greenPrimary}
                status={val == idx ? 'checked' : 'unchecked'}
              />
              <Text style={[styles.radioText]}>{item}</Text>
              <IconWrong height={12} width={12} />
              <IconCorrect height={12} width={12} />
            </View>
          ))}
        </View>
      </RadioButton.Group>
      {error != '' && <Text style={styles.msgInpError}>{error}</Text>}
    </View>
  );
}

export function SearchInput({
  placeholder,
  value,
  error,
  style,
  onChange,
  onPressIn,
  ...props
}) {
  return (
    <View style={[styles.container, {position: 'relative', flex: 1}, style]}>
      <TextInput
        {...props}
        style={[styles.input, {paddingVertical: 3, paddingLeft: 35}]}
        placeholder={placeholder}
        placeholderTextColor={colors.color.grey2}
        value={value}
        onChangeText={onChange}
        onPressIn={onPressIn}
      />
      <Image
        source={searchIcon}
        style={[{position: 'absolute', top: 15, left: 10}]}
      />
    </View>
  );
}

export function SearchInputEvent({
  placeholder,
  value,
  error,
  style,
  onChange,
  onPressIn,
  autoFocus,
  ...props
}) {
  return (
    <View style={[styles.container, style]}>
      <TextInput
        {...props}
        style={[styles.input, {paddingVertical: 3, paddingLeft: 35}]}
        placeholder={placeholder}
        placeholderTextColor={colors.color.grey2}
        value={value}
        onChangeText={onChange}
        onPressIn={onPressIn}
        autoFocus={autoFocus}
      />
      <Image
        source={searchIcon}
        style={[{position: 'absolute', top: 15, left: 10}]}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  inline: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    marginVertical: 10,
  },
  label: {
    color: colors.color.black,
    fontSize: 14,
  },
  input: {
    borderWidth: 1,
    borderRadius: 15,
    borderColor: colors.color.grey2,
    paddingVertical: 8,
    paddingHorizontal: 16,
    marginVertical: 5,
    fontSize: 14,
    backgroundColor: 'white',
    color: colors.color.black,
    fontFamily: 'HelveticaNeue',
  },
  filled: {
    borderWidth: 0,
    padding: 0,
    marginVertical: 10,
    fontSize: 14,
    color: colors.color.black,
    fontFamily: 'HelveticaNeue',
  },
  msgInpError: {
    color: colors.color.red,
    fontSize: 12,
    paddingLeft: 5,
  },
  radioText: {
    color: colors.color.black,
    marginRight: 25,
    flex: 1,
  },
});
