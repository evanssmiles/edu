import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';

import {colors} from '../styling';
import {SearchInput} from './Input';
import cartIcon from '../assets/icon/cart2.png';
import historyIcon from '../assets/icon/history.png';
import notifIcon from '../assets/icon/notifications_none2.png';
import referralIcon from '../assets/icon/refferal.png';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {baseUrl} from '../screen/dashboard/SeminarList';

export function TopBar({navigation}) {
  const [cartTotal, setCartTotal] = useState([]);
  useEffect(async () => {
    axios({
      url: baseUrl + '/v2/transaction/cart',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        token: await AsyncStorage.getItem('userToken'),
      },
    })
      .then(function (response) {
        let collect = response.data.result.DetailTransaction;
        console.log(collect);
        if (Array.isArray(collect)) {
          setCartTotal(collect);
        }
      })
      .catch(function (error) {
        console.log(error.response);
      });
  }, []);

  const route = useRoute();

  return (
    <View style={styles.container}>
      {route.name === 'DashboardStack' && (
        <View style={[styles.inline]}>
          <Text style={styles.headline}>Home</Text>
          <TouchableOpacity
            style={styles.inline}
            onPress={() => navigation.navigate('ReferralStack')}>
            <Image style={styles.menuIcon} source={referralIcon} />
            <Text style={styles.menuTxt}>Referral Code</Text>
          </TouchableOpacity>
        </View>
      )}

      <View style={[styles.inline]}>
        <SearchInput
          style={styles.inputSearch}
          placeholder="Cari Event"
          onPressIn={() =>
            route.name === 'DashboardStack' &&
            navigation.navigate('SeminarStack', {
              screen: 'Cari Seminar',
            })
          }
        />
        <TouchableOpacity
          style={{position: 'relative'}}
          onPress={() => navigation.navigate('CartStack')}>
          {cartTotal.length > 0 && (
            <View
              style={{
                position: 'absolute',
                top: -15 / 2,
                right: 0,
                backgroundColor: 'red',
                borderRadius: 15 / 2,
                width: 15,
                height: 15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 12, lineHeight: 12, color: 'white'}}>
                {cartTotal.length}
              </Text>
            </View>
          )}
          <Image style={styles.menuIcon} source={cartIcon} />
        </TouchableOpacity>
        {/* <TouchableWithoutFeedback
          onPress={() => navigation.navigate('NotificationStack')}>
          <Image style={styles.menuIcon} source={notifIcon} />
        </TouchableWithoutFeedback> */}
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate('HistoryStack')}>
          <Image style={styles.menuIcon} source={historyIcon} />
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  inline: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  container: {
    backgroundColor: 'white',
    padding: 20,
    elevation: 10,
  },
  headline: {
    fontSize: 20,
    fontWeight: '800',
  },
  menuIcon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
  inputSearch: {
    paddingRight: 30,
  },
});
