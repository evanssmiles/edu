import React, {useState} from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native'
import { colors } from '../styling'
import menuHomeActive from '../assets/icon/menuHomeActive.png'
import menuHome from '../assets/icon/menuHome.png'
import menuScheduleActive from '../assets/icon/menuJadwalActive.png'
import menuSchedule from '../assets/icon/menuJadwal.png'
import menuEvaluationActive from '../assets/icon/menuEvaluasiActive.png'
import menuEvaluation from '../assets/icon/menuEvaluasi.png'
import menuRPPActive from '../assets/icon/menuRPPActive.png'
import menuRPP from '../assets/icon/menuRPP.png'
import menuProfileActive from '../assets/icon/menuProfileActive.png'
import menuProfile from '../assets/icon/menuProfile.png'

export function BottomNavigation({navigation}) {
    const [activeMenu, setActiveMenu] = useState('home')
    const changeMenu = (menu, screen) => {
        setActiveMenu(menu)
        navigation.navigate(screen)
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity style={[styles.menu]} onPress={ () => { changeMenu('home', 'Home') }}>
                <Image source={activeMenu=='home'?menuHomeActive:menuHome}/>
                <Text style={[styles.menuTxt, activeMenu=='home'?styles.activeTxt:null]}>Home</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.menu]} onPress={ () => { changeMenu('schedule', 'Schedule') }}>
                <Image source={activeMenu=='schedule'?menuScheduleActive:menuSchedule}/>
                <Text style={[styles.menuTxt, activeMenu=='schedule'?styles.activeTxt:null]}>Jadwal</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.menu]} onPress={ () => { changeMenu('evaluation', 'Evaluation') }}>
                <Image source={activeMenu=='evaluation'?menuEvaluationActive:menuEvaluation}/>
                <Text style={[styles.menuTxt, activeMenu=='evaluation'?styles.activeTxt:null]}>Evaluasi Diri</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.menu]} onPress={ () => { changeMenu('rpp', 'RPP') }}>
                <Image source={activeMenu=='rpp'?menuRPPActive:menuRPP}/>
                <Text style={[styles.menuTxt, activeMenu=='rpp'?styles.activeTxt:null]}>RPP</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.menu]} onPress={ () => { changeMenu('profile', 'Account') }} >
                <Image source={activeMenu=='profile'?menuProfileActive:menuProfile}/>
                <Text style={[styles.menuTxt, activeMenu=='profile'?styles.activeTxt:null]}>Akun</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%',
        height: 80,
        backgroundColor: colors.color.blueDark,
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        padding: 10,
    },
    menu: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuTxt: {
        color: 'white', 
        fontSize: 12,
    }, 
    activeTxt: {
        color: colors.color.yellowPrimary
    },
})