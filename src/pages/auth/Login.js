import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native';
import { Alert } from 'react-native';
import { TextInput } from 'react-native';
import { SafeAreaView } from 'react-native';
import { Text, View, Image } from 'react-native';
import { colors, font, input, btn } from '../../styling';
import {Header, Button, Input} from '../../component';
import logo from '../../assets/img/logo.png';

function LoginScreen({navigation}) {
    return (
        <View style={{padding: 20}}>
            <Header.Header/>
            <Input.Input label='Alamat Email' placeholder='contoh@gmail.com'/>
            <Input.Input label='Password' placeholder='Minimal 8 Karakter' secureTextEntry/>
            <Text>Lupa Password?</Text>
            <Button.filledButton title="Masuk" onPress={() => { navigation.navigate('Register') }} />
            <Text style={font.textCenterBlack}>Atau</Text>
            <Button.filledButtonIcon title="Masuk dengan Google" img="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png"/>

        <Text onPress={() => { navigation.navigate('Register') }} >Belum punya akun? Daftar</Text>
        </View>   
    )
}
class FormLogin extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             email: '',
             password: '',
             isLogin: false,
        }
    }

    login = () => {
        if (!this.state.email && !this.state.password) {
            Alert.alert("Error", "Email dan Password harus diisi")
        } else {
            this.setState({isLogin: true})
        }
    }

    typeEmail = (email) => {
        this.setState({
            email: email
        })
    }

    typePassword = (pass) => {
        this.setState({
            password: pass
        })
    }
    
    render() {
        const {email, password, isLogin} = this.state;
        return (
            <View>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginVertical: 30}}>
                    <Image style={btn.iconButton} source={logo}></Image>
                    <Text style={font.headline}>Proedu</Text>
                </View>
                <SafeAreaView>
                    <Text>Alamat Email</Text>
                    <TextInput 
                        placeholder="contoh@gmail.com"
                        style={input.input}
                        value={email}
                        onChangeText={(email) => {this.setState({email})}}
                    />
                    <Text>Password</Text>
                    <TextInput 
                        placeholder="Minimal 8 karakter"
                        style={input.input}
                        value={password}
                        onChangeText={(password) => this.setState({password})}
                        // secureTextEntry={true}
                    />
                    <Text>Lupa Password?</Text>
                    <TouchableOpacity style={btn.primaryButton} onPress={()=>this.login()} >
                        <Text style={font.textCenter}>Login</Text>
                    </TouchableOpacity>
                    <Text style={font.textCenterBlack}>Atau</Text>
                    <TouchableOpacity style={btn.optionalButton} onPress={()=>this.login()} >
                        <Text style={font.textCenterBlack}>Masuk dengan Google</Text>
                        <Image style={btn.iconButton} source={{uri:'https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png'}}></Image>
                    </TouchableOpacity>
                </SafeAreaView>
                <Text>Belum punya akun? Daftar</Text>
                {isLogin && <Text>Berhasil Login</Text>}
            </View>
        )
    }
}

export default LoginScreen
// export default FormLogin