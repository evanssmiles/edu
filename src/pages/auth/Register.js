import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native';
import { Alert } from 'react-native';
import { TextInput } from 'react-native';
import { SafeAreaView } from 'react-native';
import { Text, View, Image } from 'react-native';
import { layout, colors, font, input, btn } from '../../styling';
import logo from '../../assets/img/logo.png';
import {Button, Input} from '../../component'

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function RegisterScreen({navigation}) {
    return (
        <View style={layout.container}>
            <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginVertical: 30}}>
                <Image style={btn.iconButton} source={logo}></Image>
                <Text style={font.headline}>Proedu</Text>
            </View>
            <SafeAreaView>
                <Input.Input label='Alamat Email' placeholder='contoh@gmail.com'/>
                <Input.Input label='Password' placeholder='Minimal 8 Karakter' secureTextEntry/>
                <Input.Input label='Konfirmasi Password' placeholder='Harus sama dengan password' secureTextEntry/>
                <Button.filledButton title="Daftar" onPress={()=> {navigation.navigate('Login')}}/>
                <Text style={font.textCenterBlack}>Atau</Text>
                <Button.filledButtonIcon title="Daftar dengan Google" img="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png"/>
            </SafeAreaView>
            <Text onPress={()=> {navigation.navigate('Login')}}>Sudah punya akun? Login</Text>
        </View>
    )
}
class Register extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             email: '',
             password: '',
             isLogin: false,
        }
    }

    login = () => {
        if (!this.state.email && !this.state.password) {
            Alert.alert("Error", "Email dan Password harus diisi")
        } else {
            this.setState({isLogin: true})
        }
    }

    typeEmail = (email) => {
        this.setState({
            email: email
        })
    }

    typePassword = (pass) => {
        this.setState({
            password: pass
        })
    }

    render() {
        const {email, password, isLogin} = this.state;
        return (
            <View style={layout.container}>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginVertical: 30}}>
                    <Image style={btn.iconButton} source={logo}></Image>
                    <Text style={font.headline}>Proedu</Text>
                </View>
                <SafeAreaView>
                    <Input.Input label='Alamat Email' placeholder='contoh@gmail.com'/>
                    <Input.Input label='Password' placeholder='Minimal 8 Karakter' secureTextEntry/>
                    <Input.Input label='Konfirmasi Password' placeholder='Harus sama dengan password' secureTextEntry/>
                    <Button.filledButton title="Daftar"/>
                    <Text style={font.textCenterBlack}>Atau</Text>
                    <Button.filledButtonIcon title="Daftar dengan Google" img="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png"/>
                </SafeAreaView>
                <Text onPress={() => this.props.navigation.navigate('login')}>Sudah punya akun? Login</Text>
                {isLogin && <Text>Berhasil Login</Text>}
            </View>
        )
    }
}

export default RegisterScreen
// export default Register