import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import {CertificateRedeem} from '../screen/dashboard/CertificateRedeem'

const CertificateStack = createStackNavigator();

export function CertificateStackNavigator({navigation, route}) {    
    const {id} = route.params
    const {published} = route.params
    const {certificateData} = route.params
    return (
        <CertificateStack.Navigator screenOptions={{headerShown: false}}>
            <CertificateStack.Screen name={'Sertifikat'}>
                {props => <CertificateRedeem {...props} id={id} published={published} certificateData={certificateData} />}
            </CertificateStack.Screen>
        </CertificateStack.Navigator>
    )
}
