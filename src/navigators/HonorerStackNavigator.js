import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import HonorerDetail from '../screen/dashboard/HonorerDetail'

const HonorerStack = createStackNavigator();

export function HonorerStackNavigator({navigation, route}) {    
    return (
        //screenOptions={{headerShown: false}}
        <HonorerStack.Navigator>
            <HonorerStack.Screen name={'Dokumen Guru Honorer'}>
                {props => <HonorerDetail {...props} />}
            </HonorerStack.Screen>
        </HonorerStack.Navigator>
    )
}
