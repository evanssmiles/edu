import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import ProfileDetail from '../screen/dashboard/ProfileDetail'

const ProfileStack = createStackNavigator();

export function ProfileStackNavigator({navigation, route}) {    
    const {id} = route.params
    return (
        <SeminarStack.Navigator>
            <SeminarStack.Screen name={'Aktivitas Online Course'}>
                {props => <SeminarDetail {...props} id={id} />}
            </SeminarStack.Screen>
        </SeminarStack.Navigator>
    )
}
