import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import ReferralDetail from '../screen/dashboard/ReferralDetail'

const ReferralStack = createStackNavigator();

export function ReferralStackNavigator({navigation, route}) {    
    return (
        //screenOptions={{headerShown: false}}
        <ReferralStack.Navigator> 
            <ReferralStack.Screen name={'Referral Code'}>
                {props => <ReferralDetail {...props} />}
            </ReferralStack.Screen>
        </ReferralStack.Navigator>
    )
}
