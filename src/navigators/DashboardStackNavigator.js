import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {colors, font, input, btn} from '../styling';
import {
  Header,
  Button,
  Input,
  iconButton,
  Context,
  TopBar,
  BottomNavigation,
} from '../component';
import {AuthContext} from '../component/Context';

import homeScreen from '../screen/dashboard/home';
import accountScreen from '../screen/dashboard/account';
import scheduleScreen from '../screen/dashboard/schedule';
import rppScreen from '../screen/dashboard/rpp';
import evaluationScreen from '../screen/dashboard/evaluation';
import {SeminarStackNavigator} from './SeminarStackNavigator';
import SeminarDetail from '../screen/dashboard/SeminarDetail';

const DashboardStack = createStackNavigator();

export function DashboardStackNavigator({navigation}) {
  return (
    <View style={{position: 'relative', height: '100%'}}>
      <TopBar.TopBar navigation={navigation} />
      <DashboardStack.Navigator screenOptions={{headerShown: false}}>
        <DashboardStack.Screen name={'Home'} component={homeScreen} />
        <DashboardStack.Screen name={'Account'} component={accountScreen} />
        <DashboardStack.Screen name={'RPP'} component={rppScreen} />
        <DashboardStack.Screen
          name={'Evaluation'}
          component={evaluationScreen}
        />
        <DashboardStack.Screen name={'Schedule'} component={scheduleScreen} />
      </DashboardStack.Navigator>
      <BottomNavigation.BottomNavigation navigation={navigation} />
    </View>
  );
}
