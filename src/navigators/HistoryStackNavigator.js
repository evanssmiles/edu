import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import HistoryDetail from '../screen/dashboard/HistoryDetail'

const HistoryStack = createStackNavigator();

export function HistoryStackNavigator({navigation, route}) {    
    return (
        <HistoryStack.Navigator>
            <HistoryStack.Screen name={'Riwayat'}>
                {props => <HistoryDetail {...props} />}
            </HistoryStack.Screen>
        </HistoryStack.Navigator>
    )
}
