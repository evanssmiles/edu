import React, {  useState, useEffect, useMemo } from 'react'
import { Text, View, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { colors, font, input, btn } from '../../styling';
import { Header, Button, Input, iconButton, Context } from '../component';
import { AuthContext } from '../component/Context';
import JobDetail from '../screen/CompletionData'

const CompletionStack = createStackNavigator();

export function CompletionStackNavigator() {    
    return (
        <CompletionStack.Navigator screenOptions={{headerShown: false}}>
            <CompletionStack.Screen name={'completion1'} component={JobDetail} />
        </CompletionStack.Navigator>
    )
}
