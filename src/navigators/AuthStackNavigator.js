import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Welcome from '../screen/Welcome'
import LoginScreen from '../screen/auth/LoginScreen'
import LoginPasswordScreen from '../screen/auth/LoginPasswordScreen'
import RegisterPasswordScreen from '../screen/auth/RegisterPasswordScreen'
import RegisterScreen from '../screen/auth/RegisterScreen'
import RequestPassword from '../screen/auth/RequestPassword'
import ResetPassword from '../screen/auth/ResetPassword'

const AuthStack = createStackNavigator();

export function AuthStackNavigator() {
    return (
        <AuthStack.Navigator screenOptions={{headerShown: false}}>
            <AuthStack.Screen name={'Welcome'} component={Welcome} />
            <AuthStack.Screen name={'Login'} component={LoginScreen} />
            <AuthStack.Screen name={'LoginPassword'} component={LoginPasswordScreen} />
            <AuthStack.Screen name={'RegisterPassword'} component={RegisterPasswordScreen} />
            <AuthStack.Screen name={'Register'} component={RegisterScreen} />
            <AuthStack.Screen name={'RequestPass'} component={RequestPassword} />
            <AuthStack.Screen name={'ResetPass'} component={ResetPassword} />
        </AuthStack.Navigator>
    )
}
