import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import QuizPaper from '../screen/dashboard/QuizPaper';

const QuizStack = createStackNavigator();

export function QuizStackNavigator({navigation, route}) {
  const {id} = route.params;
  const {questionBank} = route.params;
  const {quizData} = route.params;
  return (
    <QuizStack.Navigator screenOptions={{headerShown: false}}>
      <QuizStack.Screen name={'Aktivitas Online Course'}>
        {props => (
          <QuizPaper
            {...props}
            id={id}
            questionBank={questionBank}
            quizData={quizData}
          />
        )}
      </QuizStack.Screen>
    </QuizStack.Navigator>
  );
}
