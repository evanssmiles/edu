import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import PaymentDetail from '../screen/dashboard/PaymentDetail';
import PaymentChoose from '../screen/dashboard/PaymentChoose';
import PaymentConfirm from '../screen/dashboard/PaymentConfirm';
import PaymentDirect from '../screen/dashboard/PaymentDirect';

const PaymentStack = createStackNavigator();

export function PaymentStackNavigator({navigation, route}) {
  const {data} = route.params;
  return (
    <PaymentStack.Navigator screenOptions={{headerShown: false}}>
      <PaymentStack.Screen name={'Direct'}>
        {props => <PaymentDirect {...props} data={data} />}
      </PaymentStack.Screen>
      <PaymentStack.Screen name={'Summary'}>
        {props => <PaymentDetail {...props} data={data} />}
      </PaymentStack.Screen>
      <PaymentStack.Screen name={'Pembayaran'}>
        {props => <PaymentChoose {...props} data={data} />}
      </PaymentStack.Screen>
      <PaymentStack.Screen name={'Waiting'}>
        {props => <PaymentConfirm {...props} data={data} />}
      </PaymentStack.Screen>
    </PaymentStack.Navigator>
  );
}
