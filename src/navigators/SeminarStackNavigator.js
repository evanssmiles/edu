import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SeminarDetail from '../screen/dashboard/SeminarDetail';
import SeminarExplore from '../screen/dashboard/SeminarExplore';
import SeminarSearch from '../screen/dashboard/SeminarSearch';
import OnlineExplore from '../screen/dashboard/OnlineExplore';

const SeminarStack = createStackNavigator();

export function SeminarStackNavigator({navigation, route}) {
  const {id} = route.params;
  const {justPublished} = route.params.justPublished==undefined?false:route.params;
  return (
    <SeminarStack.Navigator>
      <SeminarStack.Screen name={'Eksplor Seminar'}>
        {props => <SeminarDetail {...props} id={id} justPublished={justPublished}/>}
      </SeminarStack.Screen>
      <SeminarStack.Screen name={'ExploreSeminar'}>
        {props => <SeminarExplore {...props} />}
      </SeminarStack.Screen>
      <SeminarStack.Screen name={'Eksplor Online'}>
        {props => <OnlineExplore {...props} />}
      </SeminarStack.Screen>
      <SeminarStack.Screen name={'Cari Seminar'}>
        {props => <SeminarSearch {...props} />}
      </SeminarStack.Screen>
    </SeminarStack.Navigator>
  );
}
