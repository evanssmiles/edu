import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ActivityDetail from '../screen/dashboard/ActivityDetail';

const ActivityStack = createStackNavigator();

export function ActivityStackNavigator({navigation, route}) {
  const {id} = route.params;
  const {title} = route.params;
  const {speaker} = route.params;
  const {task} = route.params;
  const {quizDone} = route.params;
  const {published} = route.params;
  const {certificateData} = route.params;

  return (
    <ActivityStack.Navigator screenOptions={{headerShown: false}}>
      <ActivityStack.Screen name={'Aktivitas Online Course'}>
        {props => (
          <ActivityDetail
            {...props}
            id={id}
            title={title}
            speaker={speaker}
            tugasAwal={task}
            quizDone={quizDone}
            published={published}
            certificateData={certificateData}
          />
        )}
      </ActivityStack.Screen>
    </ActivityStack.Navigator>
  );
}
