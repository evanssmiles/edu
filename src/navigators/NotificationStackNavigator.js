import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import NotificationDetail from '../screen/dashboard/NotificationDetail'

const NotificationStack = createStackNavigator();

export function NotificationStackNavigator({navigation, route}) {    
    return (
        <NotificationStack.Navigator>
            <NotificationStack.Screen name={'Notifikasi'}>
                {props => <NotificationDetail {...props} />}
            </NotificationStack.Screen>
        </NotificationStack.Navigator>
    )
}
