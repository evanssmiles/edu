import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import CartDetail from '../screen/dashboard/CartDetail'

const CartStack = createStackNavigator();

export function CartStackNavigator({navigation, route}) {    
    return (
        <CartStack.Navigator>
            <CartStack.Screen name={'Keranjang'}>
                {props => <CartDetail {...props} />}
            </CartStack.Screen>
        </CartStack.Navigator>
    )
}
