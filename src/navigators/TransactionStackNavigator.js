import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import TransactionList from '../screen/dashboard/TransactionList';
import TransactionDetail from '../screen/dashboard/TransactionDetail';

const TransactionStack = createStackNavigator();

export function TransactionStackNavigator({navigation, route}) {
  const {data} = route.params;
  return (
    <TransactionStack.Navigator screenOptions={{headerShown: false}}>
      <TransactionStack.Screen name={'List'}>
        {props => <TransactionList {...props} data={data} />}
      </TransactionStack.Screen>
      <TransactionStack.Screen name={'Detail'}>
        {props => <TransactionDetail {...props} data={data} />}
      </TransactionStack.Screen>
    </TransactionStack.Navigator>
  );
}
